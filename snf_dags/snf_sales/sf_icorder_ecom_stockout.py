import boto3
import pathlib
import os
import pandas as pd
from airflow import DAG
from datetime import datetime,date,timedelta

from airflow.utils.dates import days_ago
from airflow.operators.python import PythonOperator
from airflow.operators.bash_operator import BashOperator
from snf_dags.etl_functions import sftp_download
from snf_dags.etl_functions import transforming_data
from snf_dags.etl_functions import upload_file_to_S3
from snf_dags.snowflake_connection import upload_to_snowflake
from snf_dags.etl_functions import removing_raw_process_s3_files
from configparser import ConfigParser, RawConfigParser

from snf_dags.etl_functions import sf_connect_sting
from snf_dags.etl_functions import prod_sf_connect_sting

from airflow.exceptions import AirflowException
from snf_dags.etl_functions import sender_msg
from airflow.sensors.external_task_sensor import ExternalTaskSensor

####### importing credential info #############################

config = RawConfigParser()
res = config.read('/home/ubuntu/airflow/dags/snf_dags/credential.ini')

bucket_name = config.get('S3', 'S3_B_NAME')
bucket_key = config.get('S3', 'S3_SWF_B_KEY')

username = config.get('SNF_SFTP', 'USERNAME')
password = config.get('SNF_SFTP', 'PASSWORD')
sf_wh = config.get('SNOWFLAKE', 'SF_WAREHOUSE')

sf_wh = config.get('SNOWFLAKE', 'SF_WAREHOUSE')
sf_db =  config.get('SNOWFLAKE', 'SF_DATABASE')

session = boto3.session.Session('s3')
credentials = session.get_credentials()
s3_access_id = credentials.access_key
s3_secret_key = credentials.secret_key

####### importing path ########################################

path = '/home/ubuntu/airflow/dags/snf_sales/'
html_path = '/home/ubuntu/airflow/dags/'

files = ["ICOrderHistory", "eCommerceShipmentsHistory"]
comp_size_dict = {"ICOrderHistory":0, "eCommerceShipmentsHistory":500000}

unload_files = ["olap_ecommerce_1_","ecom_stockout_flash_1_", "ecommerce_flash_1_"]

####### creating email message ################################

miss_match_msg= dict(Body = 'Please check all the files in sftp',
                 Subject = 'Miss-Match Alert: Snowflake - Icorder and Ecomm files size are below the standard size')
sucess_msg= dict(Body = 'All files Size are matched and loaded sucessfully',
                 Subject = 'Snowflake: SnF Icorder and Ecomm files loaded Successfully')

####### Creating unloading function #########################

def query_df_callable(cursor, query_list):

    final_df = pd.DataFrame()
    for qry in query_list:
        cursor.execute(qry)
        col_names = ','.join([col[0] for col in cursor.description]).lower()
        for c in cursor:
            df = pd.DataFrame(c, columns=[col_names])
            final_df = pd.concat([final_df,df], axis=1)
    return(final_df)


def run_sf_multple_queries(cursor,path_name,file,**kwargs):
    scriptContents = pathlib.Path(os.path.join(path_name,'script_files',file)).read_text(encoding='utf-8')
    sqlStatements = scriptContents.split(sep=';')
    for statement in sqlStatements[:-1]:
        cursor.execute(f'{statement.format(**kwargs)}' + ';')


copy_qry = """COPY INTO PUBLIC.{0}
            FROM s3://hypersonixdata/Smart_and_Final/swf_dump/{1} 
            credentials=(AWS_KEY_ID='{2}' AWS_SECRET_KEY='{3}')
            FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '{4}' NULL_IF = ('NULL', 'null','Null','',' ')
                           EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
            FORCE = TRUE;"""

def run_sf_cmd(path,script_path):
    
    qry_list = ["select max(daydate) as ic_max_date from public.ico_order",
            "select  max(daydate) as ecom_max_date from public.ecommerce",
            "select  max(transaction_date) as stockout_max_date from public.ecom_stockout_flash"]
    
    sf_con = sf_connect_sting()
    curr = sf_con.cursor()

    curr.execute('USE DATABASE %s;' % sf_db)
    curr.execute('USE WAREHOUSE %s;' % sf_wh)
    
    final_df = query_df_callable(curr, qry_list)
    ic_max_date = str(final_df.ic_max_date[0])
    ecom_max_date = str(final_df.ecom_max_date[0])
    stockout_max_date = str(final_df.stockout_max_date[0])

            
    run_sf_multple_queries(curr,path,'sf_icorder_ecom_loading_file.sql',
                           aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key,
                           maxdate = ic_max_date)
    
    run_sf_multple_queries(curr,path,'olap_ecom.sql',
                           aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key,
                           maxdate =ecom_max_date)
    
    run_sf_multple_queries(curr,path,'ecom_stockout.sql',
                           aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key,
                           maxdate = stockout_max_date)

    curr.close()
    
def run_ecom_flash (path,script_file):
    
    qry_list = ["select max(transaction_date) as max_date from public.ecommerce_flash;"]
    
    sf_con = sf_connect_sting()
    curr = sf_con.cursor()
    curr.execute('USE DATABASE %s;' % sf_db)
    curr.execute('USE WAREHOUSE %s;' % sf_wh)
    
    prod_sf_con =prod_sf_connect_sting()
    prod_curr = prod_sf_con.cursor()
    prod_curr.execute('USE DATABASE %s;' % 'prod_smartnfinal')
    prod_curr.execute('USE WAREHOUSE %s;' % 'dev')
    
    final_df = query_df_callable(curr, qry_list)
    max_date = str(final_df.max_date[0])
    run_sf_multple_queries(curr,path,script_file, maxdate = max_date,
                          aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
    
    print("Loading olap_ecom, ecom_flash, ecom_stock data into prod")
    
    df = pd.read_sql("".join(qry_list), prod_sf_con)
    print(df)
    prod_max_date = str(df.MAX_DATE[0])
    
    run_sf_multple_queries(prod_curr,path,"sf_ent_copy_ecommerce.sql", maxdate = prod_max_date,
                          aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
    
    curr.close()
    prod_curr.close()
    
def collect_info(dict_file,query,conn):
    idx = 0
    final_df = pd.DataFrame()
    for tbl,col in dict_file.items():
        conn.execute(query.format(col_name =col,table_name = tbl))
        col_names = ','.join([col[0] for col in conn.description]).lower()
        col_list = col_names.split(",")
        df = pd.DataFrame(conn, columns = col_list)
        df.insert(loc=idx, column='table_name', value=tbl)
        final_df = pd.concat([final_df,df], axis=0)
    return(final_df)

snf_icorder_ecom_dict = {'ico_order':'daydate','ecommerce':'daydate','olap_ecommerce':'transaction_date',
                         'ecom_stockout_flash':'transaction_date','ecommerce_flash':'transaction_date'}



def success_dag_mail(path_name):
    sf_con = sf_connect_sting()
    curr = sf_con.cursor()
    curr.execute('USE DATABASE %s;' % sf_db)
    curr.execute('USE WAREHOUSE %s;' % sf_wh)

    try:
        date_query = """select {col_name} as max_date, count(*) as count, created_at 
                        from public.{table_name} 
                        group by {col_name},created_at order by {col_name} desc limit 2;"""

        df1 = collect_info(snf_icorder_ecom_dict,date_query,curr)
        df1.to_html(os.path.join(path_name,'html_files','sf_icorder_ecom.html'),justify='center',col_space=100,index=False)
        sender_msg(sucess_msg,'v_arush.s@hypersonix.ai, navaneeth@hypersonix.ai',is_html=True,html_file= 'sf_icorder_ecom.html')
    except :
        raise AirflowException(f"HTML Mail ERROR")

###### Creating dags ##########################################

default_args = {'owner': 'Sudhakar','depends_on_past': False,
               'email': ['v_arush.s@hypersonix.ai', 'navaneeth@hypersonix.ai'],'email_on_failure': True}

dag = DAG(
    dag_id = 'sf_icorder_ecom_stockout_flashes_1',
    default_args=default_args,
    start_date = datetime(2021,10,12),
    schedule_interval = '10 12 * * *')

wait_for_sales_file_loaded_task = ExternalTaskSensor(
    task_id='wait_for_sales_file_loaded',
    external_dag_id='snowflake_snf_sales_1',
    external_task_id='success_dag_mail'
)


downloading_files_task = PythonOperator(
    task_id = 'downloading_files_sftp',
    provide_context=True,
    python_callable =sftp_download,
    retry_delay = timedelta(minutes=15),
    retries = 4,
    op_kwargs={
        'path':path,
        'u_name' : username,
        'p_word' : password,
        'msg1': miss_match_msg ,
        'msg2': sucess_msg,
        'files_name': files ,
        'comp_size_files':comp_size_dict,
        'sftp_dir': '/upload/',
        'val_path': 'validation_files',
        'raw_path': 'raw_files',
        'val_file': 'snf_icorder_ecom_val.json',
    },
    dag = dag)

p_icorder_files_task = PythonOperator(
    task_id = 'transforming_icorder_raw_files',
    python_callable =transforming_data,
    op_kwargs={'path':path,'sep': '|','pattern':files[0], 'headers':True,
              'raw_path':'raw_files','process_path':'process_files'},
    dag = dag)

p_ecom_files_task = PythonOperator(
    task_id = 'transforming_ecom_raw_files',
    python_callable =transforming_data,
    op_kwargs={'path': path,'sep': ',', 'pattern':files[1],
              'raw_path':'raw_files','process_path':'process_files'},
    dag = dag)

s3_transfer_task = PythonOperator(
    task_id = 'loading_files_into_s3',
    python_callable =upload_file_to_S3,
    op_kwargs={'path': path, 'pattern':files,'process_path':'process_files',
               'bucket_name':bucket_name,'bucket_key':bucket_key},
    dag = dag)

snowflake_load_task = PythonOperator(
    task_id = 'copy_file_into_snowflake',
    python_callable =run_sf_cmd,
    op_kwargs={'path':path, 'script_path':'script_files'},
    dag = dag)

ecomm_flash_task = PythonOperator(
    task_id = 'creating_ecomm_flash_report',
    python_callable =run_ecom_flash,
    op_kwargs={'path':path, 'script_file':'ecom_flash.sql'},
    dag = dag)


remove_source_files_task = PythonOperator(
    task_id = 'deleting_raw_process_s3_files',
    python_callable =removing_raw_process_s3_files,
    op_kwargs={'path': path,'pattern':files + unload_files,
               'bucket_name':bucket_name,'bucket_key':bucket_key,
              'raw_path':'raw_files','process_path':'process_files'},
    dag = dag)

success_dag_mail_task = PythonOperator(
    task_id = 'success_dag_mail',
    python_callable =success_dag_mail,
    op_kwargs={'path_name': html_path},
    dag = dag)

wait_for_sales_file_loaded_task >> downloading_files_task >> p_icorder_files_task >> p_ecom_files_task >> s3_transfer_task  >> snowflake_load_task >> ecomm_flash_task >> remove_source_files_task >> success_dag_mail_task













