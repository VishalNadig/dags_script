import boto3
import pathlib
import os
import re
import json
import time
import paramiko
import pandas as pd
from airflow import DAG
from datetime import datetime, date , timedelta
from airflow.utils.dates import days_ago
from airflow.exceptions import AirflowException
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python import PythonOperator,BranchPythonOperator
from airflow.operators.bash_operator import BashOperator
from snf_dags.etl_functions import sftp_download
from snf_dags.etl_functions import transforming_data
from snf_dags.etl_functions import upload_file_to_S3
from snf_dags.etl_functions import sender_msg
from snf_dags.etl_functions import removing_raw_process_s3_files
from configparser import ConfigParser, RawConfigParser

from snf_dags.etl_functions import prod_sf_connect_sting
from snf_dags.etl_functions import sf_connect_sting

from airflow.sensors.external_task_sensor import ExternalTaskSensor


####### importing credential info #############################

config = RawConfigParser()
res = config.read('/home/ubuntu/airflow/dags/snf_dags/credential.ini')

bucket_name = config.get('S3', 'S3_B_NAME')
bucket_key = config.get('S3', 'S3_SWF_B_KEY')

username = config.get('SNF_SFTP', 'USERNAME')
password = config.get('SNF_SFTP', 'PASSWORD')

sf_wh = config.get('SNOWFLAKE', 'SF_WAREHOUSE')
sf_db =  config.get('SNOWFLAKE', 'SF_DATABASE')


session = boto3.session.Session('s3')
credentials = session.get_credentials()
s3_access_id = credentials.access_key
s3_secret_key = credentials.secret_key

####### importing path ########################################

path = '/home/ubuntu/airflow/dags/snf_dags/snf_sales/'
html_path = '/home/ubuntu/airflow/dags/snf_dags/'

sales_files = ['ItemDiscounts','Transactions', 'TransactionDetails', 'Tenders']
#sales_comp_size_dict = {'ItemDiscounts': 3000000, 'Transactions': 3000000, 
#                        'TransactionDetails': 15000000,'Tenders': 2000000}

sales_comp_size_dict = {'ItemDiscounts': 0, 'Transactions': 0, 
                        'TransactionDetails': 0,'Tenders': 0}

sales_budget = [ 'SalesBudget']
sales_budget_comp_size_dict = {'SalesBudget': 2000000}

item_files = ['Product','Locations','MerchStructure']
item_comp_size_dict = { 'Product': 10000000, 
                        'Locations': 40000, 'MerchStructure': 100000}

case_file = ["CaseItemSales"]
case_comp_size_dict = {"CaseItemSales":60000}

unload_files = ['olap_item_details_1_', 'dim_products_1_', 'dim_store_new_1_', 'dim_store_1_',
               'transaction_details_1_', 'transactions_1_', 'tenders_1_', 'item_discounts_1_','price_new_1_']

# 'olap_inventory_details_3_1_', 'olap_ecommerce_1_','ecom_stockout_flash_1_' ,ecommerce_flash_1_

####### creating email message template ################################

sales_miss_match_msg= dict(Body = 'Please check all the snf sales files in sftp',
                 Subject = 'Miss-Match Alert: Snowflake - SnF sales files size are below the standard size ')
sales_sucess_msg= dict(Body = 'All snf sales files Size are matched and loaded sucessfully',
                 Subject = 'Snowflake: SnF Sales files loaded Successfully')

item_miss_match_msg= dict(Body = 'Please check all the snf item files in sftp',
                 Subject = 'Miss-Match Alert: Snowflake - item files size are below the standard size')
item_sucess_msg= dict(Body = 'All item files Size are matched and loaded sucessfully',
                 Subject = 'Snowflake: SnF Items files loaded Successfully')

prod_count_miss_match_msg= dict(Body = 'Product count is less than exiting product table',
                 Subject = 'Miss-Match Alert: Snowflake- Product count miss match')
store_count_miss_match_msg= dict(Body = 'Store count is grater than exiting store table',
                 Subject = 'Miss-Match Alert:Snowflake- Store count miss match')

####### Creating unloading function #########################


copy_qry = """COPY INTO PUBLIC.{0}
            FROM s3://hypersonixdata/Smart_and_Final/swf_dump/{1} 
            credentials=(AWS_KEY_ID='{2}' AWS_SECRET_KEY='{3}')
            FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '{4}' 
                            SKIP_HEADER = 1 DATE_FORMAT = 'YYYY/MM/DD')
            FORCE = TRUE;"""
        

def branch_sftp_check(**context):
    received_response = context['ti'].xcom_pull(key='response')
    if received_response == 'Files_Available':
        return ('run_items')
    else:
        return ('run_sales')
    
def transform_sales_budget_data(path,raw_path,process_path, sep, pattern):
    files = os.listdir(os.path.join(path,raw_path))
    match_files = [fn for fn in files if re.match('|'.join(pattern),fn)]
    for fn in match_files:
        print(f"Processing files {fn}")
        df = pd.read_csv(os.path.join(path ,raw_path,fn),sep=sep)
        df['filename'] = fn
        df['created_at'] = date.today().strftime('%Y-%m-%d')
        print("processed file created")
        df.to_csv(os.path.join(path,process_path,fn),sep=sep,index=False)
    
def transform_sales_data(path,raw_path,process_path, sep, pattern):
    files = os.listdir(os.path.join(path,raw_path))
    match_files = [fn for fn in files if re.match('|'.join(pattern),fn)]
    for fn in match_files:
        print(f"Processing files {fn}")
        df = pd.read_csv(os.path.join(path ,raw_path,fn),sep=sep,compression='gzip',encoding='latin-1')
        df['filename'] = fn
        df['created_at'] = date.today().strftime('%Y-%m-%d')
        df ['date_check_id'] = df.apply(lambda x: str(x['TransactionDate']).replace('/','') + \
                                "{:04d}".format(x['StoreNumber']) + \
                                 "{:04d}".format(x['LaneNumber']) +\
                                "{:04d}".format(x['TransactionNumber']), axis=1)
        df.to_csv(os.path.join(path,process_path ,fn.replace(".gz","")),sep=sep,index=False)
    
    
###############################################################

def query_df_callable(cursor, query_list):

    final_df = pd.DataFrame()
    for qry in query_list:
        cursor.execute(qry)
        col_names = ','.join([col[0] for col in cursor.description]).lower()
        for c in cursor:
            df = pd.DataFrame(c, columns=[col_names])
            final_df = pd.concat([final_df,df], axis=1)
    return(final_df)


def run_sf_multple_queries(cursor,path_name,file,**kwargs):
    scriptContents = pathlib.Path(os.path.join(path_name,'script_files',file)).read_text(encoding='utf-8')
    sqlStatements = scriptContents.split(sep=';')
    for statement in sqlStatements[:-1]:
        cursor.execute(f'{statement.format(**kwargs)}' + ';')
        
        
##################################################

class AllowAnythingPolicy(paramiko.MissingHostKeyPolicy):
    def missing_host_key(self, client, hostname, key):
        return

def check_tran_master_file(filename):
    
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(AllowAnythingPolicy())
    client.connect('ftp.hypersonix.io', username= username, password=password,banner_timeout=200)
    sftp = client.open_sftp()
    
    sftp.chdir('upload/')
    
    curr_dt = date.today().strftime("%Y-%m-%d")
    struc_time = time.strptime(curr_dt, '%Y-%m-%d') 
    curr_mtime = time.mktime(struc_time) 

    latestfile = None
    for fileattr in sftp.listdir_attr():
        if fileattr.filename.startswith(filename) and fileattr.st_mtime > curr_mtime:
            latestfile = fileattr.filename

    if latestfile is None:
        raise AirflowException(f'Master/Transacton File is not available in sftp: {latestfile}')
    else:
        print(f'Master/Transacton File is available in sftp: {latestfile}')
        pass


def run_sf_cmd(path, **context):
    
    sf_con = sf_connect_sting()
    curr = sf_con.cursor()
    curr.execute('USE DATABASE %s;' % sf_db)
    curr.execute('USE WAREHOUSE %s;' % sf_wh)
    
    prod_sf_con =prod_sf_connect_sting()
    prod_curr = prod_sf_con.cursor()
    prod_curr.execute('USE DATABASE %s;' % 'prod_smartnfinal')
    prod_curr.execute('USE WAREHOUSE %s;' % 'dev')
    
    run_sf_multple_queries(curr,path,'master_file_stg.sql',
                           aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
    
    qry_list = ["select  min(business_date) as min_date from public.tenders_staging",
                "select  max(transaction_date) as max_date from public.olap_item_details where record_type in (1,2,3,4)",
                "select min(business_date) as from_date  from public.tenders_staging",
                "select max(business_date) as to_date  from public.tenders_staging",
                "select count(*) as new_prod_count from public.dim_item_new ;",
                "select count(*) as old_prod_count from public.dim_products;",
                "select count(*) as new_store_count from public.dim_location_new;",
                "select count(*) as old_store_count from public.dim_store;"]
    
    
    final_df = query_df_callable(curr, qry_list)
            
    min_date = str(final_df.min_date[0])
    max_date = str(final_df.max_date[0])
    from_date = str(final_df.from_date[0])
    to_date = str(final_df.to_date[0])
    new_prod_count = int(final_df.new_prod_count)
    old_prod_count = int(final_df.old_prod_count)
    new_store_count = int(final_df.new_store_count)
    old_store_count = int(final_df.old_store_count)
            

            
    received_response = context['ti'].xcom_pull(key='response')
    
    print(f'tenders_staging min_date {min_date}')
    print(f'olap_item_details max_date {max_date}')
    
    if min_date == max_date:
        
        run_sf_multple_queries(curr,path,'inserting_master_file.sql', mindate = min_date)

        if received_response == 'Files_Available':

            if new_prod_count >= old_prod_count:
                run_sf_multple_queries(curr,path,'dim_product.sql')
            else: sender_msg(prod_count_miss_match_msg, 'v_arush.s@hypersonix.ai')

            if new_store_count <= old_store_count:
                run_sf_multple_queries(curr,path,'store_update.sql')
            else: sender_msg(store_count_miss_match_msg, 'v_arush.s@hypersonix.ai')

            run_sf_multple_queries(curr,path,'olap_and_other_query.sql',fromdate = from_date, todate = to_date,
                                  aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)


        else:
            run_sf_multple_queries(curr,path,'olap_and_other_query.sql',fromdate = from_date, todate = to_date,
                              aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)

        print("Loading Sales Olap data into Prod")

        run_sf_multple_queries(prod_curr,path,'sf_ent_copy_sales_olap.sql', maxdate = max_date,
                              aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key, fromdate = from_date)

        curr.close()
        prod_curr.close()
        
    else:
        raise AirflowException(f"Min and Max Date is different of olap table and tender staging table")
                

def loading_item_files(path):
    
    sf_con = sf_connect_sting()
    curr = sf_con.cursor()
    curr.execute('USE DATABASE %s;' % 'dev_smartnfinal')
    curr.execute('USE WAREHOUSE %s;' % 'dev')
            
    run_sf_multple_queries(curr,path,'snf_item_files_loading_db.sql',
                           aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
   
    curr.close()
    
    
    
def collect_info(dict_file,query,conn):
    idx = 0
    final_df = pd.DataFrame()
    for tbl,col in dict_file.items():
        conn.execute(query.format(col_name =col,table_name = tbl))
        col_names = ','.join([col[0] for col in conn.description]).lower()
        col_list = col_names.split(",")
        df = pd.DataFrame(conn, columns = col_list)
        df.insert(loc=idx, column='table_name', value=tbl)
        final_df = pd.concat([final_df,df], axis=0)
    return(final_df)

snf_sales_dict = {'tenders':'business_date','transactions':'business_date',
              'item_discounts':'business_date','transaction_details':'businessdate',
              'olap_item_details':'transaction_date','ecommerce_flash':'transaction_date'}

snf_item_dict = {'price_new':'filename','dim_item_new':'filename',
              'dim_location_new':'filename','dim_category_new':'filename',
              'caseitem_sale':'filename'}

snf_sales_budget_dict = {'Sales_Budget':'day_date'}

def success_dag_mail(path_name):
    
    try:
        sf_con = sf_connect_sting()
        curr = sf_con.cursor()
        curr.execute('USE DATABASE %s;' % sf_db)
        curr.execute('USE WAREHOUSE %s;' % sf_wh)

        date_query = """select {col_name} as max_date, count(*) as count, created_at
                        from public.{table_name}
                        group by {col_name},created_at order by {col_name} desc limit 2;"""

        df1 = collect_info(snf_sales_dict,date_query,curr)
        df1.to_html(os.path.join(path_name,'html_files','sf_sales_file.html'),justify='center', col_space=100,index=False)
        sender_msg(sales_sucess_msg, 'v_arush.s@hypersonix.ai, navaneeth@hypersonix.ai, meenakshi@hypersonix.ai, vaibhav.g@hypersonix.ai, archit.s@hypersonix.ai',
                   is_html=True,html_file= 'sf_sales_file.html')

        file_query = """select {col_name}, count(*) as count, created_at 
                        from public.{table_name}  
                        group by {col_name},created_at order by {col_name} desc limit 1;"""

        df2 = collect_info(snf_item_dict,file_query,curr)
        df2.to_html(os.path.join(path_name,'html_files','sf_item_file.html'),justify='center', col_space=100,index=False)
        sender_msg(item_sucess_msg, 'v_arush.s@hypersonix.ai, navaneeth@hypersonix.ai, meenakshi@hypersonix.ai, vaibhav.g@hypersonix.ai, archit.s@hypersonix.ai',
                   is_html=True,html_file= 'sf_item_file.html')

        df3 = collect_info(snf_sales_budget_dict,file_query,curr)
        df3.to_html(os.path.join(path_name,'html_files','sf_sales_budget_file.html'),justify='center', col_space=100,index=False)
        sender_msg(item_sucess_msg, 'v_arush.s@hypersonix.ai, navaneeth@hypersonix.ai, meenakshi@hypersonix.ai, vaibhav.g@hypersonix.ai, archit.s@hypersonix.ai',
                   is_html=True,html_file= 'sf_sales_budget_file.html')
    except :
        raise AirflowException(f"HTML Mail ERROR")

        
    
##########################################

default_args = {'owner': 'Sudhakar','depends_on_past': True,
               'email': ['v_arush.s@hypersonix.ai', 'navaneeth@hypersonix.ai', 'vaibhav.g@hypersonix.ai', 'archit.s@hypersonix.ai'],'email_on_failure': True}


dag = DAG(
    dag_id = 'snowflake_snf_sales_1',
    default_args=default_args,
    start_date = datetime(2021,10,12),
    schedule_interval = '10 12 * * *')

check_transaction_file_task = PythonOperator(
    task_id = 'check_transaction_file',
    python_callable =check_tran_master_file,
    retry_delay = timedelta(minutes=5),
    retries =12,
    op_kwargs={'filename':'Hypersonix_Transactions.trg'},
    dag = dag)

downloading_sales_budget_files_task = PythonOperator(
    task_id = 'downloading_sales_budget_files_task',
    provide_context=True,
    python_callable =sftp_download,
    retry_delay = timedelta(minutes=5),
    retries = 3,
    op_kwargs={
        'path':path,
        'u_name' : username,
        'p_word' : password,
        'msg1': sales_miss_match_msg ,
        'msg2': sales_sucess_msg,
        'files_name': sales_budget,
        'comp_size_files':sales_budget_comp_size_dict,
        'sftp_dir': '/upload/',
        'val_path': 'validation_files',
        'raw_path': 'raw_files',
        'val_file': 'sf_sales_budget_val.json',
    },
    dag = dag)

downloading_sales_files_task = PythonOperator(
    task_id = 'downloading_sales_files_task',
    provide_context=True,
    python_callable =sftp_download,
    retry_delay = timedelta(minutes=5),
    retries = 3,
    op_kwargs={
        'path':path,
        'u_name' : username,
        'p_word' : password,
        'msg1': sales_miss_match_msg ,
        'msg2': sales_sucess_msg,
        'files_name': sales_files ,
        'comp_size_files': sales_comp_size_dict,
        'sftp_dir': '/upload/',
        'val_path': 'validation_files',
        'raw_path': 'raw_files',
        'val_file': 'snf_sales_file_val.json',
    },
    dag = dag)

check_master_file_task = PythonOperator(
    task_id = 'check_master_file',
    python_callable =check_tran_master_file,
    retry_delay = timedelta(minutes=5),
    retries = 12,
    op_kwargs={'filename':'Hypersonix_Master.trg'},
    dag = dag)

downloading_item_files_task = PythonOperator(
    task_id = 'downloading_item_files',
    provide_context=True,
    python_callable =sftp_download,
    retry_delay = timedelta(minutes=5),
    retries = 3,
    op_kwargs={
        'path':path,
        'u_name' : username,
        'p_word' : password,
        'msg1': item_miss_match_msg ,
        'msg2': item_sucess_msg,
        'files_name': item_files ,
        'comp_size_files':item_comp_size_dict,
        'sftp_dir': '/upload/',
        'val_path': 'validation_files',
        'raw_path': 'raw_files',
        'val_file': 'snf_items_file_val.json',
    },
    dag = dag)

downloading_case_file_task = PythonOperator(
    task_id = 'downloading_caseitem_files',
    provide_context=True,
    python_callable =sftp_download,
    retry_delay = timedelta(minutes=5),
    retries = 3,
    op_kwargs={
        'path':path,
        'u_name' : username,
        'p_word' : password,
        'msg1': item_miss_match_msg ,
        'msg2': item_sucess_msg,
        'files_name': case_file ,
        'comp_size_files':case_comp_size_dict,
        'sftp_dir': '/upload/',
        'val_path': 'validation_files',
        'raw_path': 'raw_files',
        'val_file': 'snf_case_file_val.json',
    },
    dag = dag)


branch_task = BranchPythonOperator(
    task_id='branch_sftp_check',
    python_callable=branch_sftp_check,
    trigger_rule='none_skipped',
    provide_context=True,
    dag=dag
  )

branch_items = DummyOperator(task_id='run_items', dag=dag)
branch_sales = DummyOperator(task_id='run_sales', dag=dag)
      

p_sales_files_task = PythonOperator(
    task_id = 'transforming_sales_raw_files',
    python_callable =transform_sales_data,
    trigger_rule='none_failed_or_skipped',
    op_kwargs={'path':path,'sep': '|','pattern':sales_files,
              'raw_path':'raw_files','process_path':'process_files'},
    dag = dag)

p_sales_budget_files_task = PythonOperator(
    task_id = 'transform_sales_budget_data',
    python_callable =transform_sales_budget_data,
    trigger_rule='none_failed_or_skipped',
    op_kwargs={'path':path,'sep': '|','pattern':sales_budget,
              'raw_path':'raw_files','process_path':'process_files'},
    dag = dag)

p_items_files_task = PythonOperator(
    task_id = 'transforming_items_raw_files',
    python_callable =transforming_data,
    op_kwargs={'path': path,'sep': '|', 'pattern':item_files,'headers':True,
              'raw_path':'raw_files','process_path':'process_files'},
    dag = dag)

p_case_file_task = PythonOperator(
    task_id = 'transforming_caseitem_raw_files',
    python_callable =transforming_data,
    op_kwargs={'path': path,'sep': '|', 'pattern':case_file,'headers':True,
              'raw_path':'raw_files','process_path':'process_files'},
    dag = dag)

loading_items_files_task = PythonOperator(
    task_id = 'loading_items_files_into_db',
    python_callable =loading_item_files,
    op_kwargs={'path': path},
    dag = dag)

s3_case_sales_transfer_task = PythonOperator(
    task_id = 'loading_sales_files_into_s3',
    python_callable =upload_file_to_S3,
    op_kwargs={'path': path, 'pattern':sales_files + case_file + sales_budget,
               'bucket_name':bucket_name,'bucket_key':bucket_key,'process_path':'process_files'},
    dag = dag)

s3_items_transfer_task = PythonOperator(
    task_id = 'loading_items_files_into_s3',
    python_callable =upload_file_to_S3,
    op_kwargs={'path': path, 'pattern':item_files,
              'bucket_name':bucket_name,'bucket_key':bucket_key,'process_path':'process_files'},
    dag = dag)

       
make_olap_task = PythonOperator(
    task_id = 'creating_olap_into_snowflake',
    python_callable =run_sf_cmd,
    provide_context=True,
    op_kwargs={'path':path},
    dag = dag)



remove_source_files_task = PythonOperator(
    task_id = 'deleting_raw_process_s3_files',
    python_callable =removing_raw_process_s3_files,
    op_kwargs={'path': path,'pattern': sales_files + item_files + case_file + sales_budget + unload_files,
              'bucket_name':bucket_name,'bucket_key':bucket_key,
              'raw_path':'raw_files','process_path':'process_files'},
    dag = dag)

success_dag_mail_task = PythonOperator(
    task_id = 'success_dag_mail',
    python_callable =success_dag_mail,
    op_kwargs={'path_name': html_path},
    dag = dag)

check_transaction_file_task >> downloading_sales_files_task >> downloading_sales_budget_files_task >> check_master_file_task >> downloading_item_files_task >> downloading_case_file_task >>  branch_task >> branch_items >> p_items_files_task >> s3_items_transfer_task
s3_items_transfer_task >> loading_items_files_task >>  p_sales_files_task >> p_sales_budget_files_task >> p_case_file_task >> s3_case_sales_transfer_task
s3_case_sales_transfer_task >> make_olap_task  >> remove_source_files_task >> success_dag_mail_task

branch_task >> branch_sales >> p_sales_files_task >>p_case_file_task >> s3_case_sales_transfer_task >> make_olap_task
make_olap_task >> remove_source_files_task >> success_dag_mail_task

