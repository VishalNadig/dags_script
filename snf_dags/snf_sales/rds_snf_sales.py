
# import os
# import re
# import json
# import boto3
# import pathlib
# import paramiko
# import psycopg2
# import pandas as pd
# from airflow import DAG
# from etl_functions import sender_msg
# from airflow.utils.dates import days_ago
# from etl_functions import sftp_download
# from etl_functions import transforming_data
# from etl_functions import upload_file_to_S3
# from datetime import datetime, date , timedelta
# from airflow.exceptions import AirflowException
# from snowflake_connection import upload_to_snowflake
# from etl_functions import get_redshift_connect_string
# from configparser import ConfigParser, RawConfigParser
# from etl_functions import removing_raw_process_s3_files
# from airflow.operators.bash_operator import BashOperator
# from airflow.operators.dummy_operator import DummyOperator
# from airflow.operators import PythonOperator,BranchPythonOperator
# from airflow.operators.sensors import ExternalTaskSensor

# ####### importing credential info #############################

# config = RawConfigParser()
# res = config.read('/home/ubuntu/airflow/dags/credential.cfg')

# bucket_name = config.get('S3', 'S3_B_NAME')
# bucket_key = config.get('S3', 'S3_RDS_B_KEY')

# username = config.get('SNF_SFTP', 'USERNAME')
# password = config.get('SNF_SFTP', 'PASSWORD')


# session = boto3.session.Session()
# credentials = session.get_credentials()
# s3_access_id = credentials.access_key
# s3_secret_key = credentials.secret_key

# ####### importing path ########################################

# path = '/home/ubuntu/airflow/dags/snf_sales/'
# html_path = '/home/ubuntu/airflow/dags/'

# sales_files = ['ItemDiscounts','Transactions', 'TransactionDetails', 'Tenders']
# sales_comp_size_dict = {'ItemDiscounts': 3000000, 'Transactions': 3000000, 
#                         'TransactionDetails': 15000000,'Tenders': 2000000}

# item_files = ['Price','Product','Locations','MerchStructure']
# item_comp_size_dict = { 'Price': 15000000, 'Product': 10000000, 
#                         'Locations': 40000, 'MerchStructure': 100000}

# case_file = ["CaseItemSales"]
# case_comp_size_dict = {"CaseItemSales":60000}

# s3_del_files = ['olap_item_details_sales','dim_products','dim_store','CaseItemSales']


# ####### creating email message template ################################

# sales_miss_match_msg= dict(Body = 'Please check all the snf sales files in sftp',
#                  Subject = 'Miss-Match Alert: Redshift - SnF sales files size are below the standard size ')
# sales_sucess_msg= dict(Body = 'All snf sales files Size are matched and loaded sucessfully',
#                  Subject = 'Redshift: SnF Sales files loaded Successfully')

# item_miss_match_msg= dict(Body = 'Please check all the snf item files in sftp',
#                  Subject = 'Miss-Match Alert: Redshift - item files size are below the standard size')
# item_sucess_msg= dict(Body = 'All item files Size are matched and loaded sucessfully',
#                  Subject = 'Redshift: SnF Items files loaded Successfully')

# prod_count_miss_match_msg= dict(Body = 'Product count is less than exiting product table',
#                  Subject = 'Miss-Match Alert: Redshift- Product count miss match')
# store_count_miss_match_msg= dict(Body = 'Store count is grater than exiting store table',
#                  Subject = 'Miss-Match Alert: Redshift- Store count miss match')

# ####### Creating unloading function #########################


# copy_qry = """copy smartnfinal.{0} FROM
#            's3://hypersonixdata/Smart_and_Final/rds_dump/{1}' 
#            access_key_id '{2}' secret_access_key '{3}' dateformat 'auto'
#            maxerror as 0 DELIMITER '{4}' ignoreheader {5} NULL AS 'NULL';"""
        

# def branch_sftp_check(**context):
#     received_response = context['ti'].xcom_pull(key='response')
#     if received_response == 'Files_Available':
#         return ('run_items')
#     else:
#         return ('run_sales')
    
    
# def transform_sales_data(path,raw_path,process_path,sep,pattern):
#     files = os.listdir(os.path.join(path,raw_path))
#     match_files = [fn for fn in files if re.match('|'.join(pattern),fn)]
#     for fn in match_files:
#         print(f"Processing files {fn}")
#         df = pd.read_csv(os.path.join(path,raw_path,fn),sep=sep,compression='gzip',encoding='latin-1')
#         df['filename'] = fn
#         df['created_at'] = date.today().strftime('%Y-%m-%d')
#         df ['date_check_id'] = df.apply(lambda x: str(x['TransactionDate']).replace('/','') + \
#                                 "{:04d}".format(x['StoreNumber']) + \
#                                  "{:04d}".format(x['LaneNumber']) +\
#                                 "{:04d}".format(x['TransactionNumber']), axis=1)
#         df.to_csv(os.path.join(path,process_path,fn.replace(".gz","")),sep=sep,index=False)
        

# def transforming_item_data(path,raw_path,process_path,sep,pattern,headers=None):
#         files = os.listdir(os.path.join(path + raw_path))
#         match_files = [fn for fn in files if re.match('|'.join(pattern),fn)]
#         for fn in match_files:
#             if fn.startswith('Price'):
#                 print(f"Processing files {fn}")
#                 df = pd.read_csv(os.path.join(path,raw_path,fn),sep=sep, 
#                                  compression='gzip',encoding='latin-1', na_values=' ')
#                 df['BMSMMultiple'] = df['BMSMMultiple'].astype('Int64')
#                 df['filename'] = fn
#                 df['created_at'] = date.today().strftime('%Y-%m-%d')
#                 df.to_csv(os.path.join(path,process_path,fn.replace(".gz","")),sep=sep,index=False)
#             elif fn.startswith('Product'):
#                 print(f"Processing files {fn}")
#                 df = pd.read_csv(os.path.join(path,raw_path,fn),sep=sep,encoding='latin-1', na_values=' ')
#                 df['FamilyCode'] = df['FamilyCode'].astype('Int64')
#                 df['Department'] = df['Department'].astype('Int64')
#                 df['filename'] = fn
#                 df['created_at'] = date.today().strftime('%Y-%m-%d')
#                 df.to_csv(os.path.join(path,process_path,fn),sep=sep,index=False)
#             else:
#                 print(f"Processing files {fn}")
#                 df = pd.read_csv(os.path.join(path,raw_path,fn),sep=sep,encoding='latin-1', na_values=' ')
#                 df['filename'] = fn
#                 df['created_at'] = date.today().strftime('%Y-%m-%d')
#                 df.to_csv(os.path.join(path,process_path,fn),sep=sep,index=False)
        
    
    
# ###############################################################

# def query_df_callable(connection, query_list):
#     try:
#         curr = connection.cursor()
#         final_df = pd.DataFrame()
#         for qry in query_list:
#             curr.execute(qry)
#             connection.commit()
#             col_names = ','.join([col[0] for col in curr.description]).lower()
#             for c in curr:
#                 df = pd.DataFrame(c, columns=[col_names])
#                 final_df = pd.concat([final_df,df], axis=1)
#         return(final_df)
#         curr.close()
#     except psycopg2.DatabaseError as error:
#           raise AirflowException(f"psycopg2.DatabaseError({error}).")


# def run_rds_multple_queries(connection,path_name,file,**kwargs):
#     curr = connection.cursor()
#     scriptContents = pathlib.Path(os.path.join(path_name,'rds_script_files',file)).read_text(encoding='utf-8')
#     sqlStatements = scriptContents.split(sep=';')
#     for statement in sqlStatements[:-1]:
#         curr.execute(f'{statement.format(**kwargs)}' + ';')
#         connection.commit()




# def run_rds_cmd(path, **context):
    
#     rds_dev_con = get_redshift_connect_string('dev')
#     rds_ent_con = get_redshift_connect_string('blueprod')
    
#     # loading snf sales data into staging dev
#     run_rds_multple_queries(rds_dev_con,path,'rds_master_file_stg.sql',
#                            aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
    
#     qry_list = ["select  min(business_date) as min_date from smartnfinal.tenders_staging",
#                 "select  max(transaction_date) as max_date from smartnfinal.olap_item_details",
#                 "select min(business_date) as from_date  from smartnfinal.tenders_staging",
#                 "select max(business_date) as to_date  from smartnfinal.tenders_staging",
#                 "select count(*) as new_prod_count from smartnfinal.dim_item_new ;",
#                 "select count(*) as old_prod_count from smartnfinal.dim_products;",
#                 "select count(*) as new_store_count from smartnfinal.dim_location_new;",
#                 "select count(*) as old_store_count from smartnfinal.dim_store;"]
    
        
#     final_df = query_df_callable(rds_dev_con, qry_list)
            
#     min_date = str(final_df.min_date[0])
#     max_date = str(final_df.max_date[0])
#     from_date = str(final_df.from_date[0])
#     to_date = str(final_df.to_date[0])
#     new_prod_count = int(final_df.new_prod_count)
#     old_prod_count = int(final_df.old_prod_count)
#     new_store_count = int(final_df.new_store_count)
#     old_store_count = int(final_df.old_store_count)
   
            
#     received_response = context['ti'].xcom_pull(key='response')
    
#     print(f'tenders_staging min_date {min_date}')
#     print(f'olap_item_details max_date {max_date}')
    
#     #-------- olap item details dev command------------------#
    
#     if min_date == max_date:
        
#         run_rds_multple_queries(rds_dev_con,path,'rds_inserting_master_file.sql', mindate = min_date)

#         if received_response == 'Files_Available':

#             if new_prod_count >= old_prod_count:
#                 run_rds_multple_queries(rds_dev_con,path,'rds_dim_product.sql')
#             else: sender_msg(prod_count_miss_match_msg, 'sudhakar.g@hypersonix.io')

#             if new_store_count <= old_store_count:
#                 run_rds_multple_queries(rds_dev_con,path,'rds_store_update.sql')
#             else: sender_msg(store_count_miss_match_msg, 'sudhakar.g@hypersonix.io')

#             run_rds_multple_queries(rds_dev_con,path,'rds_olap_and_other_query.sql', mindate = min_date,
#                                    fromdate = from_date, todate = to_date,
#                                    aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)


#         else:
#             run_rds_multple_queries(rds_dev_con,path,'rds_olap_and_other_query.sql', mindate = min_date,
#                                    fromdate = from_date, todate = to_date,
#                                    aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)

#         #-------- loading olap item details and other table enterprise command------------------#

#         run_rds_multple_queries(rds_ent_con,path,'rds_ent_copy_sales_olap.sql', maxdate = min_date,
#                                    aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)

#         rds_dev_con.close()
#         rds_ent_con.close()

#     else:
#         raise AirflowException(f"Min and Max Date is different of olap table and tender staging table")
        
      
   

            
# # def run_ecom_flash (path):
    
# #     qry_list = ["select max(transaction_date) as max_date from smartnfinal.ecommerce_flash;"]
    
# #     rds_dev_con = get_redshift_connect_string('dev')
# #     rds_ent_con = get_redshift_connect_string('blueprod')
    
# #     final_df = query_df_callable(rds_dev_con, qry_list)
# #     max_date = str(final_df.max_date[0])
    
# #     dev_curr = rds_dev_con.cursor()
# #     dev_curr.execute("""delete from smartnfinal.ecommerce_flash where transaction_date >= '{}';""".format(max_date))
# #     rds_dev_con.commit()
    
# #     run_rds_multple_queries(rds_dev_con,path,'rds_ecom_flash.sql', maxdate = max_date,
# #                            aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
    
# #     ## -----------copying ecommerce_flash data into ent
    
# #     ent_curr = rds_ent_con.cursor()
# #     ent_curr.execute("""delete from smartnfinal.ecommerce_flash where transaction_date >= '{}';""".format(max_date))
# #     rds_ent_con.commit()
# #     ent_curr.execute(copy_qry.format('ecommerce_flash','ecommerce_flash_',s3_access_id,s3_secret_key,'|',0))
# #     rds_ent_con.commit()
    
# #     ent_curr.close()
# #     dev_curr.close()
# #     rds_dev_con.close()
# #     rds_ent_con.close()


    

# def loading_item_files(path):
    
#     rds_dev_con = get_redshift_connect_string('dev')
            
#     run_rds_multple_queries(rds_dev_con,path,'rds_item_files_loading_db.sql',
#                            aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
#     rds_dev_con.close()
    
    
# def collect_info(dict_file,query,conn):
#     df = pd.DataFrame()
#     idx = 0
#     for tbl,col in dict_file.items():
#         df1 = pd.read_sql(query.format(col_name =col ,table_name = tbl), conn)
#         df1.insert(loc=idx, column='table_name', value=tbl)
#         df = pd.concat([df,df1], axis=0).reset_index(drop=True)
#     return(df)
    
# snf_sales_dict = {'tenders':'business_date','transactions':'business_date',
#               'item_discounts':'business_date','transaction_details':'businessdate',
#               'olap_item_details':'transaction_date'}

# snf_item_dict = {'price_new':'filename','dim_item_new':'filename',
#               'dim_location_new':'filename','dim_category_new':'filename',
#               'caseitem_sale':'filename'}
    
# def success_dag_mail(path_name):
#     rds_dev_con = get_redshift_connect_string('dev')
    
#     try:
#         date_query = """select {col_name} as max_date, count(*), created_at
#                         from smartnfinal.{table_name}
#                         group by {col_name},created_at order by {col_name} desc limit 2;"""

#         df1 = collect_info(snf_sales_dict,date_query,rds_dev_con)
#         df1.to_html(os.path.join(path_name,'html_files','snf_sales_file.html'),justify='center', col_space=100,index=False)
#         sender_msg(sales_sucess_msg,
#                    'sudhakar.g@hypersonix.io,meenakshi@hypersonix.io,\
#                    navaneeth@hypersonix.ai,amarjeet.s@hypersonix.ai,padmanaban@hypersonix.ai,vishnubala@hypersonix.ai',
#                    is_html=True,html_file= 'snf_sales_file.html')

#         file_query = """select {col_name}, count(*), created_at 
#                         from smartnfinal.{table_name} pn2 
#                         group by {col_name},created_at order by {col_name} desc limit 1;"""

#         df2 = collect_info(snf_item_dict,file_query,rds_dev_con)
#         df2.to_html(os.path.join(path_name,'html_files','snf_item_file.html'),justify='center', col_space=100,index=False)
#         sender_msg(item_sucess_msg,
#                    'sudhakar.g@hypersonix.io,meenakshi@hypersonix.io,\
#                    navaneeth@hypersonix.ai,amarjeet.s@hypersonix.ai,padmanaban@hypersonix.ai,vishnubala@hypersonix.ai',
#                    is_html=True,html_file= 'snf_item_file.html') 
    
#     except :
#         raise AirflowException(f"HTML Mail ERROR")
    



        
    
# ##########################################

# default_args = {'owner': 'Sudhakar','depends_on_past': True,
#                'email': ['sudhakar.g@hypersonix.io'],'email_on_failure': True}


# dag = DAG(
#     dag_id = 'rds_snf_sale',
#     default_args=default_args,
#     start_date = datetime(2021,1,20),
#     schedule_interval = '@once')



# downloading_sales_files_task = PythonOperator(
#     task_id = 'downloading_sales_files_task',
#     provide_context=True,
#     python_callable =sftp_download,
#     retry_delay = timedelta(minutes=10),
#     retries =  9,
#     op_kwargs={
#         'path':path,
#         'u_name' : username,
#         'p_word' : password,
#         'msg1': sales_miss_match_msg ,
#         'msg2': sales_sucess_msg,
#         'files_name': sales_files ,
#         'comp_size_files':sales_comp_size_dict,
#         'sftp_dir': '/upload/',
#         'val_path': 'rds_validation_files',
#         'raw_path': 'rds_raw_files',
#         'val_file': 'rds_sales_file_val.json',
#     },
#     dag = dag)

# downloading_item_files_task = PythonOperator(
#     task_id = 'downloading_item_files',
#     provide_context=True,
#     python_callable =sftp_download,
#     retry_delay = timedelta(minutes=10),
#     retries = 9,
#     op_kwargs={
#         'path':path,
#         'u_name' : username,
#         'p_word' : password,
#         'msg1': item_miss_match_msg ,
#         'msg2': item_sucess_msg,
#         'files_name': item_files ,
#         'comp_size_files':item_comp_size_dict,
#         'sftp_dir': '/upload/',
#         'val_path': 'rds_validation_files',
#         'raw_path': 'rds_raw_files',
#         'val_file': 'rds_items_file_val.json',
#     },
#     dag = dag)

# downloading_case_file_task = PythonOperator(
#     task_id = 'downloading_caseitem_files',
#     provide_context=True,
#     python_callable =sftp_download,
#     retry_delay = timedelta(minutes=10),
#     retries = 9,
#     op_kwargs={
#         'path':path,
#         'u_name' : username,
#         'p_word' : password,
#         'msg1': item_miss_match_msg ,
#         'msg2': item_sucess_msg,
#         'files_name': case_file ,
#         'comp_size_files':case_comp_size_dict,
#         'sftp_dir': '/upload/',
#         'val_path': 'rds_validation_files',
#         'raw_path': 'rds_raw_files',
#         'val_file': 'rds_case_file_val.json',
#     },
#     dag = dag)


# branch_task = BranchPythonOperator(
#     task_id='branch_sftp_check',
#     python_callable=branch_sftp_check,
#     trigger_rule='none_skipped',
#     provide_context=True,
#     dag=dag
#   )

# branch_items = DummyOperator(task_id='run_items', dag=dag)
# branch_sales = DummyOperator(task_id='run_sales', dag=dag)
      

# p_sales_files_task = PythonOperator(
#     task_id = 'transforming_sales_raw_files',
#     python_callable =transform_sales_data,
#     trigger_rule='none_failed_or_skipped',
#     op_kwargs={'path':path,'sep': '|','pattern':sales_files,
#               'raw_path':'rds_raw_files','process_path':'rds_process_files'},
#     dag = dag)

# p_items_files_task = PythonOperator(
#     task_id = 'transforming_items_raw_files',
#     python_callable =transforming_item_data,
#     op_kwargs={'path': path,'sep': '|', 'pattern':item_files,'headers':True,
#               'raw_path':'rds_raw_files','process_path':'rds_process_files'},
#     dag = dag)

# p_case_file_task = PythonOperator(
#     task_id = 'transforming_case_raw_files',
#     python_callable =transforming_data,
#     op_kwargs={'path': path,'sep': '|', 'pattern':case_file,'headers':True,
#               'raw_path':'rds_raw_files','process_path':'rds_process_files'},
#     dag = dag)

# loading_items_files_task = PythonOperator(
#     task_id = 'loading_items_files_into_sfdb',
#     python_callable =loading_item_files,
#     op_kwargs={'path': path},
#     dag = dag)


# s3_case_sales_transfer_task = PythonOperator(
#     task_id = 'loading_case_sales_files_into_s3',
#     python_callable =upload_file_to_S3,
#     op_kwargs={'path': path, 'pattern': sales_files + case_file,
#                'bucket_name':bucket_name,'bucket_key':bucket_key,'process_path':'rds_process_files'},
#     dag = dag)

# s3_items_transfer_task = PythonOperator(
#     task_id = 'loading_items_files_into_s3',
#     python_callable =upload_file_to_S3,
#     op_kwargs={'path': path, 'pattern':item_files,
#               'bucket_name':bucket_name,'bucket_key':bucket_key,'process_path':'rds_process_files'},
#     dag = dag)

       
# make_olap_task = PythonOperator(
#     task_id = 'creating_olap_into_redshift',
#     python_callable =run_rds_cmd,
#     provide_context=True,
#     op_kwargs={'path':path},
#     dag = dag)

# # ecomm_flash_task = PythonOperator(
# #     task_id = 'creating_ecomm_flash_report',
# #     python_callable =run_ecom_flash,
# #     op_kwargs={'path':path},
# #     dag = dag)


# remove_source_files_task = PythonOperator(
#     task_id = 'deleting_raw_process_s3_files',
#     python_callable =removing_raw_process_s3_files,
#     op_kwargs={'path': path,'pattern':sales_files + item_files + s3_del_files,
#                'bucket_name':bucket_name,'bucket_key':bucket_key,
#                'raw_path':'rds_raw_files','process_path':'rds_process_files'},
#     dag = dag)

# success_dag_mail_task = PythonOperator(
#     task_id = 'success_dag_mail',
#     python_callable =success_dag_mail,
#     op_kwargs={'path_name': html_path},
#     dag = dag)

# downloading_sales_files_task >> downloading_case_file_task >> downloading_item_files_task >> branch_task >> branch_items
# branch_items >> p_items_files_task  >>  s3_items_transfer_task >> loading_items_files_task >>  p_sales_files_task
# p_sales_files_task >> p_case_file_task >> s3_case_sales_transfer_task
# s3_case_sales_transfer_task >> make_olap_task >> remove_source_files_task >>success_dag_mail_task
# branch_task >> branch_sales >> p_sales_files_task >> p_case_file_task >> s3_case_sales_transfer_task >> make_olap_task
# make_olap_task >> remove_source_files_task >> success_dag_mail_task
