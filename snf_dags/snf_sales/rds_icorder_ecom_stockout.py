# import boto3
# import pathlib
# import os
# import psycopg2
# import pandas as pd
# from airflow import DAG
# from datetime import datetime,date,timedelta
# from airflow.utils.dates import days_ago
# from airflow.exceptions import AirflowException
# from airflow.operators import PythonOperator
# from airflow.operators.bash_operator import BashOperator
# from etl_functions import sftp_download
# from etl_functions import transforming_data
# from etl_functions import upload_file_to_S3
# from etl_functions import sender_msg
# from snowflake_connection import upload_to_snowflake
# from etl_functions import removing_raw_process_s3_files
# from configparser import ConfigParser, RawConfigParser
# from airflow.operators.sensors import ExternalTaskSensor
# from etl_functions import get_redshift_connect_string

# ####### importing credential info #############################

# config = RawConfigParser()
# res = config.read('/home/ubuntu/airflow/dags/credential.cfg')

# bucket_name = config.get('S3', 'S3_B_NAME')
# bucket_key = config.get('S3', 'S3_RDS_B_KEY')

# username = config.get('SNF_SFTP', 'USERNAME')
# password = config.get('SNF_SFTP', 'PASSWORD')

# session = boto3.session.Session()
# credentials = session.get_credentials()
# s3_access_id = credentials.access_key
# s3_secret_key = credentials.secret_key

# ####### importing path ########################################

# path = '/home/ubuntu/airflow/dags/snf_sales/'
# html_path = '/home/ubuntu/airflow/dags/'

# files = ["ICOrderHistory", "eCommerceShipmentsHistory"]
# comp_size_dict = {"ICOrderHistory":0, "eCommerceShipmentsHistory":500000}

# key_del = ['ecom_stockout_flash','olap_ecommerce','ecommerce_flash']

# ####### creating email message ################################

# miss_match_msg= dict(Body = 'Please check all the files in sftp',
#                  Subject = 'Miss-Match Alert: Redshift - ICorder and Ecom files size are below the standard size')
# sucess_msg= dict(Body = 'All files Size are matched and loaded sucessfully',
#                  Subject = 'Redshift : SnF ICorder and Ecom files loaded Successfully')

# ####### copy command #########################


# copy_qry = """copy smartnfinal.{0} FROM
#            's3://hypersonixdata/Smart_and_Final/rds_dump/{1}' 
#            access_key_id '{2}' secret_access_key '{3}' dateformat 'auto'
#            maxerror as 0 DELIMITER '{4}' ignoreheader {5} NULL AS 'NULL';"""

# ####### creating extra function for loading data into db #########################

# def copy_cmd_execute (query, db_con):
#     try:
#         curr = db_con.cursor()
#         curr.execute(query)
#         db_con.commit()
#         curr.close()
#     except psycopg2.DatabaseError as error:
#         raise AirflowException(f"psycopg2.DatabaseError({error}).")


# def run_rds_multple_queries(connection,path_name,file,**kwargs):
#     try:
#         curr = connection.cursor()
#         scriptContents = pathlib.Path(os.path.join(path_name,'rds_script_files',file)).read_text(encoding='utf-8')
#         sqlStatements = scriptContents.split(sep=';')
#         for statement in sqlStatements[:-1]:
#             curr.execute(f'{statement.format(**kwargs)}' + ';')
#             connection.commit()
#     except psycopg2.DatabaseError as error:
#         raise AirflowException(f"psycopg2.DatabaseError({error}).")
        

# def query_df_callable(connection, query_list):
#     curr = connection.cursor()
#     final_df = pd.DataFrame()
#     for qry in query_list:
#         curr.execute(qry)
#         connection.commit()
#         col_names = ','.join([col[0] for col in curr.description]).lower()
#         for c in curr:
#             df = pd.DataFrame(c, columns=[col_names])
#             final_df = pd.concat([final_df,df], axis=1)
#     return(final_df)


# def run_rds_cmd(path):
    
#     qry_list = ["select max(daydate) as ic_max_date from smartnfinal.ico_order",
#                 "select  max(daydate) as ecom_max_date from smartnfinal.ecommerce",
#                 "select  max(transaction_date) as stock_max_date from smartnfinal.ecom_stockout_flash"]

#     rds_dev_con = get_redshift_connect_string('dev')

#     rds_ent_con = get_redshift_connect_string('blueprod')

#     final_df = query_df_callable(rds_dev_con, qry_list)

#     ic_max_date = str(final_df.ic_max_date[0])
#     ecom_max_date = str(final_df.ecom_max_date[0])
#     stock_max_date = str(final_df.stock_max_date[0])

#     print("ic_order dev command")
#     copy_cmd_execute("""delete from smartnfinal.ico_order where daydate >= '{}';""".format(ic_max_date),rds_dev_con)
#     copy_cmd_execute(copy_qry.format('ico_order','ICOrder',s3_access_id,s3_secret_key, '|',1),rds_dev_con)

#     print("ecommerce dev command")
#     copy_cmd_execute("""delete from smartnfinal.ecommerce where daydate >= '{}';""".format(ecom_max_date),rds_dev_con)
#     copy_cmd_execute(copy_qry.format('ecommerce','eCommerce',s3_access_id,s3_secret_key,',',1),rds_dev_con)

#     print("olap ecommerce dev command")
#     run_rds_multple_queries(rds_dev_con,path,'rds_olap_ecom.sql', max_date = ecom_max_date,
#                            aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)

#     print("olap ecommerce enterprise command")  
#     copy_cmd_execute("""delete from smartnfinal.olap_ecommerce 
#                         where transaction_date  >= '{}';""".format(ecom_max_date),rds_ent_con)
#     copy_cmd_execute(copy_qry.format('olap_ecommerce','olap_ecommerce_',s3_access_id,
#                                      s3_secret_key, '|',0),rds_ent_con)

#     print("ecom stockout flash dev command") 
#     run_rds_multple_queries(rds_dev_con,path,'rds_dev_ecom_stockout.sql', max_date = stock_max_date,
#                            aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)

#     print("ecommerce stockout_flash enterprise command")
    
#     copy_cmd_execute("""delete from smartnfinal.ecom_stockout_flash 
#                         where transaction_date  >= '{}';""".format(stock_max_date),rds_ent_con)
#     copy_cmd_execute(copy_qry.format('ecom_stockout_flash','ecom_stockout_flash_',s3_access_id,
#                                      s3_secret_key, '|',0),rds_ent_con)
    
# def run_ecom_flash (path):
    
#     qry_list = ["select max(transaction_date) as max_date from smartnfinal.ecommerce_flash;"]
    
#     rds_dev_con = get_redshift_connect_string('dev')
#     rds_ent_con = get_redshift_connect_string('blueprod')
    
#     final_df = query_df_callable(rds_dev_con, qry_list)
#     max_date = str(final_df.max_date[0])
    
#     dev_curr = rds_dev_con.cursor()
#     dev_curr.execute("""delete from smartnfinal.ecommerce_flash where transaction_date >= '{}';""".format(max_date))
#     rds_dev_con.commit()
    
#     run_rds_multple_queries(rds_dev_con,path,'rds_ecom_flash.sql', maxdate = max_date,
#                            aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
    
#     ## -----------copying ecommerce_flash data into ent
    
#     ent_curr = rds_ent_con.cursor()
#     ent_curr.execute("""delete from smartnfinal.ecommerce_flash where transaction_date >= '{}';""".format(max_date))
#     rds_ent_con.commit()
#     ent_curr.execute(copy_qry.format('ecommerce_flash','ecommerce_flash_',s3_access_id,s3_secret_key,'|',0))
#     rds_ent_con.commit()
    
#     ent_curr.close()
#     dev_curr.close()
#     rds_dev_con.close()
#     rds_ent_con.close()

    
# def collect_info(dict_file,query,conn):
#     df = pd.DataFrame()
#     idx = 0
#     for tbl,col in dict_file.items():
#         df1 = pd.read_sql(query.format(col_name =col ,table_name = tbl), conn)
#         df1.insert(loc=idx, column='table_name', value=tbl)
#         df = pd.concat([df,df1], axis=0).reset_index(drop=True)
#     return(df)

# snf_icorder_ecom_dict = {'ico_order':'daydate','ecommerce':'daydate','olap_ecommerce':'transaction_date',
#                          'ecom_stockout_flash':'transaction_date','ecommerce_flash':'transaction_date'}


# def success_dag_mail(path_name):
#     rds_dev_con = get_redshift_connect_string('dev')
    
#     try:
#         date_query = """select {col_name} as max_date, count(*) as count, created_at 
#                         from smartnfinal.{table_name} 
#                         group by {col_name},created_at order by {col_name} desc limit 2;"""

#         df1 = collect_info(snf_icorder_ecom_dict,date_query,rds_dev_con)
#         df1.to_html(os.path.join(path_name,'html_files','rds_icorder_ecom.html'),justify='center',col_space=100,index=False)
#         sender_msg(sucess_msg, 'sudhakar.g@hypersonix.io',is_html=True,html_file= 'rds_icorder_ecom.html')
#     except :
#         raise AirflowException(f"HTML Mail ERROR")


# ###### Creating dags ##########################################

# default_args = {'owner': 'Sudhakar','depends_on_past': True,
#                'email': ['sudhakar.g@hypersonix.io'],'email_on_failure': True}

# dag = DAG(
#     dag_id = 'rds_icorder_ecom_stockout_flash',
#     default_args=default_args,
#     start_date = datetime(2021,4,28),
#     schedule_interval = '00 12 * * *')

# wait_for_sales_file_loaded_task = ExternalTaskSensor(
#     task_id='wait_for_sales_file_loaded',
#     external_dag_id='redshift_snf_sale',
#     external_task_id='success_dag_mail'
# )


# downloading_files_task = PythonOperator(
#     task_id = 'downloading_files_sftp',
#     provide_context=True,
#     python_callable =sftp_download,
#     retry_delay = timedelta(minutes=30),
#     retries = 3,
#     op_kwargs={
#         'path':path,
#         'u_name' : username,
#         'p_word' : password,
#         'msg1': miss_match_msg ,
#         'msg2': sucess_msg,
#         'files_name': files ,
#         'comp_size_files':comp_size_dict,
#         'sftp_dir': '/upload/',
#         'val_path': 'rds_validation_files',
#         'raw_path': 'rds_raw_files',
#         'val_file': 'rds_icorder_ecom_val.json',
#     },
#     dag = dag)

# p_icorder_files_task = PythonOperator(
#     task_id = 'transforming_icorder_raw_files',
#     python_callable =transforming_data,
#     op_kwargs={'path':path,'sep': '|','pattern':files[0], 'headers':True,
#               'raw_path':'rds_raw_files','process_path':'rds_process_files'},
#     dag = dag)

# p_ecom_files_task = PythonOperator(
#     task_id = 'transforming_ecom_raw_files',
#     python_callable =transforming_data,
#     op_kwargs={'path': path,'sep': ',', 'pattern':files[1],
#               'raw_path':'rds_raw_files','process_path':'rds_process_files'},
#     dag = dag)

# s3_transfer_task = PythonOperator(
#     task_id = 'loading_files_into_s3',
#     python_callable =upload_file_to_S3,
#     op_kwargs={'path': path, 'pattern':files,
#                'process_path':'rds_process_files',
#               'bucket_name':bucket_name,'bucket_key':bucket_key},
#     dag = dag)

# redshift_load_task = PythonOperator(
#     task_id = 'copy_file_into_redshift',
#     python_callable =run_rds_cmd,
#     op_kwargs={'path':path},
#     dag = dag)

# ecomm_flash_task = PythonOperator(
#     task_id = 'creating_ecomm_flash_report',
#     python_callable =run_ecom_flash,
#     op_kwargs={'path':path},
#     dag = dag)

# remove_source_files_task = PythonOperator(
#     task_id = 'deleting_raw_process_s3_files',
#     python_callable =removing_raw_process_s3_files,
#     op_kwargs={'path': path,'pattern':files + key_del,
#               'bucket_name':bucket_name,'bucket_key':bucket_key,
#               'raw_path':'rds_raw_files','process_path':'rds_process_files'},
#     dag = dag)

# success_dag_mail_task = PythonOperator(
#     task_id = 'success_dag_mail',
#     python_callable =success_dag_mail,
#     op_kwargs={'path_name': html_path},
#     dag = dag)

# wait_for_sales_file_loaded_task >> downloading_files_task >> p_icorder_files_task >> p_ecom_files_task >> s3_transfer_task  >> redshift_load_task >> ecomm_flash_task >> remove_source_files_task >> success_dag_mail_task

