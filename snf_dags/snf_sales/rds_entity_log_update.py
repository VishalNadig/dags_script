import boto3
import pathlib
import os
import re
import json
import paramiko
import pandas as pd

import psycopg2
import pymysql
from sqlalchemy import create_engine

from airflow import DAG
from snf_dags.etl_functions import sender_msg
from datetime import datetime, date , timedelta
from airflow.utils.dates import days_ago

from snf_dags.etl_functions import sf_connect_sting

from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python import PythonOperator,BranchPythonOperator
from configparser import ConfigParser, RawConfigParser




####### importing credential info #############################

config = RawConfigParser()
res = config.read('/home/ubuntu/airflow/dags/snf_dags/credential.ini')

sucess_msg= dict(Body = 'Sucessfully updated MySql entities in Dev, Stage and Green MySql Environment',
                 Subject = 'Redshift: Successfully updated MySql entities')

rds_dev_host = config.get('DEV_REDSHIFT', 'HOST')
rds_user_name = config.get('DEV_REDSHIFT', 'USERNAME')
rds_pass_word = config.get('DEV_REDSHIFT', 'PASSWORD')
rds_database = config.get('DEV_REDSHIFT', 'DATABASE')


user_name = config.get('DEV_MYSQL', 'USERNAME')
pass_word = config.get('DEV_MYSQL', 'PASSWORD')

dev_host = config.get('DEV_MYSQL', 'HOST')
stage_host = config.get('STAGE_MYSQL', 'HOST')
green_host = config.get('GREEN_MYSQL', 'HOST')
blue_host = config.get('BLUE_MYSQL', 'HOST')

bucket_name = config.get('S3', 'S3_B_NAME')
bucket_key = config.get('S3', 'S3_RDS_B_KEY')

session = boto3.session.Session('s3')
credentials = session.get_credentials()
s3_access_id = credentials.access_key
s3_secret_key = credentials.secret_key

host_list= [dev_host,stage_host,blue_host,green_host]

########### query command ######################


def updating_mysql_entity(list_of_host, username, password):

    sf_con = sf_connect_sting()
    curr = sf_con.cursor()
    curr.execute('USE DATABASE %s;' % 'dev_smartnfinal')
    curr.execute('USE WAREHOUSE %s;' % 'dev')


    qry = "select * from dev_smartnfinal.public.entity_log"

    df = pd.read_sql(qry, sf_con)
    df.columns = df.columns.str.lower()
    print(df)

    for h_lst in list_of_host:
        try:
            engine = create_engine('mysql+mysqlconnector://'+username+':'+password+'@'+h_lst+'/smartnfinal')
            df.to_sql(name='entity_log', con=engine, if_exists = 'replace', index=False)
            engine.dispose()
        except Exception as err:
            print("logging into {} host has thrown error: {}".format(h_lst,err))

    sender_msg(sucess_msg,'v_arush.s@hypersonix.ai')


###### Creating dags ##########################################
# 00 16 * * *

default_args = {'owner': 'Sudhakar','depends_on_past': True,
                'email': ['sudhakar.g@hypersonix.io'],'email_on_failure': True}

dag = DAG(
    dag_id = 'sf_entity_log_updates',
    default_args=default_args,
    start_date = datetime(2021,9,2),
    schedule_interval = '00 16 * * *')


run_update_task = DummyOperator(task_id='run_update', dag=dag)

updating_mysql_entities_task = PythonOperator(
    task_id = 'updating_mysql_entities',
    python_callable =updating_mysql_entity,
    op_kwargs={'list_of_host':host_list,
               'username':user_name,'password':pass_word},
    dag = dag)


run_update_task >> updating_mysql_entities_task

