# import re
# import json
# import os
# import pandas as pd
# import base64
# import pathlib
# import paramiko
# import functools
# from datetime import datetime,date, timedelta

# import mimetypes
# import smtplib, ssl
# from email import encoders
# from email.header import Header
# from email.message import Message
# from smtplib import SMTPException
# from email.utils import formataddr
# from email.mime.text import MIMEText
# from email.mime.base import MIMEBase
# from email.mime.audio import MIMEAudio
# from email.mime.image import MIMEImage
# from email.mime.multipart import MIMEMultipart
# from configparser import ConfigParser, RawConfigParser

# from airflow import DAG
# from airflow.operators.dummy_operator import DummyOperator
# from airflow.operators import PythonOperator,BranchPythonOperator
# from configparser import ConfigParser, RawConfigParser


# path = '/home/ubuntu/airflow/dags/snf_sales/'

# config = RawConfigParser()
# res = config.read('/home/ubuntu/airflow/dags/credential.cfg')

# sender_host = config.get('EMAIL', 'EMAIL_HOST')
# sender_email = config.get('EMAIL', 'EMAIL_SENDER')
# sender_username = config.get('EMAIL', 'EMAIL_USERNAME')
# sender_password = config.get('EMAIL', 'EMAIL_PASSWORD')

# username = config.get('SNF_SFTP', 'USERNAME')
# password = config.get('SNF_SFTP', 'PASSWORD')


# files = ['ItemDiscounts','Transactions', 'TransactionDetails', 'Tenders']

# comp_size_dict = {'ItemDiscounts': 3000000, 'Transactions': 3000000,
#                   'TransactionDetails': 15000000,'Tenders': 2000000}


# ####### creating email message ################################

# not_success_msg= dict(Subject = """Alert: Missing SnF Sales Data for {}""",
#                       From= 'Update: SnF Sales Availablity in SFTP')

# miss_match_msg= dict(Body = 'Please check all files in sftp',
#                  Subject = 'Alert: files size are below the standard size ')
# sucess_msg= dict(Body = 'All files Size are matched',
#                  Subject = 'SnF Sales files loaded Successfully in SFTP',
#                  From= 'SnF Sales files download update')


# def load_json_file(p_name,fname):
#     with open(os.path.join(p_name,'validation_files',fname), 'r') as f:
#         d = json.load(f)
#     return d

# class AllowAnythingPolicy(paramiko.MissingHostKeyPolicy):
#     def missing_host_key(self, client, hostname, key):
#         return

# def sender_msg(msg_dict, recipients, is_missing=None): 
#     yesterday = date.today() - timedelta(days=1)
#     snf_sales_date = yesterday.strftime("%Y-%m-%d")
    
#     if is_missing is not None:
#         try:
#             html = open(os.path.join('/home/ubuntu/airflow/dags/html_files',"Missing_Sales_Report.html"))
#             new_html = html.read().replace('missing_date',snf_sales_date)
#             msg = MIMEText(new_html, 'html')

#             msg['Subject'] = msg_dict.get('Subject').format(snf_sales_date)
#             msg['From'] = sender_email
#             msg['To'] = (', ').join(recipients.split(','))

#             server = smtplib.SMTP(sender_host, 587,timeout = 10)
#             server.starttls()
#             server.ehlo()
#             server.login(sender_username,sender_password)
#             server.send_message(msg)
#             server.quit()
#         except smtplib.SMTPException as err:
#             print (f'Error: unable to send email {err}')

#     else:
#         try:
#             body = """ {0} """.format(msg_dict.get('Body').format(snf_sales_date))
#             msg = MIMEMultipart()

#             msg['Subject'] = msg_dict.get('Subject').format(snf_sales_date)
#             msg['From'] = formataddr((str(Header(msg_dict.get('From'), 'utf-8')), sender_email)) 
#             msg['To'] = (', ').join(recipients.split(','))

#             msg.attach(MIMEText(body,'plain'))

#             server = smtplib.SMTP(sender_host, 587,timeout = 10)
#             server.starttls()
#             server.ehlo()
#             server.login(sender_username,sender_password)
#             server.send_message(msg)
#             server.quit()
#         except smtplib.SMTPException as err:
#             print (f'Error: unable to send email {err}')
        



# def hasNumbers(inputString):
#     return bool(re.search(r'\d', inputString))


# def sftp_download(path,msg1,msg2,msg3,comp_size_files,files_name,val_file):
#     execution_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    
#     df = load_json_file(path,val_file)
    
#     client = paramiko.SSHClient()
#     client.set_missing_host_key_policy(AllowAnythingPolicy())
#     client.connect('ftp.hypersonix.io', username= username, password=password,banner_timeout=200)
#     sftp = client.open_sftp()
    
#     sftp.chdir('upload/')
    
#     new_files_list = []
#     f_dict = {}
#     for fname in files_name:
#         for fileattr in sftp.listdir_attr():
#             if fileattr.filename.startswith(fname) and fileattr.filename.endswith(".csv") \
#             or fileattr.filename.endswith(".gz") :
#                 latest = fileattr.st_mtime
#                 latestfile = fileattr.filename
#                 latestsize = fileattr.st_size
#                 f_dict[latestfile] =latestsize
#         files_list = [k for k in f_dict.keys() if k.startswith(fname)]
                
#         new_list = list(set(files_list) - set(df[fname]))
#         new_files_list.extend(new_list)
#         df[fname].extend(new_list)

#     file_size_dict = dict([[key,val] for key, val in f_dict.items() if key in new_files_list])
#     if len(file_size_dict.keys()) < len(files_name):
#         print("No New File Available")
#         sender_msg(msg1,
#                    "sudhakar.g@hypersonix.io,rama@hypersonix.ai,navaneeth@hypersonix.ai,amarjeet.s@hypersonix.ai", 
#                    is_missing=True)
#     else:
#         nof= len(file_size_dict.keys())
#         print(f"We have received {nof} files now checking file size ....")
#         chk_size = []
#         for k in file_size_dict:
#             if hasNumbers(k):
#                 if k[:re.search(r"\d",k).start() -1] in comp_size_files:
#                     if file_size_dict.get(k) > comp_size_files.get(k[:re.search(r"\d",k).start() -1]):
#                         chk_size.append(k)
#             else:
#                 if any(k.startswith(key) for key in comp_size_files):
#                     match_k  = ''.join([key for key in comp_size_files if k.startswith(key)])
#                     if file_size_dict.get(k) > comp_size_files.get(match_k):
#                         chk_size.append(k)
                
#         if len(chk_size) < len(files_name):
#             sender_msg(msg2,"sudhakar.g@hypersonix.io")
#         else:
#             sender_msg(msg3,"sudhakar.g@hypersonix.io")



# ###### Creating dags ##########################################

# default_args = {'owner': 'Sudhakar','depends_on_past': True,
#                'email': ['sudhakar.g@hypersonix.io'],'email_on_failure': True}

# dag = DAG(
#     dag_id = 'sf_snf_sales_check_file',
#     default_args=default_args,
#     start_date = datetime(2021,1,22),
#     schedule_interval = '00 12 * * *')

# run_check_sftp_task = DummyOperator(task_id='run_update', dag=dag)

# p_check_sales_files_task = PythonOperator(
#     task_id = 'checking_snf_sales_files_in_sftp',
#     python_callable =sftp_download,
#     op_kwargs={'path':path,'msg1':not_success_msg,'msg2':miss_match_msg,
#                'msg3':sucess_msg,'comp_size_files':comp_size_dict,
#                'val_file':'snf_sales_file_val.json','files_name':files},
#     dag = dag)



# run_check_sftp_task >> p_check_sales_files_task 


    
                

    
    



