truncate table smartnfinal.tenders_staging;
truncate table smartnfinal.transactions_staging;
truncate table smartnfinal.transaction_details_staging;
truncate table smartnfinal.item_discounts_staging;

copy smartnfinal.transaction_details_staging FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/TransactionDetails' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
TIMEFORMAT AS 'YYYY-MM-DD HH:MI:SS' EMPTYASNULL maxerror as 0 DELIMITER '|' ignoreheader 1;


copy smartnfinal.transactions_staging FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/Transactions' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
TIMEFORMAT AS 'YYYY-MM-DD HH:MI:SS' EMPTYASNULL maxerror as 0 DELIMITER '|' ignoreheader 1;


copy smartnfinal.tenders_staging FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/Tenders' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
TIMEFORMAT AS 'YYYY-MM-DD HH:MI:SS' EMPTYASNULL maxerror as 0 DELIMITER '|' ignoreheader 1;


copy smartnfinal.item_discounts_staging FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/ItemDiscounts' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
TIMEFORMAT AS 'YYYY-MM-DD HH:MI:SS' EMPTYASNULL maxerror as 0 DELIMITER '|' ignoreheader 1;

copy smartnfinal.caseitem_sale FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/CaseItemSales' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
maxerror as 0 DELIMITER '|' EMPTYASNULL ignoreheader 1;
