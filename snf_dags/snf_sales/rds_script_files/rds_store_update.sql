update smartnfinal.dim_store 
set  
company_id=l.company_number, 
company_name=l.company, 
region_code=l.region_code, 
region=l.region, 
district_code=l.district_code, 
district=l.district, 
storeformat=l.store_format, 
priorstore_number=l.prior_storenumber, 
store_name=initcap(l.store_name), 
pricing_zone=l.pricing_zone, 
pricing_zone_name =p.pricing_zone_name,
storeopen_date=l.store_opendate, 
storeclose_date=l.store_closedate, 
address=l.address, 
city=l.city, 
state=l.state, 
zip=l.zipcode, 
filename=l.filename, 
created_at=l.created_at 
from smartnfinal.dim_store s  
join smartnfinal.dim_location_new l on s.store_number=l.store_number
join smartnfinal.dim_price_zone p on l.pricing_zone =p.pricing_zone; 

 
update smartnfinal.dim_store_new set 
company_id=d.company_id, 
company_name=d.company_name, 
region_code=d.region_code, 
region=d.region, 
district_code=d.district_code, 
district=d.district, 
storeformat=d.storeformat, 
store_name_original=d.store_name, 
pricing_zone=d.pricing_zone, 
pricing_zone_name =p.pricing_zone_name,
address=d.address, 
city=d.city, 
state=d.state, 
zip=d.zip, 
state_name=d.state_name, 
store_name=right('000'||d.store_number,3)||' '||d.store_name 
from smartnfinal.dim_store d 
join smartnfinal.dim_store_new s on  d.store_number=s.new_store_number
join smartnfinal.dim_price_zone p on d.pricing_zone =p.pricing_zone; 
