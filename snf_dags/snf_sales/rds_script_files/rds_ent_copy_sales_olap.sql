
delete from smartnfinal.olap_item_details where transaction_date >='{maxdate}';

copy smartnfinal.olap_item_details FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/olap_item_details_sales_' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
maxerror as 0 DELIMITER '|' NULL AS 'NULL';


drop table if EXISTS #pycomp;

SELECT distinct transaction_date as businessdate,old_store_number  as storenumber
into #pycomp
from smartnfinal.olap_item_details ;

update smartnfinal.olap_item_details set py_comp_status=py.comp_status_new
from smartnfinal.olap_item_details o
join (select distinct td.businessdate,td.businessdate-364,sn.new_store_number as store_number,
    case when sn.comp_start_date<=td.businessdate and  sn.storeclose_date>=td.businessdate and DATE_PART(year,sn.storeclose_date)=2050
    then 'Comp' else 'NonComp' end as comp_status_new
    from #pycomp td 
    left join smartnfinal.dim_store s on s.store_number=td.storenumber
    left join smartnfinal.dim_store_new sn on s.store_number=sn.store_number
    where td.businessdate >='{maxdate}')py on o.transaction_date=py.businessdate-364 and o.store_number=py.store_number
    where o.transaction_date=py.businessdate-364 ;


update smartnfinal.entity_log
set row_count =(select count(*) from smartnfinal.olap_item_details),
most_recent_date=(select max(transaction_date) from smartnfinal.olap_item_details),
lud=(select current_date),
min_date = (select min(transaction_date) from smartnfinal.olap_item_details),
most_recent_time = (select max(transaction_date)::timestamp from smartnfinal.olap_item_details),
lud_time=(select current_timestamp)
where entity_name='summary';


truncate table smartnfinal.dim_products;

copy smartnfinal.dim_products FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/dim_products_' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
maxerror as 0 DELIMITER '|'  NULL as 'NULL';

truncate table smartnfinal.dim_store;

copy smartnfinal.dim_store FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/dim_store_orig_' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
maxerror as 0 DELIMITER '|' NULL as 'NULL';

truncate table smartnfinal.dim_store_new;

copy smartnfinal.dim_store_new FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/dim_store_new_' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
maxerror as 0 DELIMITER '|' NULL as 'NULL';

