copy smartnfinal.price_new FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/Price' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
TIMEFORMAT AS 'YYYY-MM-DD HH:MI:SS' maxerror as 0 DELIMITER '|' ignoreheader 1 ;

TRUNCATE table smartnfinal.dim_item_new;

copy smartnfinal.dim_item_new FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/Product'
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
TIMEFORMAT AS 'YYYY-MM-DD HH:MI:SS' ACCEPTINVCHARS maxerror as 0 DELIMITER '|' ignoreheader 1;


TRUNCATE table smartnfinal.dim_location_new;

copy smartnfinal.dim_location_new  FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/Locations' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
TIMEFORMAT AS 'YYYY-MM-DD HH:MI:SS' maxerror as 0 DELIMITER '|' ignoreheader 1;


TRUNCATE table smartnfinal.dim_category_new;

copy smartnfinal.dim_category_new FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/MerchStructure' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
TIMEFORMAT AS 'YYYY-MM-DD HH:MI:SS' ACCEPTINVCHARS maxerror as 0 DELIMITER '|' ignoreheader 1;
