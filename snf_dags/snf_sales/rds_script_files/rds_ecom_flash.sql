drop table if exists #olap;
select distinct transaction_date,store_number,channel_level3 ,record_type
into #olap
from smartnfinal.olap_item_details
where record_type=1;

INSERT INTO smartnfinal.ecommerce_flash
(transaction_date, year, quarter, month_no, week_no, day_of_week, day_name, month_name, month_short, month_year, week_str, fis_p_str, fis_w_str, fis_week, fis_period, fis_quarter, fis_year, named_day, channel_level4, channel_level3, channel_level2, channel_level1, quantity, net_sales, transaction_count, item_count,item_count_per_tran,store_count,created_at)
select 
o.transaction_date,
o.year,
o.quarter,
o.month_no,
o.week_no,
o.day_of_week,
o.day_name,
o.month_name,
o.month_short,
o.month_year,
o.week_str,
o.fis_p_str,
o.fis_w_str,
o.fis_week,
o.fis_period,
o.fis_quarter,
o.fis_year,
o.named_day,
case when o.channel_level3='Shop Direct' then '01 - ShopDirect - B2C Delivery'
	when o.channel_level3='Shop Direct Pickup' then '02 - ShopDirect - B2C Pickup'
	when o.channel_level3='Dynamex' then '03 - ShopDirect - B2B Delivery'
	when o.channel_level3='Express Shopping' then '04 - ShopDirect - B2B Pickup'
	when o.channel_level3='Instacart' then '06 - Instacart Marketplace Delivery'
	when o.channel_level3='Shipt' then '08 - Shipt Marketplace Delivery'
	when o.channel_level3='DoorDash' then '09 - DoorDash Marketplace Delivery'
	when o.channel_level3='Google' then '12 - Google'
else o.channel_level3 end as channel_level4,
o.channel_level3,
o.channel_level2,
o.channel_level1,
sum(o.quantity) as quantity,
sum(o. net_sales)as net_sales,
count(distinct o.date_check_id)as transaction_count,
count(o.item_number)as item_count,
--count(o.item_number)/count(distinct o.date_check_id) as item_count_per_tran,
sum(o.quantity)/count(distinct o.date_check_id) as item_count_per_tran,
count(distinct o.store_number)as store_count,
current_date as created_at
from smartnfinal.olap_item_details o
where o.record_type=1 and  o.created_at= current_date 
group by o.transaction_date,o.year,o.quarter,o.month_no,o.week_no,o.day_of_week,o.day_name,o.month_name,o.month_short,o.month_year,o.week_str,o.fis_p_str,o.fis_w_str,o.fis_week,o.fis_period,o.fis_quarter,o.fis_year,o.named_day
,o.channel_level3,o.channel_level2,o.channel_level1 ;

INSERT INTO smartnfinal.ecommerce_flash
(transaction_date, year, quarter, month_no, week_no, day_of_week, day_name, month_name, month_short, month_year, week_str, fis_p_str, fis_w_str, fis_week, fis_period, fis_quarter, fis_year, named_day,  channel_level3, channel_level2, channel_level1,channel_level4, partner, net_sales,quantity,transaction_count,item_count_per_tran,store_count,created_at)
with ioc as(select daydate,partner,sum(netsales)as netsales,sum(total_quantity)as total_quantity,count(distinct orderid)as  transaction_count,
sum(total_quantity)/count(distinct orderid) as item_count_per_tran,count(distinct store_number)as store_count
		from smartnfinal.ico_order
		where created_at= current_date --current_date and daydate<='2019-12-31'
		GROUP by daydate,partner),
olap as(
select 
o.transaction_date,
o.year,
o.quarter,
o.month_no,
o.week_no,
o.day_of_week,
o.day_name,
o.month_name,
o.month_short,
o.month_year,
o.week_str,
o.fis_p_str,
o.fis_w_str,
o.fis_week,
o.fis_period,
o.fis_quarter,
o.fis_year,
o.named_day,
o.channel_level3,
o.channel_level2,
o.channel_level1
from smartnfinal.olap_item_details o
where o.record_type=1  and o.channel_level3='Instacart'
and o.created_at= current_date --and o.transaction_date<='2019-12-31'
group by o.transaction_date,o.year,o.quarter,o.month_no,o.week_no,o.day_of_week,o.day_name,o.month_name,o.month_short,o.month_year,o.week_str,o.fis_p_str,o.fis_w_str,o.fis_week,o.fis_period,o.fis_quarter,o.fis_year,o.named_day
,o.channel_level3,o.channel_level2,o.channel_level1)
select olap.* ,
case when ioc.partner='PBI' then '05 - Instacart PBI – B2C Delivery'
	when  ioc.partner='Instacart Pick Up' then '07 - Instacart Marketplace Pickup'
	else null end as channel_level4,
ioc.partner,
ioc.netsales as net_sales,
ioc.total_quantity as quantity,
ioc.transaction_count as transaction_count,
ioc.item_count_per_tran as item_count_per_tran,
ioc.store_count,
current_date as created_at
from olap 
join ioc on ioc.daydate=olap.transaction_date;


update smartnfinal.ecommerce_flash set 
net_sales=x.dif_net_sales,
quantity=x.dif_quantity,
transaction_count=x.dif_transaction_count,
item_count_per_tran=x.dif_item_count_per_tran
--store_count=x.dif_store_count
from smartnfinal.ecommerce_flash e 
join (with t as(
	select  transaction_date,sum(net_sales )as net_sales,sum(quantity)as quantity,sum(transaction_count)as transaction_count,
	sum(item_count_per_tran)as item_count_per_tran
	from smartnfinal.ecommerce_flash 
	where channel_level4 in ('05 - Instacart PBI – B2C Delivery','07 - Instacart Marketplace Pickup') 
	group by transaction_date
	)
	select f.transaction_date,f.channel_level4,f.net_sales ,(f.net_sales-NVL(t.net_sales,0))as dif_net_sales,
	(f.quantity-NVL(t.quantity,0))as dif_quantity,
	(f.transaction_count-NVL(t.transaction_count,0)) as dif_transaction_count,
	(f.item_count_per_tran-NVL(t.item_count_per_tran,0)) as dif_item_count_per_tran
	--(f.store_count-NVL(t.store_count,0))as dif_store_count
	from smartnfinal.ecommerce_flash f 
	join t on t.transaction_date=f.transaction_date
	where channel_level4='06 - Instacart Marketplace Delivery')x 
on e.transaction_date=x.transaction_date and e.channel_level4=x.channel_level4
where  e.channel_level4='06 - Instacart Marketplace Delivery' and 
e.created_at=current_date --and e.transaction_date<='2019-12-31'
;


insert into smartnfinal.ecommerce_flash(channel_level1,channel_level2,channel_level3,channel_level4,transaction_date,year,quarter,month_no,week_no,day_of_week,day_name,month_name,month_short,month_year,week_str,fis_p_str,fis_w_str,fis_week,fis_period,fis_quarter,fis_year,named_day,created_at)
with main as (
	with channel as (
	select distinct o.channel_level1,o.channel_level2,o.channel_level3,o.channel_level4
	from smartnfinal.ecommerce_flash  o 
	where channel_level4 is not null
	order by o.channel_level4),
	dt as(select distinct o.transaction_date,o.year,o.quarter,o.month_no,o.week_no,o.day_of_week,o.day_name,o.month_name,o.month_short,o.month_year,o.week_str,o.fis_p_str,o.fis_w_str,o.fis_week,o.fis_period,o.fis_quarter,o.fis_year,o.named_day
	from smartnfinal.ecommerce_flash o)
	select * from channel c 
	join dt on 1=1
	--where dt.transaction_date='2020-07-21'
	order by c.channel_level4),
ecom as (select distinct transaction_date,channel_level1,channel_level2,channel_level3,channel_level4
	from smartnfinal.ecommerce_flash
	--where transaction_date='2020-07-21'
	)
select m.*,
current_date as created_at
from main m 
left join ecom e on m.transaction_date=e.transaction_date and m.channel_level4= e.channel_level4
where e.channel_level4 is null;

---------------------------------Updates for WTD,PTD,QTD & YTD--------------



update smartnfinal.ecommerce_flash set 
w_net_sales=case when t.net_sales is null then 0 else t.net_sales end,
w_quantity=case when t.quantity is null then 0 else t.quantity end,
w_item_count=case when t.item_count is null then 0 else t.item_count end,
w_itemcn_pertran=case when t.item_count_per_tran is null then 0 else t.item_count_per_tran end ,
w_tran_count=case when t.transaction_count is null then 0 else t.transaction_count end
from smartnfinal.ecommerce_flash f 
join (
	select m.dt, e.channel_level4,sum(net_sales)as net_sales,sum(quantity)as quantity,sum(transaction_count)as transaction_count,
	sum(item_count)as item_count,sum(item_count_per_tran)as item_count_per_tran
	from smartnfinal.ecommerce_flash e
	 join smartnfinal.date_compare m on 1=1
	where e.transaction_date BETWEEN m.wtd_stdt and wtd_endt and m.dt >='2019-01-01'
	group by 1,2
	)t on f.channel_level4=t.channel_level4 and t.dt=f.transaction_date
where f.created_at=current_date 
;


update smartnfinal.ecommerce_flash set 
p_net_sales=case when t.net_sales is null then 0 else t.net_sales end,
p_quantity=case when t.quantity is null then 0 else t.quantity end,
p_item_count=case when t.item_count is null then 0 else t.item_count end,
p_itemcn_pertran=case when t.item_count_per_tran is null then 0 else t.item_count_per_tran end ,
p_tran_count=case when t.transaction_count is null then 0 else t.transaction_count end
from smartnfinal.ecommerce_flash f 
join (
	select m.dt, e.channel_level4,sum(net_sales)as net_sales,sum(quantity)as quantity,sum(transaction_count)as transaction_count,
	sum(item_count)as item_count,sum(item_count_per_tran)as item_count_per_tran
	from smartnfinal.ecommerce_flash e
	 join smartnfinal.date_compare m on 1=1
	where e.transaction_date BETWEEN m.ptd_stdt and m.ptd_endt and m.dt >='2019-01-01'
	group by 1,2
	)t on f.channel_level4=t.channel_level4 and t.dt=f.transaction_date
where f.created_at=current_date 
;

update smartnfinal.ecommerce_flash set 
q_net_sales=case when t.net_sales is null then 0 else t.net_sales end,
q_quantity=case when t.quantity is null then 0 else t.quantity end,
q_item_count=case when t.item_count is null then 0 else t.item_count end,
q_itemcn_pertran=case when t.item_count_per_tran is null then 0 else t.item_count_per_tran end ,
q_tran_count=case when t.transaction_count is null then 0 else t.transaction_count end
from smartnfinal.ecommerce_flash f 
join (
	select m.dt, e.channel_level4,sum(net_sales)as net_sales,sum(quantity)as quantity,sum(transaction_count)as transaction_count,
	sum(item_count)as item_count,sum(item_count_per_tran)as item_count_per_tran
	from smartnfinal.ecommerce_flash e
	 join smartnfinal.date_compare m on 1=1
	where e.transaction_date BETWEEN m.qtd_stdt and m.qtd_endt and m.dt >='2019-01-01'
	group by 1,2
	)t on f.channel_level4=t.channel_level4 and t.dt=f.transaction_date
where f.created_at=current_date 
;

update smartnfinal.ecommerce_flash set 
y_net_sales=case when t.net_sales is null then 0 else t.net_sales end,
y_quantity=case when t.quantity is null then 0 else t.quantity end,
y_item_count=case when t.item_count is null then 0 else t.item_count end,
y_itemcn_pertran=case when t.item_count_per_tran is null then 0 else t.item_count_per_tran end ,
y_tran_count=case when t.transaction_count is null then 0 else t.transaction_count end
from smartnfinal.ecommerce_flash f 
join (
	select m.dt, e.channel_level4,sum(net_sales)as net_sales,sum(quantity)as quantity,sum(transaction_count)as transaction_count,
	sum(item_count)as item_count,sum(item_count_per_tran)as item_count_per_tran
	from smartnfinal.ecommerce_flash e
	 join smartnfinal.date_compare m on 1=1
	where e.transaction_date BETWEEN m.ytd_stdt and m.ytd_endt and m.dt >='2019-01-01'
	group by 1,2
	)t on f.channel_level4=t.channel_level4 and t.dt=f.transaction_date
where f.created_at=current_date 
;

-----------------------------------

update smartnfinal.ecommerce_flash set 
w_store_count=case when t.store_count is null then 0 else t.store_count end
from smartnfinal.ecommerce_flash f 
join (
	select m.dt, case when o.channel_level3='Shop Direct' then '01 - ShopDirect - B2C Delivery'
	when o.channel_level3='Shop Direct Pickup' then '02 - ShopDirect - B2C Pickup'
	when o.channel_level3='Dynamex' then '03 - ShopDirect - B2B Delivery'
	when o.channel_level3='Express Shopping' then '04 - ShopDirect - B2B Pickup'
	when o.channel_level3='Instacart' then '06 - Instacart Marketplace Delivery'
	when o.channel_level3='Shipt' then '08 - Shipt Marketplace Delivery'
	when o.channel_level3='DoorDash' then '09 - DoorDash Marketplace Delivery'
	when o.channel_level3='Google' then '12 - Google'
	when ioc.partner='PBI' then '05 - Instacart PBI – B2C Delivery'
	when  ioc.partner='Instacart Pick Up' then '07 - Instacart Marketplace Pickup'
else o.channel_level3 end as channel_level4,count(distinct o.store_number)as store_count
from #olap o
left join smartnfinal.ico_order ioc on o.transaction_date=ioc.daydate and o.store_number=ioc.store_number
 join smartnfinal.date_compare m on 1=1
where o.record_type=1 and  o.transaction_date BETWEEN m.wtd_stdt and m.wtd_endt  and m.dt>='2020-08-01'
and o.transaction_date>='2020-08-01'
	group by 1,2,channel_level4
	)t on f.channel_level4=t.channel_level4 and t.dt=f.transaction_date
where f.created_at=current_date 
;

update smartnfinal.ecommerce_flash set 
p_store_count=case when t.store_count is null then 0 else t.store_count end
from smartnfinal.ecommerce_flash f 
join (
	select m.dt, case when o.channel_level3='Shop Direct' then '01 - ShopDirect - B2C Delivery'
	when o.channel_level3='Shop Direct Pickup' then '02 - ShopDirect - B2C Pickup'
	when o.channel_level3='Dynamex' then '03 - ShopDirect - B2B Delivery'
	when o.channel_level3='Express Shopping' then '04 - ShopDirect - B2B Pickup'
	when o.channel_level3='Instacart' then '06 - Instacart Marketplace Delivery'
	when o.channel_level3='Shipt' then '08 - Shipt Marketplace Delivery'
	when o.channel_level3='DoorDash' then '09 - DoorDash Marketplace Delivery'
	when o.channel_level3='Google' then '12 - Google'
	when ioc.partner='PBI' then '05 - Instacart PBI – B2C Delivery'
	when  ioc.partner='Instacart Pick Up' then '07 - Instacart Marketplace Pickup'
else o.channel_level3 end as channel_level4,count(distinct o.store_number)as store_count
from #olap o
left join smartnfinal.ico_order ioc on o.transaction_date=ioc.daydate and o.store_number=ioc.store_number
 join smartnfinal.date_compare m on 1=1
where o.record_type=1 and  o.transaction_date BETWEEN m.ptd_stdt and m.ptd_endt  and m.dt>='2020-01-01'
and o.transaction_date>='2020-01-01'
	group by 1,2,channel_level4
	)t on f.channel_level4=t.channel_level4 and t.dt=f.transaction_date
where f.created_at=current_date 
;


update smartnfinal.ecommerce_flash set 
q_store_count=case when t.store_count is null then 0 else t.store_count end
from smartnfinal.ecommerce_flash f 
join (
	select m.dt, case when o.channel_level3='Shop Direct' then '01 - ShopDirect - B2C Delivery'
	when o.channel_level3='Shop Direct Pickup' then '02 - ShopDirect - B2C Pickup'
	when o.channel_level3='Dynamex' then '03 - ShopDirect - B2B Delivery'
	when o.channel_level3='Express Shopping' then '04 - ShopDirect - B2B Pickup'
	when o.channel_level3='Instacart' then '06 - Instacart Marketplace Delivery'
	when o.channel_level3='Shipt' then '08 - Shipt Marketplace Delivery'
	when o.channel_level3='DoorDash' then '09 - DoorDash Marketplace Delivery'
	when o.channel_level3='Google' then '12 - Google'
	when ioc.partner='PBI' then '05 - Instacart PBI – B2C Delivery'
	when  ioc.partner='Instacart Pick Up' then '07 - Instacart Marketplace Pickup'
else o.channel_level3 end as channel_level4,count(distinct o.store_number)as store_count
from #olap o
left join smartnfinal.ico_order ioc on o.transaction_date=ioc.daydate and o.store_number=ioc.store_number
 join smartnfinal.date_compare m on 1=1
where o.record_type=1 and  o.transaction_date BETWEEN m.qtd_stdt and m.qtd_endt  and m.dt>='2020-01-01'
and o.transaction_date>='2020-01-01'
	group by 1,2,channel_level4
	)t on f.channel_level4=t.channel_level4 and t.dt=f.transaction_date
where f.created_at=current_date 
;

update smartnfinal.ecommerce_flash set 
y_store_count=case when t.store_count is null then 0 else t.store_count end
from smartnfinal.ecommerce_flash f 
join (
	select m.dt, case when o.channel_level3='Shop Direct' then '01 - ShopDirect - B2C Delivery'
	when o.channel_level3='Shop Direct Pickup' then '02 - ShopDirect - B2C Pickup'
	when o.channel_level3='Dynamex' then '03 - ShopDirect - B2B Delivery'
	when o.channel_level3='Express Shopping' then '04 - ShopDirect - B2B Pickup'
	when o.channel_level3='Instacart' then '06 - Instacart Marketplace Delivery'
	when o.channel_level3='Shipt' then '08 - Shipt Marketplace Delivery'
	when o.channel_level3='DoorDash' then '09 - DoorDash Marketplace Delivery'
	when o.channel_level3='Google' then '12 - Google'
	when ioc.partner='PBI' then '05 - Instacart PBI – B2C Delivery'
	when  ioc.partner='Instacart Pick Up' then '07 - Instacart Marketplace Pickup'
else o.channel_level3 end as channel_level4,count(distinct o.store_number)as store_count
from #olap o
left join smartnfinal.ico_order ioc on o.transaction_date=ioc.daydate and o.store_number=ioc.store_number
 join smartnfinal.date_compare m on 1=1
where o.record_type=1 and  o.transaction_date BETWEEN m.ytd_stdt and m.ytd_endt  and m.dt>='2019-06-01'
and o.transaction_date>='2019-06-01'
	group by 1,2,channel_level4
	)t on f.channel_level4=t.channel_level4 and t.dt=f.transaction_date
where f.created_at=current_date 
;

-----------Update for channel level5 & APs values----------------

update smartnfinal.ecommerce_flash set 
channel_level5 = case when channel_level4 in ('01 - ShopDirect - B2C Delivery','02 - ShopDirect - B2C Pickup','03 - ShopDirect - B2B Delivery','05 - Instacart PBI – B2C Delivery','04 - ShopDirect - B2B Pickup')
						then '01 - ShopDirect'
					  when channel_level4 in ('06 - Instacart Marketplace Delivery','07 - Instacart Marketplace Pickup','08 - Shipt Marketplace Delivery','09 - DoorDash Marketplace Delivery')
					    then '02 - Marketplace'
					  else channel_level4 end 
where smartnfinal.ecommerce_flash.created_at=current_date ;

update smartnfinal.ecommerce_flash set 
ch5_store_count=case when t.store_count is null then 0 else t.store_count end
from smartnfinal.ecommerce_flash f 
join (
	select o.transaction_date,case when o.channel_level3='Shop Direct' then '01 - ShopDirect'
	when o.channel_level3='Shop Direct Pickup' then '01 - ShopDirect'
	when o.channel_level3='Dynamex' then '01 - ShopDirect'
	when o.channel_level3='Express Shopping' then '01 - ShopDirect'
	when o.channel_level3='Instacart' then '02 - Marketplace'
	when o.channel_level3='Shipt' then '02 - Marketplace'
	when o.channel_level3='DoorDash' then '02 - Marketplace'
	when o.channel_level3='Google' then '12 - Google'
	when ioc.partner='PBI' then '01 - ShopDirect'
	when  ioc.partner='Instacart Pick Up' then '02 - Marketplace'
	else o.channel_level3 end as channel_level5,
count(distinct o.store_number)as store_count
from #olap o
left join smartnfinal.ico_order ioc on o.transaction_date=ioc.daydate and o.store_number=ioc.store_number
where o.record_type=1 
group by o.transaction_date,channel_level5
	)t on f.channel_level5=t.channel_level5 and t.transaction_date=f.transaction_date
where f.created_at=current_date 
;

update smartnfinal.ecommerce_flash set 
ch5_w_store_count=case when t.store_count is null then 0 else t.store_count end
from smartnfinal.ecommerce_flash f 
join (
	select m.dt,case when o.channel_level3='Shop Direct' then '01 - ShopDirect'
	when o.channel_level3='Shop Direct Pickup' then '01 - ShopDirect'
	when o.channel_level3='Dynamex' then '01 - ShopDirect'
	when o.channel_level3='Express Shopping' then '01 - ShopDirect'
	when o.channel_level3='Instacart' then '02 - Marketplace'
	when o.channel_level3='Shipt' then '02 - Marketplace'
	when o.channel_level3='DoorDash' then '02 - Marketplace'
	when o.channel_level3='Google' then '12 - Google'
	when ioc.partner='PBI' then '01 - ShopDirect'
	when  ioc.partner='Instacart Pick Up' then '02 - Marketplace'
	else o.channel_level3 end as channel_level5,
count(distinct o.store_number)as store_count
from #olap o
left join smartnfinal.ico_order ioc on o.transaction_date=ioc.daydate and o.store_number=ioc.store_number
join smartnfinal.date_compare m on 1=1
where o.record_type=1 and  o.transaction_date BETWEEN m.wtd_stdt and m.wtd_endt  and m.dt>='2020-08-01'
and o.transaction_date>='2020-08-01'
group by 1,2,channel_level5)t on f.channel_level5=t.channel_level5 and t.dt=f.transaction_date
where f.created_at=current_date 
;

update smartnfinal.ecommerce_flash set 
ch5_p_store_count=case when t.store_count is null then 0 else t.store_count end
from smartnfinal.ecommerce_flash f 
join (
	select m.dt,case when o.channel_level3='Shop Direct' then '01 - ShopDirect'
	when o.channel_level3='Shop Direct Pickup' then '01 - ShopDirect'
	when o.channel_level3='Dynamex' then '01 - ShopDirect'
	when o.channel_level3='Express Shopping' then '01 - ShopDirect'
	when o.channel_level3='Instacart' then '02 - Marketplace'
	when o.channel_level3='Shipt' then '02 - Marketplace'
	when o.channel_level3='DoorDash' then '02 - Marketplace'
	when o.channel_level3='Google' then '12 - Google'
	when ioc.partner='PBI' then '01 - ShopDirect'
	when  ioc.partner='Instacart Pick Up' then '02 - Marketplace'
	else o.channel_level3 end as channel_level5,
count(distinct o.store_number)as store_count
from #olap o
left join smartnfinal.ico_order ioc on o.transaction_date=ioc.daydate and o.store_number=ioc.store_number
join smartnfinal.date_compare m on 1=1
where o.record_type=1 and  o.transaction_date BETWEEN m.ptd_stdt and m.ptd_endt  and m.dt>='2020-01-01'
and o.transaction_date>='2020-01-01'
group by 1,2,channel_level5)t on f.channel_level5=t.channel_level5 and t.dt=f.transaction_date
where f.created_at=current_date 
;

update smartnfinal.ecommerce_flash set 
ch5_q_store_count=case when t.store_count is null then 0 else t.store_count end
from smartnfinal.ecommerce_flash f 
join (
	select m.dt,case when o.channel_level3='Shop Direct' then '01 - ShopDirect'
	when o.channel_level3='Shop Direct Pickup' then '01 - ShopDirect'
	when o.channel_level3='Dynamex' then '01 - ShopDirect'
	when o.channel_level3='Express Shopping' then '01 - ShopDirect'
	when o.channel_level3='Instacart' then '02 - Marketplace'
	when o.channel_level3='Shipt' then '02 - Marketplace'
	when o.channel_level3='DoorDash' then '02 - Marketplace'
	when o.channel_level3='Google' then '12 - Google'
	when ioc.partner='PBI' then '01 - ShopDirect'
	when  ioc.partner='Instacart Pick Up' then '02 - Marketplace'
	else o.channel_level3 end as channel_level5,
count(distinct o.store_number)as store_count
from #olap o
left join smartnfinal.ico_order ioc on o.transaction_date=ioc.daydate and o.store_number=ioc.store_number
join smartnfinal.date_compare m on 1=1
where o.record_type=1 and  o.transaction_date BETWEEN m.qtd_stdt and m.qtd_endt  and m.dt>='2020-01-01'
and o.transaction_date>='2019-01-01'
group by 1,2,channel_level5)t on f.channel_level5=t.channel_level5 and t.dt=f.transaction_date
where f.created_at=current_date 
;

update smartnfinal.ecommerce_flash set 
ch5_y_store_count=case when t.store_count is null then 0 else t.store_count end
from smartnfinal.ecommerce_flash f 
join (
	select m.dt,case when o.channel_level3='Shop Direct' then '01 - ShopDirect'
	when o.channel_level3='Shop Direct Pickup' then '01 - ShopDirect'
	when o.channel_level3='Dynamex' then '01 - ShopDirect'
	when o.channel_level3='Express Shopping' then '01 - ShopDirect'
	when o.channel_level3='Instacart' then '02 - Marketplace'
	when o.channel_level3='Shipt' then '02 - Marketplace'
	when o.channel_level3='DoorDash' then '02 - Marketplace'
	when o.channel_level3='Google' then '12 - Google'
	when ioc.partner='PBI' then '01 - ShopDirect'
	when  ioc.partner='Instacart Pick Up' then '02 - Marketplace'
	else o.channel_level3 end as channel_level5,
count(distinct o.store_number)as store_count
from #olap o
left join smartnfinal.ico_order ioc on o.transaction_date=ioc.daydate and o.store_number=ioc.store_number
join smartnfinal.date_compare m on 1=1
where o.record_type=1 and  o.transaction_date BETWEEN m.ytd_stdt and m.ytd_endt  and m.dt>='2019-06-01'
and o.transaction_date>='2019-06-01'
group by 1,2,channel_level5)t on f.channel_level5=t.channel_level5 and t.dt=f.transaction_date
where f.created_at=current_date 
;
					 
update smartnfinal.ecommerce_flash set 					 
aps_netsales=case when store_count=0 or store_count is null then 0 else  net_sales/store_count end,
w_aps_netsales=case when w_store_count=0 or w_store_count is null then 0 else w_net_sales/w_store_count end,
p_aps_netsales=case when p_store_count=0 or p_store_count is null then 0 else p_net_sales/p_store_count end,
q_aps_netsales=case when q_store_count=0 or q_store_count is null then 0 else q_net_sales/q_store_count end,
y_aps_netsales=case when y_store_count=0 or y_store_count is null then 0 else y_net_sales/y_store_count end,
aps_trancount=case when store_count=0 or store_count is null then 0 else transaction_count/store_count end,
w_aps_trancount=case when w_store_count=0 or w_store_count is null then 0 else w_tran_count/w_store_count end,
p_aps_trancount=case when p_store_count=0 or p_store_count is null then 0 else p_tran_count/p_store_count end,
q_aps_trancount=case when q_store_count=0 or q_store_count is null then 0 else q_tran_count/q_store_count end,
y_aps_trancount=case when y_store_count=0 or y_store_count is null then 0 else y_tran_count/y_store_count end,
aps_itemcn_pertran=case when store_count=0or store_count is null then 0 else item_count_per_tran/store_count end,
w_aps_itemcn_pertran=case when w_store_count=0 or w_store_count is null then 0 else w_itemcn_pertran/w_store_count end,
p_aps_itemcn_pertran=case when p_store_count=0 or p_store_count is null then 0 else p_itemcn_pertran/p_store_count end,
q_aps_itemcn_pertran=case when q_store_count=0 or q_store_count is null then 0 else q_itemcn_pertran/q_store_count end,
y_aps_itemcn_pertran=case when y_store_count=0 or y_store_count is null then 0 else y_itemcn_pertran/y_store_count end
where smartnfinal.ecommerce_flash.created_at=current_date ;

------Update for Ly values----
update smartnfinal.ecommerce_flash set 
ly_net_sales=t.ly_net_sales,
ly_tran_count=t.ly_tran_count,
ly_item_count_per_tran=t.ly_item_count_per_tran,
ly_w_net_sales=t.ly_w_net_sales,
ly_p_net_sales=t.ly_p_net_sales,
ly_q_net_sales=t.ly_q_net_sales,
ly_y_net_sales=t.ly_y_net_sales,
ly_w_tran_count=t.ly_w_tran_count,
ly_p_tran_count=t.ly_p_tran_count,
ly_q_tran_count=t.ly_q_tran_count,
ly_y_tran_count=t.ly_y_tran_count,
ly_w_itemcn_pertran=t.ly_w_itemcn_pertran,
ly_p_itemcn_pertran=t.ly_p_itemcn_pertran,
ly_q_itemcn_pertran=t.ly_q_itemcn_pertran,
ly_y_itemcn_pertran=t.ly_y_itemcn_pertran
from smartnfinal.ecommerce_flash e 
join (
	select
		ecom_flash.transaction_date ,
		ecom_flash.channel_level4 ,	
		max(py.net_sales) as ly_net_sales ,	max(py.w_net_sales) as ly_w_net_sales ,	max(py.p_net_sales) as ly_p_net_sales ,max(py.q_net_sales) as ly_q_net_sales ,max(py.y_net_sales) as ly_y_net_sales,max(py.tran_count) as ly_tran_count,max(py.w_tran_count) as ly_w_tran_count,max(py.p_tran_count) as ly_p_tran_count,max(py.q_tran_count) as ly_q_tran_count,max(py.y_tran_count) as ly_y_tran_count,max(py.item_count_per_tran) as ly_item_count_per_tran,max(py.w_itemcn_pertran) as ly_w_itemcn_pertran,max(py.p_itemcn_pertran) as ly_p_itemcn_pertran,max(py.q_itemcn_pertran) as ly_q_itemcn_pertran,max(py.y_itemcn_pertran) as ly_y_itemcn_pertran
	from
		smartnfinal.ecommerce_flash ecom_flash
	left outer join (
			select transaction_date ,
			channel_level4 ,
			sum(net_sales) as net_sales ,sum(w_net_sales) as w_net_sales ,sum(p_net_sales) as p_net_sales ,sum(q_net_sales) as q_net_sales ,sum(y_net_sales) as y_net_sales,sum(transaction_count)as tran_count,sum(w_tran_count) as w_tran_count,sum(p_tran_count) as p_tran_count,sum(q_tran_count) as q_tran_count,sum(y_tran_count) as y_tran_count,sum(item_count_per_tran) as item_count_per_tran,sum(w_itemcn_pertran) as w_itemcn_pertran,sum(p_itemcn_pertran) as p_itemcn_pertran,sum(q_itemcn_pertran) as q_itemcn_pertran,sum(y_itemcn_pertran) as y_itemcn_pertran
		from smartnfinal.ecommerce_flash
		group by transaction_date ,	channel_level4 ) py ON
		ecom_flash.transaction_date = dateadd(day,	364,	py.transaction_date )
		and ecom_flash.channel_level4 = py.channel_level4
	group by
		ecom_flash.transaction_date ,ecom_flash.channel_level4) t on e.transaction_date=t.transaction_date and e.channel_level4=t.channel_level4
;



update smartnfinal.entity_log
set row_count =(select count(*) from smartnfinal.ecommerce_flash),
most_recent_date=(select max(transaction_date) from smartnfinal.ecommerce_flash),
min_date = (select min(transaction_date) from smartnfinal.ecommerce_flash),
lud=(select current_date),
most_recent_time = (select max(transaction_date)::timestamp from smartnfinal.ecommerce_flash),
lud_time=(select current_timestamp)
where entity_name='ecom_flash';

--------unload ecom-flash----------------


unload ('select * from smartnfinal.ecommerce_flash where created_at = CURRENT_DATE')
to 's3://hypersonixdata/Smart_and_Final/rds_dump/ecommerce_flash_' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
parallel on delimiter '|' NULL AS 'NULL';
