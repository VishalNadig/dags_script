ALTER WAREHOUSE DEV SET WAREHOUSE_SIZE=LARGE;

truncate table public.tenders_staging;
truncate table public.transactions_staging;
truncate table public.transaction_details_staging;
truncate table public.item_discounts_staging;


COPY INTO PUBLIC.transaction_details_staging
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/TransactionDetails
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' SKIP_HEADER = 1 DATE_FORMAT = 'YYYY/MM/DD')
FORCE = TRUE;

COPY INTO PUBLIC.transactions_staging
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/Transactions
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' SKIP_HEADER = 1 DATE_FORMAT = 'YYYY/MM/DD')
FORCE = TRUE;

COPY INTO PUBLIC.tenders_staging
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/Tenders
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' SKIP_HEADER = 1 DATE_FORMAT = 'YYYY/MM/DD')
FORCE = TRUE;

COPY INTO PUBLIC.item_discounts_staging
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/ItemDiscounts
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' SKIP_HEADER = 1 DATE_FORMAT = 'YYYY/MM/DD')
FORCE = TRUE;

truncate table PUBLIC.CASEITEM_SALE_STG;

COPY INTO PUBLIC.CASEITEM_SALE_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/CaseItemSales
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' SKIP_HEADER = 1 DATE_FORMAT = 'YYYY/MM/DD')
FORCE = TRUE;

insert into CASEITEM_SALE
select  BUSINESSDATE,
      TRANSACTIONDATE,
      STORENUMBER ,
      LANENUMBER ,
      TRANSACTIONNUMBER ,
      TRANSACTIONTYPECODE,
      ITEMNUMBER ,
      SCALEINDICATOR,
      ITEMDEPARTMENTNUMBER ,
      ITEMQUANTITY ,
      ITEMTOTALSALESPRICEAMOUNT ,
      ITEMTOTALCOSTAMOUNT ,
      FILENAME ,
      to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.CASEITEM_SALE_STG;