
import os
import re
import time
import json
import boto3
import pathlib
import urllib3
import requests
import paramiko
import psycopg2
import pandas as pd
from airflow import DAG

import pymysql
from sqlalchemy import create_engine
from sqlalchemy.exc import SQLAlchemyError

from json import JSONDecodeError

from snf_dags.etl_functions import sender_msg
from airflow.utils.dates import days_ago
from snf_dags.etl_functions import sftp_download

from snf_dags.etl_functions import upload_file_to_S3
from datetime import datetime, date , timedelta

from airflow.exceptions import AirflowException
from snf_dags.etl_functions import get_redshift_connect_string
from snf_dags.etl_functions import get_mongo_connect_string
from snf_dags.etl_functions import sender_msg

from configparser import ConfigParser, RawConfigParser
from snf_dags.etl_functions import removing_raw_process_s3_files
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.sensors.external_task_sensor import ExternalTaskSensor
from airflow.operators.python import PythonOperator,BranchPythonOperator

####### importing credential info #############################

config = RawConfigParser()
res = config.read('/home/ubuntu/airflow/dags/snf_dags/credential.ini')

bucket_name = config.get('S3', 'S3_B_NAME')
bucket_key = config.get('S3', 'S3_RDS_B_KEY')

host_dev = config.get('DEV_MYSQL', 'HOST')
user_dev = config.get('DEV_MYSQL', 'USERNAME')
pwd_dev = config.get('DEV_MYSQL', 'PASSWORD')

host_stg = config.get('STAGE_MYSQL', 'HOST')
user_stg = config.get('STAGE_MYSQL', 'USERNAME')
pwd_stg = config.get('STAGE_MYSQL', 'PASSWORD')

host_ent_green = config.get('GREEN_MYSQL', 'HOST')
user_ent_green = config.get('GREEN_MYSQL', 'USERNAME')
pwd_ent_green = config.get('GREEN_MYSQL', 'PASSWORD')

host_ent_blue = config.get('BLUE_MYSQL', 'HOST')
user_ent_blue = config.get('BLUE_MYSQL', 'USERNAME')
pwd_ent_blue = config.get('BLUE_MYSQL', 'PASSWORD')

session = boto3.session.Session('s3')
s3client = session.client('s3')
credentials = session.get_credentials()
s3_access_id = credentials.access_key
s3_secret_key = credentials.secret_key

####### importing path ########################################

path = '/home/ubuntu/airflow/dags/snf_sales/'
html_path = '/home/ubuntu/airflow/dags/'

client_name = 'smartnfinal'

ip_adds = {'dev_stage':{'ip':'10.200.17.84','db':client_name},
           'ent_green':{'ip':'10.200.17.14','db':client_name},
           'ent_blue':{'ip':'10.200.28.26','db':client_name}}


####### creating email message ################################

sucess_msg_dev = dict(Body = 'Dev SnF API NeesFeed data loaded sucessfully',
                 Subject = 'MySQL : Dev SnF API NeesFeed data loaded Successfully')
sucess_msg_stage = dict(Body = 'Stage SnF API NeesFeed data loaded sucessfully',
                 Subject = 'MySQL : Stage SnF API NeesFeed data loaded Successfully')
sucess_msg_green = dict(Body = 'Ent-Green SnF API NeesFeed data loaded sucessfully',
                 Subject = 'MySQL : Ent-Green SnF API NeesFeed data loaded Successfully')
sucess_msg_blue = dict(Body = 'Ent-Blue SnF API NeesFeed data loaded sucessfully',
                 Subject = 'MySQL : Ent-Blue SnF API NeesFeed data loaded Successfully')

error_ent_green = dict(Body = 'Alert: Ent-Green SnF API NeesFeed -- data is not loaded',
                 Subject = 'Alert: MySql Green-Ent SnF API NeesFeed data is not loaded loaded')
error_ent_blue  = dict(Body = 'Alert: Ent-Blue SnF API NeesFeed -- data is not loaded',
                 Subject = 'Alert: MySql Blue-Ent SnF API NeesFeed data is not loaded loaded')

################## Query ##########################################

search_query = """select count(*) as check_count from {0}.hsq_newsfeed_data where transaction_date = {1}"""

####### creating extra function for loading data into db #########################
engine_dev = create_engine(f'mysql+mysqlconnector://{user_dev}:{pwd_dev}@{host_dev}/{client_name}')
engine_stg = create_engine(f'mysql+mysqlconnector://{user_stg}:{pwd_stg}@{host_stg}/{client_name}')
engine_ent_blue = create_engine(f'mysql+mysqlconnector://{user_ent_blue}:{pwd_ent_blue}@{host_ent_blue}/{client_name}')
engine_ent_green = create_engine(f'mysql+mysqlconnector://{user_ent_green}:{pwd_ent_green}@{host_ent_green}/{client_name}')
            

def dev_stage_retrieve_api_data (path, dev_mysql_engine, stg_mysql_engine):
    
    ip = ip_adds.get('dev_stage').get('ip')
    db = ip_adds.get('dev_stage').get('db')
    
    if db == client_name:
        url = """http://{ip}:5000/hsapi/newsfeedload?data_flag=1&company_db={company_db}""".format(ip=ip,company_db=db)

        t0 = time.time()

        try:
            a = requests.get(url)
            timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            print(a.content)

            print('Time Taken : ',time.time() - t0)
            print('Insights ran @ ',timestamp)

            response = json.loads(a.content)
            olap_date = response.get('latest_date_in_olap')
            del response['latest_date_in_olap']

            dev_check = pd.read_sql(search_query.format(db,olap_date), dev_mysql_engine)

            if dev_check.check_count[0]!=0:
                raise AirflowException(f"Olap.transaction_date and hsq_neewsfeed_date.transaction_date date is matched ")
            else:
                cards = [item for x in response.values() for item in x]

                card_level = [cards[x].get('card_level')for x in range(len(cards))]
                card_type= [cards[x].get('card_value').get('card_type') for x in range(len(cards))]
                card_value =[json.dumps(cards[x].get('card_value').get('card_value')) for x in range(len(cards))]
                card_info = [json.dumps(cards[x].get('card_value').get('card_info')) for x in range(len(cards))]
                plot_data = [json.dumps(cards[x].get('card_value').get('plot_data')) for x in range(len(cards))]
                date_data = [cards[x].get('card_value').get('transaction_date') for x in range(len(cards))]

                data_dict = {'card_level':card_level,'card_type':card_type,'card_value':card_value,
                            'card_info':card_info,'plot_data':plot_data, 'transaction_date':date_data}

                response_df = pd.DataFrame(data_dict)
                response_df['fetched_date'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

                print("Total number of records :",response_df.shape[0])

                #inseting data into mysql dev
                response_df.to_sql(name='hsq_newsfeed_data', con=dev_mysql_engine, if_exists = 'append', index=False)
                dev_mysql_engine.dispose()
                
                #inseting data into mysql stage
                response_df.to_sql(name='hsq_newsfeed_data', con=stg_mysql_engine, if_exists = 'append', index=False)
                stg_mysql_engine.dispose()

        except requests.exceptions.RequestException as err:
            raise AirflowException(f"OOps: Something Else {err}")
        except requests.exceptions.HTTPError as errh:
            raise AirflowException(f"Http Error: {errh}")
        except requests.exceptions.ConnectionError as errc:
            raise AirflowException(f"Error Connecting: {errc}")
        except requests.exceptions.Timeout as errt:
            raise AirflowException(f"Timeout Error: {errt}")
        except JSONDecodeError as e:
            raise AirflowException(f"JSONDecodeError: {e}")
            print(a)
        except SQLAlchemyError as e:
            error = str(e.__dict__['orig'])
            print(error)
    else:
        raise AirflowException(f"DB client is not {client_name}")
        

        
def retrieve_api_data (path,env,ent_mysql_engine):
    
    ip = ip_adds.get(env).get('ip')
    db = ip_adds.get(env).get('db')
    
    if db == client_name:
        url = """http://{ip}:5000/hsapi/newsfeedload?data_flag=1&company_db={company_db}""".format(ip=ip,company_db=db)

        t0 = time.time()

        try:
            a = requests.get(url)
            timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            print(a.content)

            print('Time Taken : ',time.time() - t0)
            print('Insights ran @ ',timestamp)

            response = json.loads(a.content)
            olap_date = response.get('latest_date_in_olap')
            del response['latest_date_in_olap']

            ent_check = pd.read_sql(search_query.format(db,olap_date), ent_mysql_engine)

            if ent_check.check_count[0]!=0:
                raise AirflowException(f"Olap.transaction_date and hsq_neewsfeed_date.transaction_date date is matched ")
            else:
                cards = [item for x in response.values() for item in x]

                card_level = [cards[x].get('card_level')for x in range(len(cards))]
                card_type= [cards[x].get('card_value').get('card_type') for x in range(len(cards))]
                card_value =[json.dumps(cards[x].get('card_value').get('card_value')) for x in range(len(cards))]
                card_info = [json.dumps(cards[x].get('card_value').get('card_info')) for x in range(len(cards))]
                plot_data = [json.dumps(cards[x].get('card_value').get('plot_data')) for x in range(len(cards))]
                date_data = [cards[x].get('card_value').get('transaction_date') for x in range(len(cards))]

                data_dict = {'card_level':card_level,'card_type':card_type,'card_value':card_value,
                            'card_info':card_info,'plot_data':plot_data, 'transaction_date':date_data}

                response_df = pd.DataFrame(data_dict)
                response_df['fetched_date'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

                print("Total number of records :",response_df.shape[0])


                response_df.to_sql(name='hsq_newsfeed_data', con=ent_mysql_engine, if_exists = 'append', index=False)
                ent_mysql_engine.dispose()

        except requests.exceptions.RequestException as err:
            raise AirflowException(f"OOps: Something Else {err}")
        except requests.exceptions.HTTPError as errh:
            raise AirflowException(f"Http Error: {errh}")
        except requests.exceptions.ConnectionError as errc:
            raise AirflowException(f"Error Connecting: {errc}")
        except requests.exceptions.Timeout as errt:
            raise AirflowException(f"Timeout Error: {errt}")
        except JSONDecodeError as e:
            raise AirflowException(f"JSONDecodeError: {e}")
            print(a)
        except SQLAlchemyError as e:
            error = str(e.__dict__['orig'])
            print(error)
    else:
        raise AirflowException(f"DB client is not {client_name}")
        
def ent_green_blue_neewsfeed(path_name, envrn, mysql_engine, error_msg):                          
    try:
        retrieve_api_data(path_name,envrn,mysql_engine)
    except:
        sender_msg(error_msg, 'v_arush.s@hypersonix.ai, navaneeth@hypersonix.ai')
                                 
                                       
        
def collect_info(dict_file,query,conn):
    df = pd.DataFrame()
    idx = 0
    for tbl,col in dict_file.items():
        df1 = pd.read_sql(query.format(col_name =col ,table_name = tbl), conn)
        df1.insert(loc=idx, column='table_name', value=tbl)
        df = pd.concat([df,df1], axis=0).reset_index(drop=True)
    return(df)


snf_api_dict = {'hsq_newsfeed_data':'fetched_date'}


def success_dag_mail(path_name,engine_dev,engine_green,engine_stg, engine_blue):
   
    try:
        date_query = """select {col_name}, count(*) as count
                        from smartnfinal.{table_name} 
                        group by {col_name} order by {col_name} desc limit 1;"""

        df_dev = collect_info(snf_api_dict,date_query,engine_dev)
        df_dev.to_html(os.path.join(path_name,'html_files','snf_api_newsfeed_dev.html'),
                    justify='center',col_space=100,index=False)
        
        sender_msg(sucess_msg_dev,'v_arush.s@hypersonix.ai, navaneeth@hypersonix.ai',
                   is_html=True,html_file= 'snf_api_newsfeed_dev.html')
        
        df_stage = collect_info(snf_api_dict,date_query,engine_stg)
        df_stage.to_html(os.path.join(path_name,'html_files','snf_api_newsfeed_stage.html'),
                    justify='center',col_space=100,index=False)
        sender_msg(sucess_msg_stage,'v_arush.s@hypersonix.ai, navaneeth@hypersonix.ai',
                   is_html=True,html_file= 'snf_api_newsfeed_stage.html')
        try:
            df_ent_green = collect_info(snf_api_dict,date_query,engine_green)
            df_ent_green.to_html(os.path.join(path_name,'html_files','snf_api_newsfeed_ent_green.html'),
                        justify='center',col_space=100,index=False)
            sender_msg(sucess_msg_green,'v_arush.s@hypersonix.ai',
                    is_html=True,html_file= 'snf_api_newsfeed_ent_green.html')
        except Exception as err:
            print("logging into green host has thrown error: {}".format(err))
            # sender_msg("Green ENT not working so data not registered",'v_arush.s@hypersonix.ai, navaneeth@hypersonix.ai')
        try:
            df_ent_blue = collect_info(snf_api_dict,date_query,engine_blue)
            df_ent_blue.to_html(os.path.join(path_name,'html_files','snf_api_newsfeed_ent_blue.html'),
                        justify='center',col_space=100,index=False)
            sender_msg(sucess_msg_blue,'v_arush.s@hypersonix.ai',
                    is_html=True,html_file= 'snf_api_newsfeed_ent_blue.html')
        except Exception as err:
            print("logging into blue host has thrown error: {}".format(err))
            # sender_msg("Blue ENT not working so data not registered",'v_arush.s@hypersonix.ai, navaneeth@hypersonix.ai')


    except :
        raise AirflowException(f"HTML Mail ERROR")
        
######## MOJART -- MONGODB##################################

def get_connect_string():
    return pymysql.connect(host=host_dev, user=user_dev, password=pwd_dev)

def daily_pull_newsfeed_data():
    
    ip = ip_adds.get('dev_stage').get('ip')
    db = ip_adds.get('dev_stage').get('db')
    
    url = "http://{ip}/hsapi/newsfeedload?data_flag=1&company_db={company_db}".format(ip=ip,company_db=db)

    http = urllib3.PoolManager()
    response = http.request( 'GET', url)

    return json.loads(response.data)

def pull_newsfeed_data_sql():
    
    conn_obj = get_connect_string()
    cursor_obj = conn_obj.cursor(dictionary=True)
    cursor_obj.execute("SELECT * from {}.hsq_newsfeed_data ORDER BY fetched_date DESC".format(client_name))
    newsfeed_rows = cursor_obj.fetchall()
    return newsfeed_rows

def _get_card_type_id_mapping():
    
    conn_obj = get_connect_string()
    cursor_obj = conn_obj.cursor(dictionary=True)
    cursor_obj.execute("SELECT * from {}.hsq_newsfeed_config".format(client_name))
    newsfeed_config_rows = cursor_obj.fetchall()
    card_type_id_mapping = {}
    for row in newsfeed_config_rows:
        card_type_id_mapping[str(row["card_type_id"])] = {
                "capsule_name" : row["capsule_name"],
                "measure" : row["measure"]
                }
    return card_type_id_mapping



def index_sql_newsfeed_data(mozart_ip):
    
    mongo_newsfeed_data = []

    pulled_data =  pull_newsfeed_data_sql()
    del pulled_data['latest_date_in_olap']
    card_type_id_mapping = _get_card_type_id_mapping()
    
    transaction_date = str(datetime.now().date())
    for card in pulled_data:
        document = {}
        card_value = json.loads(card["card_value"])  
        document["card_value"] = card_value  
        document["comment"] = card_value.get("hsdata", {}).get("comment","")
        document.update(card_value.get("callback_json", {}).get("card_filters",{}))
        document["card_value"]["hsdata"].update(card_type_id_mapping.get(str(card.get("card_type","")),{}))
        document.update(card_type_id_mapping.get(str(card.get("card_type","")), ""))

        document["card_value"]["hsdata"]["fetched_date"] = str(card.get("fetched_date", ""))
        document["fetched_date"] = str(card.get("fetched_date", ""))

        ### get measures from mozart 
        comment = document["comment"]
        comment = re.sub(r"<b>|</b>", "", comment)

        http = urllib3.PoolManager()
        response = http.request(
            'POST',
            "{}/mozart_lite/{}".format(mozart_ip, client_name),
            headers={'Content-Type': 'application/json'},
            body=json.dumps({"inputText" : comment})
        )
        if response.status == 200:
            mozart_response = json.loads(response.data)
        
            hskpilist = mozart_response["hsresult"]["hskpilist"]

            mozart_dimensions_measures = []
            for kpi in hskpilist:
                mozart_dimensions_measures.extend(kpi["hscallback_json"]["hsdimlist"])
                mozart_dimensions_measures.extend(kpi["hscallback_json"]["hsmeasurelist"])
            mozart_dimensions_measures = " ".join(list(set(mozart_dimensions_measures)))

            document["dimensions_measures"] = mozart_dimensions_measures

        mongo_newsfeed_data.append(document)

    get_mongo_connect_string()[client_name]["newsfeed_data"].drop()
    get_mongo_connect_string()[client_name]["newsfeed_data"].insert_many(mongo_newsfeed_data)
    get_mongo_connect_string()[client_name]["newsfeed_data"].create_index( [ ("$**", "text" )] )

def index_daily_newsfeed_data(mozart_ip):
    card_type_id_mapping = _get_card_type_id_mapping()
    mongo_newsfeed_data = []

    pulled_data =  daily_pull_newsfeed_data()
    del pulled_data['latest_date_in_olap']
    pulled_data = [item for sublist in pulled_data.values() for item in sublist]

    for card in pulled_data:
        document = {}
        card_value = card.get("card_value", {}).get("card_value", "")  
        document["comment"] = card_value.get("hsdata", {}).get("comment","")
        document.update(card_value.get("callback_json", {}).get("card_filters",{}))
        document["card_value"] = card_value
        document["card_value"]["hsdata"].update(card_type_id_mapping.get(str(card.get("card_value",{}).get("card_type","")) , {}))
        document.update(card_type_id_mapping.get(str(card.get("card_value",{}).get("card_type","")) , {}))
        document["card_value"]["hsdata"]["fetched_date"] = str(datetime.now())
        document["fetched_date"] = str(datetime.now())

        ## transaction date if not present "today" would be used as transaction_date
        document["fetched_date"] = str(datetime.now())

        ### get measures from mozart 
        mozart_dimensions_measures = []
        comment = document["comment"]
        comment = re.sub(r"<b>|</b>", "", comment)

        http = urllib3.PoolManager()
        response = http.request(
            'POST',
            "{}/mozart_lite/{}".format( mozart_ip, client_name),
            headers={'Content-Type': 'application/json'},
            body=json.dumps({"inputText" : comment})
        )
        if response.status == 200:
            mozart_response = json.loads(response.data)        
            hskpilist = mozart_response["hsresult"]["hskpilist"]
            for kpi in hskpilist:
                mozart_dimensions_measures.extend(kpi["hscallback_json"]["hsdimlist"])
                mozart_dimensions_measures.extend(kpi["hscallback_json"]["hsmeasurelist"])
            mozart_dimensions_measures = " ".join(list(set(mozart_dimensions_measures)))

            document["dimensions_measures"] = mozart_dimensions_measures

        mongo_newsfeed_data.append(document)

    get_mongo_connect_string()[client_name]["newsfeed_data"].insert_many(mongo_newsfeed_data)
    
    fetched_date = str(datetime.now().date())
    print('MojarMongo_Count_Date:'+fetched_date,get_mongo_connect_string()[client_name]["newsfeed_data"].\
                                  find({ "fetched_date" : fetched_date}).count())

            
###### Creating dags ##########################################


default_args = {'owner': 'Sudhakar','depends_on_past': False,
               'email': ['v_arush.s@hypersonix.ai', 'navaneeth@hypersonix.ai'],'email_on_failure': True}

dag = DAG(
    dag_id = 'sf_snf_api_neewsfeed_1',
    default_args=default_args,
    start_date = datetime(2021,10,12),
    schedule_interval = '10 12 * * *')


run_api_task = DummyOperator(task_id='run_api', dag=dag)

wait_for_ecom_task = ExternalTaskSensor(
    task_id='wait_for_ecom_file_loaded',
    external_dag_id='sf_icorder_ecom_stockout_flashes_1',
    allowed_states = ['success','upstream_failed'],
    external_task_id='success_dag_mail'
)

dev_stage_retrieve_api_data_task = PythonOperator(
    task_id = 'dev_stage_getting_api_data',
    python_callable =dev_stage_retrieve_api_data,
    retry_delay = timedelta(minutes=15),
    retries = 2,
    op_kwargs={'path':path,'dev_mysql_engine':engine_dev,
               'stg_mysql_engine':engine_stg},
    dag = dag)

ent_green_retrieve_api_data_task = PythonOperator(
    task_id = 'ent_green_getting_api_data',
    python_callable =ent_green_blue_neewsfeed,
    trigger_rule='none_skipped',
    retry_delay = timedelta(minutes=15),
    retries = 2,
    op_kwargs={'path_name':path,'envrn':'ent_green',
               'mysql_engine':engine_ent_green, 'error_msg':error_ent_green},
    dag = dag)
                                 
ent_blue_retrieve_api_data_task = PythonOperator(
    task_id = 'ent_blue_getting_api_data',
    python_callable =ent_green_blue_neewsfeed,
    trigger_rule='none_skipped',
    retry_delay = timedelta(minutes=15),
    retries = 2,
    op_kwargs={'path_name':path,'envrn':'ent_blue',
               'mysql_engine':engine_ent_blue, 'error_msg':error_ent_blue},
    dag = dag)
                                 

# load_mojart_task = PythonOperator(
#     task_id = 'load_mysql_dev_data_to_Mojart',
#     python_callable =index_daily_newsfeed_data,
#     retry_delay = timedelta(minutes=10),
#     retries = 1,
#     op_kwargs={'mozart_ip':'10.200.29.104'},
#     dag = dag)


success_dag_mail_task = PythonOperator(
    task_id = 'success_dag_mail',
    python_callable =success_dag_mail,
    op_kwargs={'path_name': html_path, 
               'engine_dev':engine_dev,
               'engine_stg':engine_stg,
               'engine_green':engine_ent_green,
               'engine_blue':engine_ent_blue},
    dag = dag)


run_api_task >> wait_for_ecom_task >>dev_stage_retrieve_api_data_task >> ent_green_retrieve_api_data_task >> ent_blue_retrieve_api_data_task >>success_dag_mail_task       
            
            
            
   
