delete from prod_smartnfinal.public.olap_ecommerce where transaction_date >='{maxdate}';

COPY INTO prod_smartnfinal.PUBLIC.olap_ecommerce
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/olap_ecommerce_1_ 
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' NULL_IF = ('NULL', 'null','Null','',' ')
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

delete from prod_smartnfinal.public.ecom_stockout_flash where transaction_date >='{maxdate}';

COPY INTO prod_smartnfinal.PUBLIC.ecom_stockout_flash
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/ecom_stockout_flash_1_ 
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' NULL_IF = ('NULL', 'null','Null','',' ')
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

delete from prod_smartnfinal.public.ecommerce_flash where transaction_date >='{maxdate}';

COPY INTO prod_smartnfinal.PUBLIC.ecommerce_flash
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/ecommerce_flash_1_ 
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' NULL_IF = ('NULL', 'null','Null','',' ')
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

update prod_smartnfinal.public.entity_log
set row_count =(select count(*) from prod_smartnfinal.public.ecommerce_flash),
most_recent_date=(select max(transaction_date) from prod_smartnfinal.public.ecommerce_flash),
min_date = (select min(transaction_date) from prod_smartnfinal.public.ecommerce_flash),
lud=(select current_date),
most_recent_time = (select max(transaction_date)::timestamp from prod_smartnfinal.public.ecommerce_flash),
lud_time=(select current_timestamp)
where entity_name='ecom_flash';

update prod_smartnfinal.public.entity_log
set row_count =(select count(*) from prod_smartnfinal.public.olap_ecommerce),
most_recent_date=(select max(transaction_date) from prod_smartnfinal.public.olap_ecommerce),
min_date = (select min(transaction_date) from prod_smartnfinal.public.olap_ecommerce),
lud=(select current_date),
most_recent_time = (select max(transaction_date)::timestamp from prod_smartnfinal.public.olap_ecommerce),
lud_time=(select current_timestamp)
where entity_name='ecom';

update prod_smartnfinal.public.entity_log
set row_count =(select count(*) from prod_smartnfinal.public.ecom_stockout_flash),
most_recent_date=(select max(transaction_date) from prod_smartnfinal.public.ecom_stockout_flash),
min_date = (select min(transaction_date) from prod_smartnfinal.public.ecom_stockout_flash),
lud=(select current_date),
most_recent_time = (select max(transaction_date)::timestamp from prod_smartnfinal.public.ecom_stockout_flash),
lud_time=(select current_timestamp)
where entity_name='ecom_stockout';