-- ================= Loading Data =====================================================

delete from prod_smartnfinal.public.olap_item_details where record_type in (1,2,3,4) and  transaction_date >='{maxdate}';

copy into prod_smartnfinal.public.olap_item_details
from s3://hypersonixdata/Smart_and_Final/swf_dump/olap_item_details_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

delete from prod_smartnfinal.public.olap_ecommerce where transaction_date >='{maxdate}';

copy into prod_smartnfinal.public.olap_ecommerce
from s3://hypersonixdata/Smart_and_Final/swf_dump/olap_ecommerce_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

delete from prod_smartnfinal.public.ecom_stockout_flash where transaction_date >='{maxdate}';

copy into prod_smartnfinal.public.ecom_stockout_flash
from s3://hypersonixdata/Smart_and_Final/swf_dump/ecom_stockout_flash_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

truncate table prod_smartnfinal.public.dim_products;

copy into prod_smartnfinal.public.dim_products
from s3://hypersonixdata/Smart_and_Final/swf_dump/dim_products_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

truncate table prod_smartnfinal.public.dim_store_new;

copy into prod_smartnfinal.public.dim_store_new
from s3://hypersonixdata/Smart_and_Final/swf_dump/dim_store_new_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

truncate table prod_smartnfinal.public.dim_store;

copy into prod_smartnfinal.public.dim_store
from s3://hypersonixdata/Smart_and_Final/swf_dump/dim_store1_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

