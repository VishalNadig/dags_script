truncate table PUBLIC.ICO_ORDER_STG;

COPY INTO PUBLIC.ICO_ORDER_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/ICOrderHistory
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' SKIP_HEADER = 1 
               DATE_FORMAT = 'YYYY/MM/DD' TRIM_SPACE = TRUE)
FORCE = TRUE;

delete from public.ico_order where daydate >= '{maxdate}';

insert into ICO_ORDER
select
    ORDERID,
    DAYDATE,
    PARTNER ,
    NETSALES,
    TOTAL_QUANTITY,
    STORE_NUMBER,
    FILENAME,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.ICO_ORDER_STG;

    
truncate table PUBLIC.ECOMMERCE_STG;

COPY INTO PUBLIC.ECOMMERCE_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/eCommerceShipmentsHistory
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' SKIP_HEADER = 1 
               DATE_FORMAT = 'YYYY/MM/DD' TRIM_SPACE = TRUE)
FORCE = TRUE;

delete from public.ecommerce where daydate >= '{maxdate}';

insert into ECOMMERCE
select
    PARTNER ,
    ORDERID ,
    DAYDATE,
    STORENUMBER ,
    ITEMNUMBER ,
    ORDEREDQUANTITY ,
    SHIPPEDQUANTITY ,
    SHIPPEDQUANTITY_NOSUB ,
    LOSSSALES ,
    REGULAREACHRETAIL ,
    FILENAME ,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.ECOMMERCE_STG;
    
