ALTER WAREHOUSE DEV SET WAREHOUSE_SIZE=LARGE;

INSERT INTO DEV_SMARTNFINAL."PUBLIC".dim_store
(COMPANY_ID, COMPANY_NAME , REGION_CODE, REGION, DISTRICT_CODE, DISTRICT, STOREFORMAT, STORE_NUMBER, PRIORSTORE_NUMBER, STORE_NAME, PRICING_ZONE, STOREOPEN_DATE, STORECLOSE_DATE, ADDRESS, CITY, STATE, ZIP, FILENAME, CREATED_AT)
SELECT COMPANY_NUMBER, COMPANY, REGION_CODE, REGION, DISTRICT_CODE, DISTRICT, STORE_FORMAT, STORE_NUMBER, PRIOR_STORENUMBER, STORE_NAME, PRICING_ZONE, STORE_OPENDATE, STORE_CLOSEDATE, ADDRESS, CITY, STATE, ZIPCODE, FILENAME, CREATED_AT
FROM DEV_SMARTNFINAL."PUBLIC".DIM_LOCATION_NEW
WHERE STOrE_NUMBER NOT IN (SELECT  DISTINCT STORE_NUMBER FROM DEV_SMARTNFINAL."PUBLIC".dim_store);

UPDATE  DEV_SMARTNFINAL."PUBLIC".dim_store SET 
STATE_NAME =CASE WHEN STATE IN('CA','Ca','ca') THEN 'California'
				 WHEN STATE IN('FL','Fl','fl') THEN 'Florida'
				 WHEN STATE IN('NV','Nv','nv') THEN 'Nevada'
				 WHEN STATE IN('AZ','Az','az') THEN 'Arizona' 
				 WHEN STATE IN('TX','Tx','tx') THEN 'Texas'END,
REGION_NAME = REGION_CODE||' - '||REGION ,
DISTRICT_NAME = DISTRICT_CODE ||' - '||DISTRICT ,
SIZE_RANK=CASE WHEN TOTAL_SQUARE_FOOTAGE <=19000 THEN '1 - Lowest Group'
WHEN TOTAL_SQUARE_FOOTAGE >19000 AND TOTAL_SQUARE_FOOTAGE <=24000 THEN '2 - Low Group'
WHEN TOTAL_SQUARE_FOOTAGE >24000 AND TOTAL_SQUARE_FOOTAGE <=27869 THEN '3 - Middle Group'
WHEN TOTAL_SQUARE_FOOTAGE >27869 AND TOTAL_SQUARE_FOOTAGE <=31150 THEN '4 - High Group'
WHEN TOTAL_SQUARE_FOOTAGE >31150 AND TOTAL_SQUARE_FOOTAGE <=50000  THEN '5 - Highest Group'
ELSE null END,
STORE_SIZE=CASE WHEN TOTAL_SQUARE_FOOTAGE <=19000 THEN '1 - Smallest Group <~19K'
WHEN TOTAL_SQUARE_FOOTAGE >19000 AND TOTAL_SQUARE_FOOTAGE <=24000 THEN '2 - Small Group <~24K'
WHEN TOTAL_SQUARE_FOOTAGE >24000 AND TOTAL_SQUARE_FOOTAGE <=27869 THEN '3 - Mid Group <~28K'
WHEN TOTAL_SQUARE_FOOTAGE >27869 AND TOTAL_SQUARE_FOOTAGE <=31150 THEN '4 - Large Group <~31K'
WHEN TOTAL_SQUARE_FOOTAGE >31150 AND TOTAL_SQUARE_FOOTAGE <=50000  THEN '5 - Largest Group <~50K'
ELSE null END   ;

UPDATE DEV_SMARTNFINAL."PUBLIC".dim_store s
SET NEW_STORE_NUMBER = T.new_store_num 
FROM (SELECT s.store_number , s.PRIORSTORE_NUMBER ,s.NEW_STORE_NUMBER,s1.store_number AS st_num,
CASE WHEN s.store_number NOT IN (SELECT DISTINCT PRIORSTORE_NUMBER FROM DEV_SMARTNFINAL."PUBLIC".dim_store) THEN s.store_number
ELSE NULL END test,
CASE WHEN test IS NOT NULL THEN test ELSE s1.store_number END AS new_store_num
FROM DEV_SMARTNFINAL."PUBLIC".dim_store s
LEFT JOIN DEV_SMARTNFINAL."PUBLIC".dim_store s1 ON s.store_number=s1.PRIORSTORE_NUMBER 
ORDER BY s.store_number)t
WHERE s.store_number=T.store_number ;

update DEV_SMARTNFINAL."PUBLIC".dim_store set 
vintage=date_part(year,storeopen_date)||' New' 
where vintage is null and storeformat='Extra' and priorstore_number=0; 

update DEV_SMARTNFINAL."PUBLIC".dim_store set 
vintage=date_part(year,storeopen_date)||' Relos' 
where vintage is null and storeformat='Extra' and priorstore_number<>0;

UPDATE DEV_SMARTNFINAL."PUBLIC".dim_store s SET
COMP_START_DATE =T.COMP_START_DATE1
FROM (
SELECT DISTINCT STORE_NUMBER ,PRIORSTORE_NUMBER,NEW_STORE_NUMBER ,STOREOPEN_DATE ,COMP_START_DATE,dayname(dateadd(week,60,STOREOPEN_DATE)) AS day_name,
CASE WHEN PRIORSTORE_NUMBER =0 THEN (
	CASE WHEN dayname(dateadd(week,60,STOREOPEN_DATE))='Sun' THEN dateadd(week,60,STOREOPEN_DATE) ELSE
	next_day(dateadd(week,60,STOREOPEN_DATE),'Sunday') END)
	END AS COMP_START_DATE1
	--datediff(DAY,COMP_START_DATE1,COMP_START_DATE)
FROM DEV_SMARTNFINAL."PUBLIC".dim_store
WHERE PRIORSTORE_NUMBER =0
)T
WHERE T.STORE_NUMBER=s.STORE_NUMBER AND s.PRIORSTORE_NUMBER =0;

UPDATE DEV_SMARTNFINAL."PUBLIC".dim_store s SET
COMP_START_DATE =T.COMP_START_DATE
FROM (
SELECT DISTINCT STORE_NUMBER ,PRIORSTORE_NUMBER,NEW_STORE_NUMBER ,STOREOPEN_DATE ,COMP_START_DATE
FROM DEV_SMARTNFINAL."PUBLIC".dim_store
WHERE PRIORSTORE_NUMBER <>0
)T
WHERE T.PRIORSTORE_NUMBER=s.STORE_NUMBER ;

UPDATE DEV_SMARTNFINAL."PUBLIC".dim_store s
SET COMP_FLAG =T.flag1
FROM (
SELECT DISTINCT NEW_STORE_NUMBER ,store_number,PRIORSTORE_NUMBER ,STOREOPEN_DATE ,
STORECLOSE_DATE ,COMP_FLAG ,COMP_START_DATE,
CASE WHEN (STORECLOSE_DATE<current_date OR STOREOPEN_DATE > current_date)THEN 0 ELSE 1 END AS flag1
FROM DEV_SMARTNFINAL."PUBLIC".dim_store
)T
WHERE T.STORE_NUMBER=s.STORE_NUMBER ;


update public.dim_store s 
set  
company_id=l.company_number, 
company_name=l.company, 
region_code=l.region_code, 
region=l.region, 
district_code=l.district_code, 
district=l.district, 
storeformat=l.store_format, 
priorstore_number=l.prior_storenumber, 
store_name=initcap(l.store_name), 
pricing_zone=l.pricing_zone, 
pricing_zone_name =p.pricing_zone_name,
storeopen_date=l.store_opendate, 
storeclose_date=l.store_closedate, 
address=l.address, 
city=l.city, 
state=l.state, 
zip=l.zipcode, 
filename=l.filename, 
created_at=l.created_at 
from public.dim_location_new l  
join public.dim_price_zone p on l.pricing_zone =p.pricing_zone
where s.store_number=l.store_number; 


INSERT INTO DEV_SMARTNFINAL."PUBLIC".DIM_STORE_NEW
(COMPANY_ID, COMPANY_NAME, REGION_CODE, REGION, DISTRICT_CODE, DISTRICT, STOREFORMAT, STORE_NUMBER, PRIORSTORE_NUMBER, STORE_NAME_ORIGINAL, PRICING_ZONE, STOREOPEN_DATE, STORECLOSE_DATE, ADDRESS, CITY, STATE, ZIP, FILENAME, CREATED_AT, STATE_NAME, NEW_STORE_NUMBER, REGION_NAME, DISTRICT_NAME, LATITUDE, LONGITUDE, VINTAGE, COMP_START_DATE, COMP_FLAG, PRICING_ZONE_NAME, STORE_NAME, TOTAL_SQUARE_FOOTAGE, SIZE_RANK, STORE_SIZE)
SELECT COMPANY_ID, COMPANY_NAME, REGION_CODE, REGION, DISTRICT_CODE, DISTRICT, STOREFORMAT, STORE_NUMBER, PRIORSTORE_NUMBER, STORE_NAME, PRICING_ZONE, STOREOPEN_DATE, STORECLOSE_DATE, ADDRESS, CITY, STATE, ZIP, FILENAME, CREATED_AT, STATE_NAME, NEW_STORE_NUMBER, REGION_NAME, DISTRICT_NAME, LATITUDE, LONGITUDE, VINTAGE, COMP_START_DATE, COMP_FLAG, PRICING_ZONE_NAME, right('000'||new_store_number,3)||' '||store_name AS STORE_NAME , TOTAL_SQUARE_FOOTAGE, SIZE_RANK, STORE_SIZE
FROM DEV_SMARTNFINAL."PUBLIC".dim_store
WHERE store_number NOT IN (SELECT DISTINCT store_number FROM DEV_SMARTNFINAL."PUBLIC".dim_store_new);



update public.dim_store_new s set 
company_id=d.company_id, 
company_name=d.company_name, 
region_code=d.region_code, 
region=d.region, 
district_code=d.district_code, 
district=d.district, 
storeformat=d.storeformat, 
store_name_original=d.store_name, 
pricing_zone=d.pricing_zone, 
pricing_zone_name =p.pricing_zone_name,
address=d.address, 
city=d.city, 
state=d.state, 
zip=d.zip, 
state_name=d.state_name, 
store_name=right('000'||d.store_number,3)||' '||d.store_name,
filename=d.filename, 
created_at=d.created_at 
from public.dim_store d 
join public.dim_price_zone p on d.pricing_zone =p.pricing_zone
where d.store_number=s.new_store_number; 
