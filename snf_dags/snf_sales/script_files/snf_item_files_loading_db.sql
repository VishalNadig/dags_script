
-- TRUNCATE table PUBLIC.PRICE_NEW_STG
TRUNCATE table PUBLIC.DIM_LOCATION_NEW_STG;
TRUNCATE table PUBLIC.DIM_ITEM_NEW_STG;
TRUNCATE table PUBLIC.DIM_CATEGORY_NEW_STG;

-- COPY INTO PUBLIC.PRICE_NEW_STG
-- FROM s3://hypersonixdata/Smart_and_Final/swf_dump/Price
-- credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
-- FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' SKIP_HEADER = 1 DATE_FORMAT = 'YYYY/MM/DD')
-- FORCE = TRUE


-- insert into PRICE_NEW
-- select DAY_DATE,
--     STORE_NUMBER ,
--     ITEM_NUMBER ,
--     REGULAR_PRICE ,
--     PROMO_PRICE ,
--     DISCOUNT_AMOUNT ,
--     PROMO_STARTDATE,
--     PROMO_ENDDATE,
--     BMSM_MULLTIPLE ,
--     BMSM_PRICE ,
--     BMSM_STARTDATE,
--     BMSM_ENDDATE,
--     FILENAME ,
--     to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.PRICE_NEW_STG 

TRUNCATE table public.dim_item_new;

COPY INTO PUBLIC.DIM_ITEM_NEW_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/Product
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' SKIP_HEADER = 1 DATE_FORMAT = 'YYYY/MM/DD')
FORCE = TRUE;

insert into DIM_ITEM_NEW
select COMPANY_NUMBER ,
    ITEMNUMBER ,
    ITEMCREATE_DATE,
    ITEM_DESCRIPTION ,
    SINAGE_DESCRIPTION ,
    ITEM_SIZE ,
    FAMILYCODE ,
    BRAND ,
    PLBORNON_DATE,
    CLUBSIZE ,
    BUSINESS_ITEM ,
    SEASONFLAG ,
    SCALEABLEFLAG ,
    COMPONENT_CODE ,
    DEPARTMENT ,
    DEPT_DESC ,
    ITEMSTATUS ,
    ISNONFAT ,
    ISVEGAN ,
    ISSUGARFREE ,
    ISHALAL ,
    ISKOSHER ,
    ISNONGMO ,
    ISGOODANDWELL ,
    ISSOYFREE ,
    ISGLUTENFREE ,
    ISFATFREE ,
    ISORGANIC ,
    FILENAME ,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.DIM_ITEM_NEW_STG ;

TRUNCATE table public.dim_location_new;

COPY INTO PUBLIC.DIM_LOCATION_NEW_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/Locations
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' SKIP_HEADER = 1 DATE_FORMAT = 'YYYY/MM/DD')
FORCE = TRUE;


insert into DIM_LOCATION_NEW
select COMPANY_NUMBER ,
        COMPANY ,
        REGION_CODE ,
        REGION ,
        DISTRICT_CODE ,
        DISTRICT ,
        STORE_FORMAT ,
        STORE_NUMBER ,
        PRIOR_STORENUMBER ,
        STORE_NAME ,
        PRICING_ZONE ,
        STORE_OPENDATE,
        STORE_CLOSEDATE,
        ADDRESS,
        CITY ,
        STATE ,
        ZIPCODE ,
        FILENAME,
        to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.DIM_LOCATION_NEW_STG ;

TRUNCATE table public.dim_category_new;

COPY INTO PUBLIC.DIM_CATEGORY_NEW_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/MerchStructure
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' SKIP_HEADER = 1 DATE_FORMAT = 'YYYY/MM/DD')
FORCE = TRUE;

insert into DIM_CATEGORY_NEW
select CATEGORY_CODE,
    CATEGORY ,
    SUBCATEGORY_CODE,
    SUBCATEGORY ,
    COMPONENT_CODE ,
    COMPONENT ,
    FILENAME,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.DIM_CATEGORY_NEW_STG ;


