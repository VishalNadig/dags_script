Truncate table public.dim_products;

INSERT INTO public.dim_products
(company_number, itemnumber, itemcreate_date, item_description, sinage_description, item_size, familycode, brand, plbornon_date, clubsize, clubsize_1, business_item, business_item_1, private_label, seasonflag, scaleableflag, itemstatus, isnonfat, isvegan, issugarfree, ishalal, iskosher, isnongmo, isgoodandwell, issoyfree, isglutenfree, isfatfree, isorganic, department, dept_desc, include_in_sales, category_code, category, subcategory_code, subcategory,src_component_code, component_code, component, department_name, category_name, subcategory_name, component_name, filename, created_at)
select i.company_number,
i.itemnumber,
i.itemcreate_date,
i.item_description,
i.sinage_description,
i.item_size,
i.familycode,
i.brand,
i.plbornon_date,
i.clubsize,
case WHEN i.clubsize='Y'then 'Club Items'
     when i.clubsize='N'then 'Non-Club' else null end as clubsize_1  ,
i.business_item,
case WHEN i.business_item='Y'then 'Business Items'
     when i.business_item='N'then 'Non-Business' else null end as business_item_1,
case when i.brand is null or i.brand ='' then 'Non-Private Label'
      else 'Private Label Items'end as private_label,
i.seasonflag,
i.scaleableflag,
i.itemstatus,
i.isnonfat,
i.isvegan,
i.issugarfree,
i.ishalal,
i.iskosher,
i.isnongmo,
i.isgoodandwell,
i.issoyfree,
i.isglutenfree,
i.isfatfree,
i.isorganic,
i.department,
UPPER(i.dept_desc)as dept_desc,
de.include_in_sales,
right('000'||LTRIM(c.category_code,'M-'),3) as category_code,
UPPER(c.category)as category,
right('000'||SUBSTRING(c.subcategory_code,6,7),3) as subcategory_code,
UPPER(c.subcategory)as subcategory,
c.component_code as src_component_code,
right('000'||SUBSTRING(c.component_code,9,11),3)  as component_code,
UPPER(c.component)as component,
right('000'||i.department,3)||' - '||i.dept_desc as department_name,
right('000'||LTRIM(c.category_code,'M-'),3)||' - '||c.category as category_name,
right('000'||SUBSTRING(c.subcategory_code,6,7),3)||' - '||c.subcategory as subcategory_name,
right('000'||SUBSTRING(c.component_code,9,11),3)||' - '||c.component as component_name,
i.filename,
i.created_at
from public.dim_item_new i
left join public.dim_category_new c on c.component_code=i.component_code
left join public.dim_dept de on de.department=i.department;

update public.dim_products p set item_net_sales=t.item_net_sales
from (select item_number,sum(net_sales)as item_net_sales
	from public.olap_item_details
	where record_type=1 and year=2020
	group by item_number)t
where p.itemnumber=t.item_number;
