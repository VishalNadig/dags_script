
-- --================Unloading Data =================================================

-- COPY INTO 's3://hypersonixdata/Smart_and_Final/swf_dump/olap_item_details_1_'
-- FROM (select * from dev_smartnfinal.public.olap_item_details  where created_at = current_date)
-- CREDENTIALS = (AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
-- FILE_FORMAT = (TYPE = CSV
--             FIELD_DELIMITER = '|'
--             SKIP_HEADER = 1
--             FIELD_OPTIONALLY_ENCLOSED_BY ='"' 
--             TRIM_SPACE = TRUE 
--             EMPTY_FIELD_AS_NULL = TRUE
--             NULL_IF = ('NULL', 'null','Null','',' '));


-- COPY INTO 's3://hypersonixdata/Smart_and_Final/swf_dump/olap_ecommerce_1_'
-- FROM (select * from dev_smartnfinal.public.olap_ecommerce  where created_at = current_date)
-- CREDENTIALS = (AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
-- FILE_FORMAT = (TYPE = CSV
--             FIELD_DELIMITER = '|'
--             SKIP_HEADER = 1
--             FIELD_OPTIONALLY_ENCLOSED_BY ='"' 
--             TRIM_SPACE = TRUE 
--             EMPTY_FIELD_AS_NULL = TRUE
--             NULL_IF = ('NULL', 'null','Null','',' '));

-- COPY INTO 's3://hypersonixdata/Smart_and_Final/swf_dump/olap_inventory_details_3_1_'
-- FROM (select * from dev_smartnfinal.public.olap_inventory_details_3  where created_at = current_date)
-- CREDENTIALS = (AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
-- FILE_FORMAT = (TYPE = CSV
--             FIELD_DELIMITER = '|'
--             SKIP_HEADER = 1
--             FIELD_OPTIONALLY_ENCLOSED_BY ='"' 
--             TRIM_SPACE = TRUE 
--             EMPTY_FIELD_AS_NULL = TRUE
--             NULL_IF = ('NULL', 'null','Null','',' '));


-- COPY INTO 's3://hypersonixdata/Smart_and_Final/swf_dump/dim_products_1_'
-- FROM (select * from dev_smartnfinal.public.dim_products  where created_at = current_date)
-- CREDENTIALS = (AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
-- FILE_FORMAT = (TYPE = CSV
--             FIELD_DELIMITER = '|'
--             SKIP_HEADER = 1
--             FIELD_OPTIONALLY_ENCLOSED_BY ='"' 
--             TRIM_SPACE = TRUE 
--             EMPTY_FIELD_AS_NULL = TRUE
--             NULL_IF = ('NULL', 'null','Null','',' '));

-- COPY INTO 's3://hypersonixdata/Smart_and_Final/swf_dump/dim_store_new_1_'
-- FROM (select * from dev_smartnfinal.public.dim_store_new  where created_at = current_date)
-- CREDENTIALS = (AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
-- FILE_FORMAT = (TYPE = CSV
--             FIELD_DELIMITER = '|'
--             SKIP_HEADER = 1
--             FIELD_OPTIONALLY_ENCLOSED_BY ='"' 
--             TRIM_SPACE = TRUE 
--             EMPTY_FIELD_AS_NULL = TRUE
--             NULL_IF = ('NULL', 'null','Null','',' '));

-- COPY INTO 's3://hypersonixdata/Smart_and_Final/swf_dump/dim_store_1_'
-- FROM (select * from dev_smartnfinal.public.dim_store  where created_at = current_date)
-- CREDENTIALS = (AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
-- FILE_FORMAT = (TYPE = CSV
--             FIELD_DELIMITER = '|'
--             SKIP_HEADER = 1
--             FIELD_OPTIONALLY_ENCLOSED_BY ='"' 
--             TRIM_SPACE = TRUE 
--             EMPTY_FIELD_AS_NULL = TRUE
--             NULL_IF = ('NULL', 'null','Null','',' '));

-- COPY INTO 's3://hypersonixdata/Smart_and_Final/swf_dump/ecom_stockout_flash_'
-- FROM (select * from dev_smartnfinal.public.ecom_stockout_flash  where created_at = current_date)
-- CREDENTIALS = (AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
-- FILE_FORMAT = (TYPE = CSV
--             FIELD_DELIMITER = '|'
--             SKIP_HEADER = 1
--             FIELD_OPTIONALLY_ENCLOSED_BY ='"' 
--             TRIM_SPACE = TRUE 
--             EMPTY_FIELD_AS_NULL = TRUE
--             NULL_IF = ('NULL', 'null','Null','',' '));

            



