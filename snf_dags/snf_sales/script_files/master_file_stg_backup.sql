ALTER WAREHOUSE DEV SET WAREHOUSE_SIZE=LARGE;

truncate table public.sales_budget_stag;

COPY INTO PUBLIC.sales_budget_stag
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/SalesBudget
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' SKIP_HEADER = 1 DATE_FORMAT = 'YYYY/MM/DD')
FORCE = TRUE;
