ALTER WAREHOUSE DEV SET WAREHOUSE_SIZE=LARGE;

delete from public.ecom_stockout_flash where transaction_date >= '{maxdate}';

INSERT INTO public.ecom_stockout_flash
(transaction_date, year, quarter, month_no, week_no, day_of_week, day_name, month_name, month_short, month_year, week_str, fis_p_str, fis_w_str, fis_week, fis_period, fis_quarter, fis_year, named_day, channel_level_1,channel_level_2, shipped_quantity, ordered_quantity, substockout_without_sub, substockout_with_sub, loss_sales, stockouts_with_sub, stockouts_without_sub, store_count, transaction_count,created_at)
select e.transaction_date,
d.year,
d.quarter,
d.month as month_no,
d.week as week_no,
f.day_of_week,
d.day_name,
d.month_name,
d.month_short,
d.month_year,
d.week_str,
f.fis_p_str,
f.fis_w_str,
f.fis_week_varchar as fis_week,
f.fis_period_varchar as fis_period,
f.fis_quarter,
f.fis_year,
d.named_day,
case when e.partner='Shop Dire' then '01 - ShopDirect'
		when e.partner in ('Instacart','Shipt') then '02 - Marketplace'
		else e.partner end as channel_level_1,
	case when e.partner='Shop Dire' then '01 - ShopDirect - B2C Delivery'
		when e.partner='Instacart' then '06 - Instacart Marketplace Delivery'
		when e.partner='Shipt' then '08 - Shipt Marketplace Delivery'
		else e.partner end as channel_level_2,
	sum(e.shipped_quantity) as shipped_quantity,
	sum(e.ordered_quantity)as ordered_quantity,
	sum(e.substockout_without_sub)as substockout_without_sub,
	sum(e.substockout_with_sub)as substockout_with_sub,
	sum(e.loss_sales)as loss_sales,
	(sum(e.substockout_with_sub)/sum(e.ordered_quantity))as Stockouts_With_Sub,
	(sum(e.substockout_without_sub)/sum(e.ordered_quantity))as Stockouts_Without_Sub,
	count(distinct e.store_number )AS store_count,
	count(distinct e.order_id)as transaction_count,
	current_date  as created_at
from public.olap_ecommerce e
left join public.dim_date d on d.date=e.transaction_date
left join public.dim_fiscal_calendar f on f.datekey=e.transaction_date
where e.created_at=current_date 
group by e.transaction_date,d.year,d.quarter,d.month ,d.week ,f.day_of_week,d.day_name,d.month_name,d.month_short,d.month_year,d.week_str,f.fis_p_str,f.fis_w_str,f.fis_week_varchar,f.fis_period_varchar,f.fis_quarter,f.fis_year,d.named_day,e.partner 
;

---Update for WTD,YTD-------

update public.ecom_stockout_flash set 
w_stout_with_sub=t.Stockouts_With_Sub,
w_stout_without_sub=t.Stockouts_Without_Sub,
w_loss_sales=t.loss_sales
from public.ecom_stockout_flash f 
join (
	select m.dt, e.channel_level_2,sum(e.ordered_quantity)as ordered_quantity,
	(sum(e.substockout_with_sub)/sum(e.ordered_quantity))as Stockouts_With_Sub,
	(sum(e.substockout_without_sub)/sum(e.ordered_quantity))as Stockouts_Without_Sub,
	sum(transaction_count)as transaction_count,
	sum(loss_sales)as loss_sales
	from public.ecom_stockout_flash e
	 join public.date_compare m on 1=1
	where e.transaction_date BETWEEN m.wtd_stdt and wtd_endt and m.dt >='2019-12-01'
	group by 1,2
	)t on f.channel_level_2=t.channel_level_2 and t.dt=f.transaction_date
where f.created_at=current_date 
;

update public.ecom_stockout_flash set 
p_stout_with_sub=t.Stockouts_With_Sub,
p_stout_without_sub=t.Stockouts_Without_Sub,
p_loss_sales=t.loss_sales
from public.ecom_stockout_flash f 
join (
	select m.dt, e.channel_level_2,sum(e.ordered_quantity)as ordered_quantity,
	(sum(e.substockout_with_sub)/sum(e.ordered_quantity))as Stockouts_With_Sub,
	(sum(e.substockout_without_sub)/sum(e.ordered_quantity))as Stockouts_Without_Sub,
	sum(transaction_count)as transaction_count,
	sum(loss_sales)as loss_sales
	from public.ecom_stockout_flash e
	 join public.date_compare m on 1=1
	where e.transaction_date BETWEEN m.ptd_stdt and m.ptd_endt and m.dt >='2019-12-01'
	group by 1,2
	)t on f.channel_level_2=t.channel_level_2 and t.dt=f.transaction_date
where f.created_at=current_date 
;

update public.ecom_stockout_flash set 
q_stout_with_sub=t.Stockouts_With_Sub,
q_stout_without_sub=t.Stockouts_Without_Sub,
q_loss_sales=t.loss_sales
from public.ecom_stockout_flash f 
join (
	select m.dt, e.channel_level_2,sum(e.ordered_quantity)as ordered_quantity,
	(sum(e.substockout_with_sub)/sum(e.ordered_quantity))as Stockouts_With_Sub,
	(sum(e.substockout_without_sub)/sum(e.ordered_quantity))as Stockouts_Without_Sub,
	sum(transaction_count)as transaction_count,
	sum(loss_sales)as loss_sales
	from public.ecom_stockout_flash e
	 join public.date_compare m on 1=1
	where e.transaction_date BETWEEN m.qtd_stdt and m.qtd_endt and m.dt >='2019-12-01'
	group by 1,2
	)t on f.channel_level_2=t.channel_level_2 and t.dt=f.transaction_date
where f.created_at=current_date 
;

update public.ecom_stockout_flash set 
y_stout_with_sub=t.Stockouts_With_Sub,
y_stout_without_sub=t.Stockouts_Without_Sub,
y_loss_sales=t.loss_sales
from public.ecom_stockout_flash f 
join (
	select m.dt, e.channel_level_2,sum(e.ordered_quantity)as ordered_quantity,
	(sum(e.substockout_with_sub)/sum(e.ordered_quantity))as Stockouts_With_Sub,
	(sum(e.substockout_without_sub)/sum(e.ordered_quantity))as Stockouts_Without_Sub,
	sum(transaction_count)as transaction_count,
	sum(loss_sales)as loss_sales
	from public.ecom_stockout_flash e
	 join public.date_compare m on 1=1
	where e.transaction_date BETWEEN m.ytd_stdt and m.ytd_endt and m.dt >='2019-12-01'
	group by 1,2
	)t on f.channel_level_2=t.channel_level_2 and t.dt=f.transaction_date
where f.created_at=current_date 
;


update public.ecom_stockout_flash set 
w_store_count=t.store_count
from public.ecom_stockout_flash f 
join (
	select m.dt, case when o.partner='Shop Dire' then '01 - ShopDirect - B2C Delivery'
		when o.partner='Instacart' then '06 - Instacart Marketplace Delivery'
		when o.partner='Shipt' then '08 - Shipt Marketplace Delivery'
		else o.partner end as channel_level_2,
		count(distinct o.store_number)as store_count
from public.olap_ecommerce o
join public.date_compare m on 1=1
where o.transaction_date BETWEEN m.wtd_stdt and wtd_endt  and m.dt>='2019-12-01'
	group by 1,2,channel_level_2
	)t on f.channel_level_2=t.channel_level_2 and t.dt=f.transaction_date
where f.created_at=current_date 
;
update public.ecom_stockout_flash set 
p_store_count=t.store_count
from public.ecom_stockout_flash f 
join (
	select m.dt, case when o.partner='Shop Dire' then '01 - ShopDirect - B2C Delivery'
		when o.partner='Instacart' then '06 - Instacart Marketplace Delivery'
		when o.partner='Shipt' then '08 - Shipt Marketplace Delivery'
		else o.partner end as channel_level_2,
		count(distinct o.store_number)as store_count
from public.olap_ecommerce o
join public.date_compare m on 1=1
where o.transaction_date BETWEEN m.ptd_stdt and m.ptd_endt  and m.dt>='2019-12-01'
	group by 1,2,channel_level_2
	)t on f.channel_level_2=t.channel_level_2 and t.dt=f.transaction_date
where f.created_at=current_date 
;
update public.ecom_stockout_flash set 
q_store_count=t.store_count
from public.ecom_stockout_flash f 
join (
	select m.dt, case when o.partner='Shop Dire' then '01 - ShopDirect - B2C Delivery'
		when o.partner='Instacart' then '06 - Instacart Marketplace Delivery'
		when o.partner='Shipt' then '08 - Shipt Marketplace Delivery'
		else o.partner end as channel_level_2,
		count(distinct o.store_number)as store_count
from public.olap_ecommerce o
join public.date_compare m on 1=1
where o.transaction_date BETWEEN m.qtd_stdt and m.qtd_endt  and m.dt>='2019-12-01'
	group by 1,2,channel_level_2
	)t on f.channel_level_2=t.channel_level_2 and t.dt=f.transaction_date
where f.created_at=current_date
;
update public.ecom_stockout_flash set 
y_store_count=t.store_count
from public.ecom_stockout_flash f 
join (
	select m.dt, case when o.partner='Shop Dire' then '01 - ShopDirect - B2C Delivery'
		when o.partner='Instacart' then '06 - Instacart Marketplace Delivery'
		when o.partner='Shipt' then '08 - Shipt Marketplace Delivery'
		else o.partner end as channel_level_2,
		count(distinct o.store_number)as store_count
from public.olap_ecommerce o
join public.date_compare m on 1=1
where o.transaction_date BETWEEN m.ytd_stdt and m.ytd_endt  and m.dt>='2019-12-01'
	group by 1,2,channel_level_2
	)t on f.channel_level_2=t.channel_level_2 and t.dt=f.transaction_date
where f.created_at=current_date
;

----------------Update for APs---------
update public.ecom_stockout_flash set 					 
aps_loss_sales=loss_sales/store_count,
w_aps_loss_sales=w_loss_sales/w_store_count,
p_aps_loss_sales=p_loss_sales/p_store_count,
q_aps_loss_sales=q_loss_sales/q_store_count,
y_aps_loss_sales=y_loss_sales/y_store_count
where public.ecom_stockout_flash.created_at= current_date ;


update public.entity_log
set row_count =(select count(*) from public.ecom_stockout_flash),
most_recent_date=(select max(transaction_date) from public.ecom_stockout_flash),
min_date = (select min(transaction_date) from public.ecom_stockout_flash),
lud=(select current_date ),
most_recent_time = (select max(transaction_date)::timestamp from public.ecom_stockout_flash),
lud_time=(select current_timestamp)
where entity_name='ecom_stockout';

------------- Unload File ---------------------------------------

COPY INTO 's3://hypersonixdata/Smart_and_Final/swf_dump/ecom_stockout_flash_1_'
FROM (select * from dev_smartnfinal.public.ecom_stockout_flash  where created_at = current_date )
CREDENTIALS = (AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV
            FIELD_DELIMITER = '|'
            SKIP_HEADER = 1
            FIELD_OPTIONALLY_ENCLOSED_BY ='"' 
            TRIM_SPACE = TRUE 
            EMPTY_FIELD_AS_NULL = TRUE
            NULL_IF = ('NULL', 'null','Null','',' '));
            
            



