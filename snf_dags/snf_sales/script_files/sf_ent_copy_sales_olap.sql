-- ================= Loading Data =====================================================


delete from prod_smartnfinal.public.olap_item_details where record_type in (1,2,3,4) and  transaction_date >='{maxdate}';
delete from prod_smartnfinal.public.olap_item_details where record_type=5;

copy into prod_smartnfinal.public.olap_item_details
from s3://hypersonixdata/Smart_and_Final/swf_dump/olap_item_details_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

delete from prod_smartnfinal.public.transaction_details where businessdate >='{maxdate}';

copy into prod_smartnfinal.public.transaction_details
from s3://hypersonixdata/Smart_and_Final/swf_dump/transaction_details_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

delete from prod_smartnfinal.public.transactions where business_date >='{maxdate}';

copy into prod_smartnfinal.public.transactions
from s3://hypersonixdata/Smart_and_Final/swf_dump/transactions_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

delete from prod_smartnfinal.public.tenders where business_date >='{maxdate}';

copy into prod_smartnfinal.public.tenders
from s3://hypersonixdata/Smart_and_Final/swf_dump/tenders_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

delete from prod_smartnfinal.public.item_discounts where business_date >='{maxdate}';

copy into prod_smartnfinal.public.item_discounts
from s3://hypersonixdata/Smart_and_Final/swf_dump/item_discounts_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;


truncate table prod_smartnfinal.public.dim_products;

copy into prod_smartnfinal.public.dim_products
from s3://hypersonixdata/Smart_and_Final/swf_dump/dim_products_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

truncate table prod_smartnfinal.public.dim_store_new;

copy into prod_smartnfinal.public.dim_store_new
from s3://hypersonixdata/Smart_and_Final/swf_dump/dim_store_new_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

truncate table prod_smartnfinal.public.dim_store;

copy into prod_smartnfinal.public.dim_store
from s3://hypersonixdata/Smart_and_Final/swf_dump/dim_store_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;


-- copy into prod_smartnfinal.public.price_new
-- from s3://hypersonixdata/Smart_and_Final/swf_dump/price_new_1_
-- credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
-- FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
--                EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
-- FORCE = TRUE


update prod_smartnfinal.public.olap_item_details o 
set py_comp_status=py.comp_status_new 
from (select distinct td.businessdate,td.businessdate-364 as py_business_date,sn.new_store_number as store_number, 
      case when sn.comp_start_date<=td.businessdate and sn.storeclose_date>=td.businessdate 
      and DATE_PART(year,sn.storeclose_date)=2050  then 'Comp' else 'NonComp' end as comp_status_new    
      from prod_smartnfinal.public.transaction_details td    
      left join prod_smartnfinal.public.dim_store s on s.store_number=td.storenumber 
      left join prod_smartnfinal.public.dim_store_new sn on s.store_number=sn.store_number    
      where td.businessdate >='{fromdate}')py    
where  o.transaction_date=py.py_business_date and o.store_number=py.store_number;

update prod_smartnfinal.public.entity_log set row_count =(select count(*) from prod_smartnfinal.public.olap_item_details where record_type IN (1,2,3,4)),
most_recent_date=(select max(transaction_date) from prod_smartnfinal.public.olap_item_details where record_type IN (1,2,3,4)),
lud=(select current_date),
min_date = (select min(transaction_date) from prod_smartnfinal.public.olap_item_details where record_type IN (1,2,3,4)),
most_recent_time = (select max(transaction_date)::timestamp from prod_smartnfinal.public.olap_item_details where record_type IN (1,2,3,4)),
lud_time=(select current_timestamp)
where entity_name='summary';


