# import boto3
# import pathlib
# import os
# import re
# import pandas as pd
# from airflow import DAG
# from datetime import date
# from datetime import datetime,timedelta
# from airflow.utils.dates import days_ago
# from airflow.operators import PythonOperator
# from airflow.operators.bash_operator import BashOperator
# from etl_functions import sftp_download
# from etl_functions import transforming_data
# from etl_functions import upload_file_to_S3
# from snowflake_connection import upload_to_snowflake
# from etl_functions import removing_raw_process_s3_files
# from configparser import ConfigParser, RawConfigParser
# from etl_functions import get_redshift_connect_string
# from airflow.exceptions import AirflowException
# from etl_functions import sender_msg

# ####### importing credential info #############################

# config = RawConfigParser()
# res = config.read('/home/ubuntu/airflow/dags/credential.cfg')

# bucket_name = config.get('S3', 'S3_B_NAME')
# bucket_key = config.get('S3', 'S3_RDS_B_KEY')

# username = config.get('SNF_SFTP', 'USERNAME')
# password = config.get('SNF_SFTP', 'PASSWORD')

# session = boto3.session.Session()
# credentials = session.get_credentials()
# s3_access_key = credentials.access_key
# s3_secret_key = credentials.secret_key

# ####### importing path ########################################

# path = '/home/ubuntu/airflow/dags/snf_inventory/'
# html_path = '/home/ubuntu/airflow/dags/'

# files = ["DC_BOH", "Store_Alloc", "Item_WT_Lookup", "In_Transit_PO","shipper_items","checkstand"]
# comp_size_dict = {"DC_BOH":700000, "Store_Alloc":3000000, "Item_WT_Lookup":6000, 
#                   "In_Transit_PO":600000, "shipper_items":30000 ,"checkstand" :2500}


# ####### creating email message ################################

# miss_match_msg= dict(Body = 'Please check all files in sftp',
#                  Subject = 'Miss-Match Alert: Redshift - files size are below the min size ')
# sucess_msg= dict(Body = 'All files Size are matched and loaded sucessfully',
#                  Subject = 'Redshift: SnF Inv Others files loaded Successfully')


# ####### copy command function #########################

# # def transforming_inv_data(path, raw_path,process_path, sep, pattern):
# #     files = os.listdir(os.path.join(path + raw_path))
# #     match_files = [fn for fn in files if re.match('|'.join(pattern),fn)]
# #     for fn in match_files:
# #         print(f"Processing files {fn}")
# #         if fn.startswith('shipper'):
# #             df = pd.read_csv(os.path.join(path,raw_path,fn),sep=sep, header=None,na_values= ' ')
# #             df[2] = df[2].astype('Int64')
# #             df['filename'] = fn
# #             df['created_at'] = date.today().strftime('%Y-%m-%d')
# #             df.to_csv(os.path.join(path,process_path,fn),sep=sep,index=False)
# #         elif fn.startswith('checkstand'):
# #             df = pd.read_csv(os.path.join(path,raw_path,fn),sep=sep, header=None, na_values= ' ')
# #             df[1] = df[1].astype('Int64')
# #             df['filename'] = fn
# #             df['created_at'] = date.today().strftime('%Y-%m-%d')
# #             df.to_csv(os.path.join(path,process_path,fn),sep=sep,index=False)
# #         else:
# #             df = pd.read_csv(os.path.join(path,raw_path,fn),sep=sep, header=None)
# #             df['filename'] = fn
# #             df['created_at'] = date.today().strftime('%Y-%m-%d')
# #             df.to_csv(os.path.join(path ,process_path,fn),sep=sep,index=False)
            
            


# copy_qry = """copy smartnfinal.{0} FROM
#            's3://hypersonixdata/Smart_and_Final/rds_dump/{1}' 
#            access_key_id '{2}' secret_access_key '{3}' dateformat 'auto'
#            maxerror as 0 DELIMITER ',' ignoreheader 1;"""


# def copy_cmd_execute (query, db_con):
#     try:
#         curr = db_con.cursor()
#         curr.execute(query)
#         db_con.commit()
#     except psycopg2.DatabaseError as error:
#         print(error)

# def upload_to_redshift(path,pattern):

#     files = os.listdir(os.path.join(path,'rds_process_files'))
#     match_files = [fn for fn in files if re.match('|'.join(pattern),fn)]
  
#     rds_dev_con = get_redshift_connect_string('dev')
    
#     for fn in match_files:
#         print(f"Loading ({fn}) into redshift")

#         if re.match(r'^DC_BOH', fn):
#             copy_cmd_execute(copy_qry.format('dc_boh',fn,s3_access_key,s3_secret_key),rds_dev_con)
#         elif re.match(r'^Item_WT_Lookup', fn):
#             copy_cmd_execute(copy_qry.format('item_wt_lookup',fn,s3_access_key,s3_secret_key),rds_dev_con)
#         elif re.match(r'^Store_Alloc', fn):
#             copy_cmd_execute(copy_qry.format('store_alloc',fn,s3_access_key,s3_secret_key),rds_dev_con)
#         elif re.match(r'^In_Transit_PO', fn):
#             copy_cmd_execute(copy_qry.format('in_transit_po',fn,s3_access_key,s3_secret_key),rds_dev_con)
#         elif re.match(r'^shipper', fn):
#             copy_cmd_execute(copy_qry.format('shipper',fn,s3_access_key,s3_secret_key),rds_dev_con)
#         elif re.match(r'^checkstand', fn):
#             copy_cmd_execute(copy_qry.format('checkstand',fn,s3_access_key,s3_secret_key),rds_dev_con)

#     rds_dev_con.close()
    
# def collect_info(dict_file,query,conn):
#     df = pd.DataFrame()
#     idx = 0
#     for tbl,col in dict_file.items():
#         df1 = pd.read_sql(query.format(col_name =col ,table_name = tbl), conn)
#         df1.insert(loc=idx, column='table_name', value=tbl)
#         df = pd.concat([df,df1], axis=0).reset_index(drop=True)
#     return(df)

# snf_inv_other_dict = {'DC_BOH':'filename','Store_Alloc':'filename','Item_WT_Lookup':'filename',
#                       'In_Transit_PO':'filename','shipper':'filename', 'checkstand':'filename'}

# def success_dag_mail(path_name):
#     rds_dev_con = get_redshift_connect_string('dev')
    
#     try:
#         file_query = """select {col_name}, count(*), created_at 
#                         from smartnfinal.{table_name}  
#                         group by {col_name},created_at order by {col_name} desc limit 1;"""

#         df2 = collect_info(snf_inv_other_dict,file_query,rds_dev_con)
#         df2.to_html(os.path.join(path_name,'html_files','rds_inv_other_file.html'),
#                                     justify='center',col_space=100,index=False)
#         sender_msg(sucess_msg,'sudhakar.g@hypersonix.io',is_html=True,html_file= 'rds_inv_other_file.html')
#     except :
#         raise AirflowException(f"HTML Mail ERROR")


# ######### defining dags ####################################


# default_args = {'owner': 'Sudhakar','depends_on_past': True,
#                'email': ['sudhakar.g@hypersonix.io'],'email_on_failure': True}

# dag = DAG(
#     dag_id = 'rds_snf_inv_other_file',
#     default_args=default_args,
#     start_date = datetime(2020,11,5),
#     schedule_interval = '00 09 * * *')


# downloading_files_task = PythonOperator(
#     task_id = 'downloading_files_sftp',
#     provide_context=True,
#     python_callable =sftp_download,
#     retry_delay = timedelta(minutes=30),
#     retries =  4,
#     op_kwargs={
#         'path':path,
#         'u_name' : username,
#         'p_word' : password,
#         'msg1': miss_match_msg ,
#         'msg2': sucess_msg,
#         'files_name': files ,
#         'comp_size_files':comp_size_dict,
#         'sftp_dir': '/upload/Inventory/',
#         'val_path': 'rds_validation_files',
#         'raw_path': 'rds_raw_files',
#         'val_file': 'rds_inv_others_val.json',
#     },
#     dag = dag)

# process_files_task = PythonOperator(
#     task_id = 'transforming_raw_files',
#     python_callable =transforming_data,
#     op_kwargs={'path':path,'sep': ',','pattern':files,
#                'raw_path':'rds_raw_files','process_path':'rds_process_files'},
#     dag = dag)

# s3_transfer_task = PythonOperator(
#     task_id = 'loading_files_into_s3',
#     python_callable =upload_file_to_S3,
#     op_kwargs={'path': path, 'pattern':files,
#                'process_path':'rds_process_files',
#                'bucket_name':bucket_name,'bucket_key':bucket_key},
#     dag = dag)

# redshift_load_task = PythonOperator(
#     task_id = 'copy_file_into_redshift',
#     python_callable =upload_to_redshift,
#     op_kwargs={'path':path, 'pattern':files},
#     dag = dag)

# remove_source_files_task = PythonOperator(
#     task_id = 'deleting_raw_process_s3_files',
#     python_callable =removing_raw_process_s3_files,
#     op_kwargs={'path': path,'pattern':files,
#               'bucket_name':bucket_name,'bucket_key':bucket_key,
#               'raw_path':'rds_raw_files','process_path':'rds_process_files'},
#     dag = dag)

# success_dag_mail_task = PythonOperator(
#     task_id = 'success_dag_mail',
#     python_callable =success_dag_mail,
#     op_kwargs={'path_name': html_path},
#     dag = dag)

# downloading_files_task >> process_files_task >> s3_transfer_task  >> redshift_load_task >> remove_source_files_task
# remove_source_files_task >> success_dag_mail_task










