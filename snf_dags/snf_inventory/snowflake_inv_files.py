import boto3
import pathlib
import os
import re
import pandas as pd
from airflow import DAG
from datetime import date
from datetime import datetime,timedelta
from airflow.utils.dates import days_ago
from airflow.operators.python import PythonOperator
from airflow.operators.bash_operator import BashOperator
from snf_dags.etl_functions import sftp_download
from snf_dags.etl_functions import transforming_data
from snf_dags.etl_functions import upload_file_to_S3
from snf_dags.snowflake_connection import upload_to_snowflake
from snf_dags.etl_functions import removing_raw_process_s3_files
from configparser import ConfigParser, RawConfigParser
from airflow.sensors.external_task_sensor import ExternalTaskSensor
from airflow.exceptions import AirflowException
from snf_dags.etl_functions import sf_connect_sting
from snf_dags.etl_functions import prod_sf_connect_sting
from snf_dags.etl_functions import sender_msg

####### importing credential info #############################

config = RawConfigParser()
res = config.read(r'/home/ubuntu/airflow/dags/snf_dags/credential.ini')

bucket_name = config.get('S3', 'S3_B_NAME')
bucket_key = config.get('S3', 'S3_SWF_B_KEY')



username = config.get('SNF_SFTP', 'USERNAME')
password = config.get('SNF_SFTP', 'PASSWORD')

sf_wh = config.get('SNOWFLAKE', 'SF_WAREHOUSE')
sf_db =  config.get('SNOWFLAKE', 'SF_DATABASE')

session = boto3.session.Session('s3')
credentials = session.get_credentials()
s3_access_id = credentials.access_key
s3_secret_key = credentials.secret_key

####### importing path ########################################

path = '/home/ubuntu/airflow/dags/snf_dags/snf_inventory/'
html_path = '/home/ubuntu/airflow/dags/snf_dags/'

files = ["store_receipts","inv_adjustments","transfer_in_out","physical_inventory","dc_returns",
         "dc_to_store_orderables","delivery_schedule"]

# comp_size_dict = {"store_receipts":800000, "inv_adjustment":0,
#                   "transfer_in_out":0, "physical_inventory":0,"dc_returns":0,
#                   "dc_to_store_orderables":0, "delivery_schedule":0}

comp_size_dict = {"store_receipts":3000000, "inv_adjustment":800000,
                  "transfer_in_out":2000, "physical_inventory":0,"dc_returns":0,
                  "dc_to_store_orderables":0, "delivery_schedule":40000}


miss_match_msg= dict(Body = 'Please check all files in sftp',
                 Subject = 'Miss-Match Alert: Snowflake - SnF Inv files size are below the min size')
sucess_msg= dict(Body = 'All SnF Inv files Size are matched and loaded sucessfully',
                 Subject = 'Snowflake: SnF Inv files loaded Successfully')


####### copy command function #########################

insert_cmd = """insert into public.stg_physical_inventory_full
select * from (select item_id, store_id, inventory_date, avg(cost) cost, avg(price)price, 
ROW_NUMBER() over(partition by item_id, store_id order by inventory_date desc ) rn, dateadd(day,+1,current_date)::date as dt
from public.physical_inventory 
where inventory_date <= dateadd(day,+1,current_date) group by item_id, store_id, inventory_date) a where  rn = 1;"""

####### extra function #########################

def query_df_callable(cursor, query_list):

    final_df = pd.DataFrame()
    for qry in query_list:
        cursor.execute(qry)
        col_names = ','.join([col[0] for col in cursor.description]).lower()
        for c in cursor:
            df = pd.DataFrame(c, columns=[col_names])
            final_df = pd.concat([final_df,df], axis=1)
    return(final_df)


def run_sf_multple_queries(cursor,path_name,file,**kwargs):
    scriptContents = pathlib.Path(os.path.join(path_name,'script_files',file)).read_text(encoding='utf-8')
    sqlStatements = scriptContents.split(sep=';')
    for statement in sqlStatements[:-1]:
        cursor.execute(f'{statement.format(**kwargs)}' + ';')


def run_sf_cmd(path,pattern):
    
    sf_con = sf_connect_sting()
    sf_curr = sf_con.cursor()
    sf_curr.execute('USE DATABASE %s;' % sf_db)
    sf_curr.execute('USE WAREHOUSE %s;' % sf_wh)
    
    run_sf_multple_queries(sf_curr,path,'snf_inv_file_loading.sql',
                           aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
    

    
def loading_inv_file_db(path,pattern):

    files = os.listdir(os.path.join(path,'process_files'))
    match_files = [fn for fn in files if re.match('|'.join(pattern),fn)]
  
    sf_con = sf_connect_sting()
    sf_curr = sf_con.cursor()
    sf_curr.execute('USE DATABASE %s;' % sf_db)
    sf_curr.execute('USE WAREHOUSE %s;' % sf_wh)
    
    prod_sf_con =prod_sf_connect_sting()
    prod_curr = prod_sf_con.cursor()
    prod_curr.execute('USE DATABASE %s;' % 'prod_smartnfinal')
    prod_curr.execute('USE WAREHOUSE %s;' % 'dev')
    
    sf_curr.execute(insert_cmd)
    sf_curr.execute('truncate table public.olap_inventory_details_updated;')
    for fn in match_files:
        print(f"Filename ({fn})")

        if re.match(r'^store_receipts', fn):
            dt_num = re.sub("[^\d]", "", fn)[0:8]
            min_date = datetime.strptime(dt_num, '%d%m%Y').date().strftime('%Y-%m-%d')
            run_sf_multple_queries(sf_curr,path,'swf_inv_record_1.sql',filename=fn)
            run_sf_multple_queries(sf_curr,path,'swf_inv_record_3.sql',mindate=min_date)
        elif re.match(r'^transfer_in_out', fn):
            run_sf_multple_queries(sf_curr,path,'swf_inv_record_2.sql',filename=fn)
            run_sf_multple_queries(sf_curr,path,'swf_inv_record_4.sql',filename=fn)
        elif re.match(r'^inv_adjustments', fn):
            run_sf_multple_queries(sf_curr,path,'swf_inv_record_5_8.sql',filename=fn)
        elif re.match(r'^dc_returns', fn):
            run_sf_multple_queries(sf_curr,path,'swf_inv_record_9.sql',filename=fn)
        elif re.match(r'^physical_inventory', fn):
            run_sf_multple_queries(sf_curr,path,'swf_inv_record_10.sql',filename=fn)
            
    run_sf_multple_queries(sf_curr,path,'swf_inv_update_cmd.sql', 
                          aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
    
    print("Loading Inventory data into Prod")
    
    run_sf_multple_queries(prod_curr,path,'sf_ent_copy_inventory.sql',
                          aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
    sf_curr.close()
    prod_curr.close()
    
    
    
    
############# Success Mail Function ########################    
    
def collect_info(dict_file,query,conn):
    idx = 0
    final_df = pd.DataFrame()
    for tbl,col in dict_file.items():
        conn.execute(query.format(col_name =col,table_name = tbl))
        col_names = ','.join([col[0] for col in conn.description]).lower()
        col_list = col_names.split(",")
        df = pd.DataFrame(conn, columns = col_list)
        df.insert(loc=idx, column='table_name', value=tbl)
        final_df = pd.concat([final_df,df], axis=0)
    return(final_df)

snf_inv_dict = {'store_receipt':'filename','inventory_adjustment':'filename',
                'transfer_in_out':'filename','physical_inventory':'filename', 
                'dc_return': 'filename', 'dc_to_store':'filename','delivery_schedule':'filename' }

snf_inv_olap_dict = {'olap_inventory_details_3':'record_type','olap_inventory_details_updated':'record_type'}



def success_dag_mail(path_name):
    
    sf_con = sf_connect_sting()
    curr = sf_con.cursor()
    curr.execute('USE DATABASE %s;' % sf_db)
    curr.execute('USE WAREHOUSE %s;' % sf_wh)
    
    try:
        file_query = """select {col_name}, count(*) as count, created_at 
                        from public.{table_name}  
                        group by {col_name},created_at order by created_at desc limit 1;"""

        df1 = collect_info(snf_inv_dict,file_query,curr)
        df1.to_html(os.path.join(path_name,'html_files','sf_inv_file.html'),justify='center', col_space=100,index=False)
        sender_msg(sucess_msg, 'v_arush.s@hypersonix.ai, navaneeth@hypersonix.ai',is_html=True,html_file= 'sf_inv_file.html')
        
        olap_query = """select {col_name}, count(*) as count, created_at 
                        from public.{table_name}
                        where created_at = current_date
                        group by {col_name},created_at order by {col_name};"""

        df2 = collect_info(snf_inv_olap_dict,olap_query,curr)
        df2.to_html(os.path.join(path_name,'html_files','sf_inv_olap_file.html'),
                                    justify='center',col_space=100,index=False)
        sender_msg(sucess_msg,'v_arush.s@hypersonix.ai, navaneeth@hypersonix.ai',is_html=True,html_file= 'sf_inv_olap_file.html')
    except :
        raise AirflowException(f"HTML Mail ERROR")


######### defining dags ####################################

# 05 12 * * *

default_args = {'owner': 'Sudhakar','depends_on_past': False,
               'email': ['v_arush.s@hypersonix.ai', 'navaneeth@hypersonix.ai'],'email_on_failure': True}

dag = DAG(
    dag_id = 'snowflake_inv_files_1',
    default_args=default_args,
    start_date = datetime(2021,10,12),
    schedule_interval = '10 12 * * *')

wait_for_sales_file_loaded_task = ExternalTaskSensor(
    task_id='wait_for_sales_file_loaded',
    external_dag_id='snowflake_snf_sales_1',
    external_task_id='success_dag_mail')


downloading_files_task = PythonOperator(
    task_id = 'downloading_files_sftp',
    provide_context=True,
    python_callable =sftp_download,
    retry_delay = timedelta(minutes=15),
    retries = 4,
    op_kwargs={
        'path':path,
        'u_name' : username,
        'p_word' : password,
        'msg1': miss_match_msg ,
        'msg2': sucess_msg,
        'files_name': files ,
        'comp_size_files':comp_size_dict,
        'sftp_dir': '/upload/Inventory/',
        'val_path': 'validation_files',
        'raw_path': 'raw_files',
        'val_file': 'snf_inv_files_val.json',
    },
    dag = dag)

process_files_task = PythonOperator(
    task_id = 'transforming_raw_files',
    python_callable =transforming_data,
    op_kwargs={'path':path,'sep': ',','pattern':files,
               'raw_path':'raw_files','process_path':'process_files'},
    dag = dag)

s3_transfer_task = PythonOperator(
    task_id = 'loading_files_into_s3',
    python_callable =upload_file_to_S3,
    op_kwargs={'path': path, 'pattern':files + ['inv_adjustment'],
               'process_path':'process_files',
               'bucket_name':bucket_name,'bucket_key':bucket_key},
    dag = dag)

snowflake_copy_inv_file_task = PythonOperator(
    task_id = 'copy_inv_files',
    python_callable =run_sf_cmd,
    op_kwargs={'path':path, 'pattern':files},
    dag = dag)

snowflake_load_inv_file_db_task = PythonOperator(
    task_id = 'load_inv_file_into_db',
    python_callable =loading_inv_file_db,
    op_kwargs={'path':path, 'pattern':files},
    dag = dag)

remove_source_files_task = PythonOperator(
    task_id = 'deleting_raw_process_s3_files',
    python_callable =removing_raw_process_s3_files,
    op_kwargs={'path': path,'pattern':files + ["olap_inventory_details_3_1_"],
              'bucket_name':bucket_name,'bucket_key':bucket_key,
              'raw_path':'raw_files','process_path':'process_files'},
    dag = dag)

success_dag_mail_task = PythonOperator(
    task_id = 'success_dag_mail',
    python_callable =success_dag_mail,
    op_kwargs={'path_name': html_path},
    dag = dag)


wait_for_sales_file_loaded_task >> downloading_files_task >> process_files_task >> s3_transfer_task
s3_transfer_task >> snowflake_copy_inv_file_task
snowflake_copy_inv_file_task >> snowflake_load_inv_file_db_task >> remove_source_files_task >> success_dag_mail_task

