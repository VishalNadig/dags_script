truncate table PUBLIC.DC_BOH_STG;

COPY INTO PUBLIC.DC_BOH_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/DC_BOH
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' SKIP_HEADER = 1 DATE_FORMAT = 'DD-MON-YY' TRIM_SPACE = TRUE)
FORCE = TRUE;

insert into DC_BOH
select ITEM ,
    LOC ,
    BOH ,
    BOHDATE,
    SOURCE ,
    FILENAME ,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.DC_BOH_STG;
    
truncate table PUBLIC.ITEM_WT_LOOKUP_STG;

COPY INTO PUBLIC.ITEM_WT_LOOKUP_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/Item_WT_Lookup
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' SKIP_HEADER = 1 DATE_FORMAT = 'DD-MON-YY' TRIM_SPACE = TRUE)
FORCE = TRUE;

insert into ITEM_WT_LOOKUP
select ITEM ,
    WEIGHT ,
    FILENAME ,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.ITEM_WT_LOOKUP_STG;
    
truncate table PUBLIC.STORE_ALLOC_STG;

COPY INTO PUBLIC.STORE_ALLOC_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/Store_Alloc
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' SKIP_HEADER = 1 DATE_FORMAT = 'DD-MON-YY' TRIM_SPACE = TRUE)
FORCE = TRUE;

insert into STORE_ALLOC
select ITEM ,
    LOC ,
    ALLOCQTY,
    ALLOCDATE,
    SOURCE,
    FILENAME ,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.STORE_ALLOC_STG;
    
truncate table PUBLIC.IN_TRANSIT_PO_STG;

COPY INTO PUBLIC.IN_TRANSIT_PO_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/In_Transit_PO
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' SKIP_HEADER = 1 DATE_FORMAT = 'DD-MON-YY' TRIM_SPACE = TRUE)
FORCE = TRUE;

insert into IN_TRANSIT_PO
select ITEM ,
    LOC ,
    QTY,
    DEL_DATE,
    SOURCE,
    FILENAME ,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.IN_TRANSIT_PO_STG;

truncate table PUBLIC.SHIPPER_STG;

COPY INTO PUBLIC.SHIPPER_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/shipper_items
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' SKIP_HEADER = 1 DATE_FORMAT = 'DD-MON-YY' TRIM_SPACE = TRUE)
FORCE = TRUE;

insert into SHIPPER
select CHILD_ITEM_ID ,
    TYPE ,
    PARENT_ITEM_ID,
    QUANTITY,
    FILENAME ,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.SHIPPER_STG;
    
truncate table PUBLIC.CHECKSTAND_STG;

COPY INTO PUBLIC.CHECKSTAND_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/checkstand
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' SKIP_HEADER = 1 DATE_FORMAT = 'DD-MON-YY' TRIM_SPACE = TRUE)
FORCE = TRUE;

insert into CHECKSTAND
select CHILD_ITEM_ID ,
    PARENT_ITEM_ID ,
    QUANTITY,
    FILENAME ,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.CHECKSTAND_STG;
    