ALTER WAREHOUSE DEV SET WAREHOUSE_SIZE=LARGE;

truncate table PUBLIC.DC_TO_STORE_STG;

COPY INTO PUBLIC.DC_TO_STORE_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/dc_to_store_orderables
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' SKIP_HEADER = 1 DATE_FORMAT = 'DD-MON-YY' TRIM_SPACE = TRUE)
FORCE = TRUE;

insert into DC_TO_STORE
select  ITEM ,
      SOURCE ,
      DEST ,
      EFF,
      DISC,
      MIN_QTY ,
      INC_QTY ,
      ORDERABLE_STATUS ,
      FILENAME ,
      to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.DC_TO_STORE_STG;
      
truncate table PUBLIC.TRANSFER_IN_OUT_STG;

COPY INTO PUBLIC.TRANSFER_IN_OUT_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/transfer_in_out
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' DATE_FORMAT = 'YYMMDD' 
               SKIP_HEADER = 1  TIMESTAMP_FORMAT = 'DD-MON-YY HH24.MI.SS.FF AM TZH:TZM' TRIM_SPACE = TRUE)
FORCE = TRUE;

insert into TRANSFER_IN_OUT
select  SOURCE ,
    SOURCE_TYPE ,
    BUSINESS_DATE,
    STORE_ID ,
    CATEGORY ,
    SUB_CATEGORY ,
    UPC ,
    ITEM_NUMBER ,
    GL_ACCOUNT ,
    BATCH ,
    AP_VENDOR ,
    DOCUMENT ,
    QUANTITY,
    COST ,
    RETAIL,
    DESCRIPTION ,
    DSD_SEQ ,
    ORDER_TYPE ,
    ACCOUNT_DEPARTMENT ,
    POS_DEPARTMENT ,
    TIMESTAMP,
    FILENAME ,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.TRANSFER_IN_OUT_STG;
    
truncate table PUBLIC.DC_RETURN_STG;

COPY INTO PUBLIC.DC_RETURN_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/dc_returns
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' SKIP_HEADER = 1 DATE_FORMAT = 'DD-MON-YY' TRIM_SPACE = TRUE)
FORCE = TRUE;

insert into DC_RETURN
select  SITE_ID ,
    STORE_ID ,
    ITEM_ID ,
    SHIP_DATE,
    QUANTITY ,
    COST ,
    FILENAME ,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.DC_RETURN_STG;
    
truncate table PUBLIC.INVENTORY_ADJUSTMENT_STG;

COPY INTO PUBLIC.INVENTORY_ADJUSTMENT_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/inv_adjustments
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' DATE_FORMAT = 'YYMMDD'
               SKIP_HEADER = 1 TIMESTAMP_FORMAT = 'DD-MON-YY HH24.MI.SS.FF AM TZH:TZM' TRIM_SPACE = TRUE)
FORCE = TRUE;

insert into INVENTORY_ADJUSTMENT
select  SOURCE ,
    SOURCE_TYPE ,
    BUSINESS_DATE,
    STORE_ID ,
    CATEGORY ,
    SUB_CATEGORY ,
    UPC ,
    ITEM_NUMBER ,
    GL_ACCOUNT ,
    BATCH ,
    AP_VENDOR ,
    DOCUMENT ,
    QUANTITY ,
    COST ,
    RETAIL ,
    DESCRIPTION ,
    DSD_SEQ ,
    ORDER_TYPE ,
    ACCOUNT_DEPARTMENT ,
    POS_DEPARTMENT ,
    TIMESTAMP,
    FILENAME ,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.INVENTORY_ADJUSTMENT_STG;
    
truncate table PUBLIC.STORE_RECEIPT_STG;

COPY INTO PUBLIC.STORE_RECEIPT_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/store_receipts
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' SKIP_HEADER = 1 DATE_FORMAT = 'DD-MON-YY' TRIM_SPACE = TRUE)
FORCE = TRUE;

insert into STORE_RECEIPT
select  ITEM_ID ,
    STORE_ID ,
    SHIP_DATE,
    QUANTITY ,
    SOURCE ,
    FILENAME ,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.STORE_RECEIPT_STG;
    
truncate table PUBLIC.DELIVERY_SCHEDULE_STG;

COPY INTO PUBLIC.DELIVERY_SCHEDULE_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/delivery_schedule
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' DATE_FORMAT = 'DD-MON-YY'
               SKIP_HEADER = 1  TIMESTAMP_FORMAT ='DD-MON-YY HH24:MI:SS' TRIM_SPACE = TRUE)
FORCE = TRUE;

insert into DELIVERY_SCHEDULE
select  WHS_ID ,
    WHS_ALTERNATE_ID ,
    WHS_NAME ,
    STORE_ID ,
    ORDER_TYPE ,
    CUTOFF_DATE,
    DELIVERY_DATE,
    FILENAME ,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.DELIVERY_SCHEDULE_STG;

truncate table PUBLIC.PHYSICAL_INVENTORY_STG;


COPY INTO PUBLIC.PHYSICAL_INVENTORY_STG
FROM s3://hypersonixdata/Smart_and_Final/swf_dump/physical_inventory
credentials=(AWS_KEY_ID='{aws_secret_id}' AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' SKIP_HEADER = 1 TRIM_SPACE = TRUE)
FORCE = TRUE;


insert into PHYSICAL_INVENTORY
select  ITEM_ID ,
    STORE_ID ,
    INVENTORY_DATE,
    QUANTITY ,
    COST ,
    PRICE ,
    FILENAME ,
    to_date(CREATED_AT, 'yyyy-mm-dd') from PUBLIC.PHYSICAL_INVENTORY_STG;