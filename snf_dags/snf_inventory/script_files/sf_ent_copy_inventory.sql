copy into prod_smartnfinal.public.olap_inventory_details_3
from s3://hypersonixdata/Smart_and_Final/swf_dump/olap_inventory_details_3_1_
credentials=(AWS_KEY_ID='{aws_secret_id}', AWS_SECRET_KEY='{aws_secret_key}')
FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|'  NULL_IF = ('NULL', 'null','Null','',' ') 
               EMPTY_FIELD_AS_NULL = TRUE  TRIM_SPACE = TRUE  FIELD_OPTIONALLY_ENCLOSED_BY='"')
FORCE = TRUE;

update prod_smartnfinal.public.entity_log
set row_count =(select count(*) from prod_smartnfinal.public.olap_inventory_details_3),
most_recent_date=(select max(transaction_date) from prod_smartnfinal.public.olap_inventory_details_3),
min_date = (select min(transaction_date) from prod_smartnfinal.public.olap_inventory_details_3),
lud=(select current_date),
most_recent_time = (select max(transaction_date)::timestamp from prod_smartnfinal.public.olap_inventory_details_3),
lud_time=(select current_timestamp)
where entity_name='inventory';