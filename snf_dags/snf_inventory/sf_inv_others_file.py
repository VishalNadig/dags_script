import boto3
import pathlib
import os
import re
import pandas as pd
from airflow import DAG
from datetime import date
from datetime import datetime,timedelta
from airflow.utils.dates import days_ago
from airflow.operators.python import PythonOperator
from airflow.operators.bash_operator import BashOperator
from snf_dags.etl_functions import sftp_download
from snf_dags.etl_functions import transforming_data
from snf_dags.etl_functions import upload_file_to_S3
from snf_dags.snowflake_connection import upload_to_snowflake
from snf_dags.etl_functions import removing_raw_process_s3_files
from configparser import ConfigParser, RawConfigParser
from airflow.exceptions import AirflowException
from snf_dags.etl_functions import sf_connect_sting
from snf_dags.etl_functions import sender_msg

####### importing credential info #############################

config = ConfigParser()
res = config.read(r'/home/ubuntu/airflow/dags/snf_dags/credential.cfg')

bucket_name = config.get('S3', 'S3_B_NAME')
bucket_key = config.get('S3', 'S3_SWF_B_KEY')

username = config.get('SNF_SFTP', 'USERNAME')
password = config.get('SNF_SFTP', 'PASSWORD')
sf_wh = config.get('SNOWFLAKE', 'SF_WAREHOUSE')

sf_wh = config.get('SNOWFLAKE', 'SF_WAREHOUSE')
sf_db =  config.get('SNOWFLAKE', 'SF_DATABASE')

session = boto3.session.Session('s3')
credentials = session.get_credentials()
s3_access_id = credentials.access_key
s3_secret_key = credentials.secret_key

####### importing path ########################################

path = '/home/ubuntu/airflow/dags/snf_inventory/'
html_path = '/home/ubuntu/airflow/dags/'

files = ["DC_BOH", "Store_Alloc", "Item_WT_Lookup", "In_Transit_PO","shipper_items","checkstand"]
comp_size_dict = {"DC_BOH":600000, "Store_Alloc":3000000, "Item_WT_Lookup":5000, 
                  "In_Transit_PO":600000, "shipper_items":30000 ,"checkstand" :2500}


####### creating email message ################################

miss_match_msg= dict(Body = 'Please check all files in sftp',
                 Subject = 'Miss-Match Alert:Snowflake- Files size are below the min size ')
sucess_msg= dict(Body = 'All files Size are matched and loaded sucessfully',
                 Subject = 'Snowflake : SnF Inv Others Files loaded Successfully')


####### extra function #########################

def query_df_callable(cursor, query_list):

    final_df = pd.DataFrame()
    for qry in query_list:
        cursor.execute(qry)
        col_names = ','.join([col[0] for col in cursor.description]).lower()
        for c in cursor:
            df = pd.DataFrame(c, columns=[col_names])
            final_df = pd.concat([final_df,df], axis=1)
    return(final_df)


def run_sf_multple_queries(cursor,path_name,file,**kwargs):
    scriptContents = pathlib.Path(os.path.join(path_name,'script_files',file)).read_text(encoding='utf-8')
    sqlStatements = scriptContents.split(sep=';')
    for statement in sqlStatements[:-1]:
        cursor.execute(f'{statement.format(**kwargs)}' + ';')
            


def upload_to_snowflake():

    sf_con = sf_connect_sting()
    sf_curr = sf_con.cursor()
    sf_curr.execute('USE DATABASE %s;' % sf_db)
    sf_curr.execute('USE WAREHOUSE %s;' % sf_wh)
    
    run_sf_multple_queries(sf_curr,path,'snf_inv_other_file_loading.sql',
                           aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
    
def collect_info(dict_file,query,conn):
    idx = 0
    final_df = pd.DataFrame()
    for tbl,col in dict_file.items():
        conn.execute(query.format(col_name =col,table_name = tbl))
        col_names = ','.join([col[0] for col in conn.description]).lower()
        col_list = col_names.split(",")
        df = pd.DataFrame(conn, columns = col_list)
        df.insert(loc=idx, column='table_name', value=tbl)
        final_df = pd.concat([final_df,df], axis=0)
    return(final_df)

snf_inv_other_dict = {'DC_BOH':'filename','Store_Alloc':'filename','Item_WT_Lookup':'filename',
                      'In_Transit_PO':'filename','shipper':'filename', 'checkstand':'filename'}

def success_dag_mail(path_name):
    
    sf_con = sf_connect_sting()
    curr = sf_con.cursor()
    curr.execute('USE DATABASE %s;' % sf_db)
    curr.execute('USE WAREHOUSE %s;' % sf_wh)
    
    try:
        file_query = """select {col_name}, count(*) as count, created_at 
                        from public.{table_name}  
                        group by {col_name},created_at order by {col_name} desc limit 1;"""

        df2 = collect_info(snf_inv_other_dict,file_query,curr)
        df2.to_html(os.path.join(path_name,'html_files','sf_inv_other_file.html'),justify='center', col_space=100,index=False)
        sender_msg(sucess_msg, 'v_arush.s@hypersonix.ai',is_html=True,html_file= 'sf_inv_other_file.html')
    except :
        raise AirflowException(f"HTML Mail ERROR")


######### defining dags ####################################


default_args = {'owner': 'Sudhakar','depends_on_past': False,
               'email': ['v_arush.s@hypersonix.ai', 'navaneeth@hypersonix.ai'],'email_on_failure': True}

dag = DAG(
    dag_id = 'sf_snf_inv_other_files',
    default_args=default_args,
    start_date = datetime(2021,4,21),
    schedule_interval = '00 09 * * *')


downloading_files_task = PythonOperator(
    task_id = 'downloading_files_sftp',
    provide_context=True,
    python_callable =sftp_download,
    retry_delay = timedelta(minutes=30),
    retries =  4,
    op_kwargs={
        'path':path,
        'u_name' : username,
        'p_word' : password,
        'msg1': miss_match_msg ,
        'msg2': sucess_msg,
        'files_name': files ,
        'comp_size_files':comp_size_dict,
        'sftp_dir': '/upload/Inventory/',
        'val_path': 'validation_files',
        'raw_path': 'raw_files',
        'val_file': 'snf_inv_others_val.json',
    },
    dag = dag)

process_files_task = PythonOperator(
    task_id = 'transforming_raw_files',
    python_callable =transforming_data,
    op_kwargs={'path':path,'raw_path':'raw_files','process_path':'process_files',
               'sep': ',','pattern':files},
    dag = dag)

s3_transfer_task = PythonOperator(
    task_id = 'loading_files_into_s3',
    python_callable =upload_file_to_S3,
    op_kwargs={'path': path, 'pattern':files,'process_path':'process_files',
              'bucket_name':bucket_name,'bucket_key':bucket_key},
    dag = dag)

snowflake_load_task = PythonOperator(
    task_id = 'copy_file_into_snowflake',
    python_callable =upload_to_snowflake,
    dag = dag)

remove_source_files_task = PythonOperator(
    task_id = 'deleting_raw_process_s3_files',
    python_callable =removing_raw_process_s3_files,
    op_kwargs={'path': path,'pattern':files,
              'bucket_name':bucket_name,'bucket_key':bucket_key,
              'raw_path':'raw_files','process_path':'process_files'},
    dag = dag)


success_dag_mail_task = PythonOperator(
    task_id = 'success_dag_mail',
    python_callable =success_dag_mail,
    op_kwargs={'path_name': html_path},
    dag = dag)

downloading_files_task >> process_files_task >> s3_transfer_task  >> snowflake_load_task >> remove_source_files_task
remove_source_files_task >>success_dag_mail_task










