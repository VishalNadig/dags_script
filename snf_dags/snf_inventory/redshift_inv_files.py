# import os
# import re
# import time
# import json
# import boto3
# import pathlib
# import requests
# import paramiko
# import psycopg2
# import pandas as pd

# from airflow import DAG
# from datetime import date
# from datetime import datetime,timedelta
# from airflow.utils.dates import days_ago
# from airflow.operators import PythonOperator
# from airflow.operators.bash_operator import BashOperator
# from etl_functions import sftp_download
# from etl_functions import transforming_data
# from etl_functions import upload_file_to_S3
# from snowflake_connection import upload_to_snowflake
# from etl_functions import removing_raw_process_s3_files
# from configparser import ConfigParser, RawConfigParser
# from airflow.operators.sensors import ExternalTaskSensor

# from airflow.exceptions import AirflowException
# from etl_functions import get_redshift_connect_string
# from etl_functions import sender_msg

# ####### importing credential info #############################

# config = RawConfigParser()
# res = config.read('/home/ubuntu/airflow/dags/credential.cfg')

# bucket_name = config.get('S3', 'S3_B_NAME')
# bucket_key = config.get('S3', 'S3_RDS_B_KEY')

# username = config.get('SNF_SFTP', 'USERNAME')
# password = config.get('SNF_SFTP', 'PASSWORD')

# session = boto3.session.Session()
# credentials = session.get_credentials()
# s3_access_id = credentials.access_key
# s3_secret_key = credentials.secret_key

# ####### importing path ########################################

# path = '/home/ubuntu/airflow/dags/snf_inventory/'
# html_path = '/home/ubuntu/airflow/dags/'

# files = ["store_receipts","inv_adjustments","transfer_in_out",
#          "physical_inventory","dc_returns","dc_to_store_orderables","delivery_schedule"]

# # comp_size_dict = {"store_receipts":800000, "inv_adjustment":0,
# #                   "transfer_in_out":0, "physical_inventory":0,"dc_returns":0,
# #                   "dc_to_store_orderables":0, "delivery_schedule":0}
                  
# comp_size_dict = {"store_receipts":3000000, "inv_adjustment":800000,
#                   "transfer_in_out":2000, "physical_inventory":0,"dc_returns":0,
#                   "dc_to_store_orderables":0, "delivery_schedule":40000}


# ####### creating email message ################################

# miss_match_msg= dict(Body = 'Please check all files in sftp',
#                  Subject = 'Miss-Match Alert: Redshift- SnF Inv files size are below the min size ')
# sucess_msg= dict(Body = 'All SnF Inv files Size are matched and loaded sucessfully',
#                  Subject = 'Redshift - SnF Inv files loaded Successfully')


# ####### copy command function #########################

# insert_cmd = """insert into smartnfinal.stg_physical_inventory_full
# select * from (select item_id, store_id, inventory_date, avg(cost) cost, avg(price)price, 
# ROW_NUMBER() over(partition by item_id, store_id order by inventory_date desc ) rn, dateadd(day,+1,current_date)::date as dt
# from smartnfinal.physical_inventory 
# where inventory_date <= dateadd(day,+1,current_date) group by item_id, store_id, inventory_date) a where  rn = 1;"""

# dc_to_store_cmd = """copy smartnfinal.dc_to_store FROM
#                     's3://hypersonixdata/Smart_and_Final/rds_dump/{filename}' 
#                      access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
#                      dateformat 'auto'  maxerror as 0 DELIMITER ',' ignoreheader 1;"""

# delivery_cmd = """copy smartnfinal.delivery_schedule FROM
#                     's3://hypersonixdata/Smart_and_Final/rds_dump/{filename}' 
#                     access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
#                     dateformat 'auto' TIMEFORMAT AS 'DD-MON-YY HH:MI:SS'  maxerror as 0 DELIMITER ',' ignoreheader 1;"""

            
# copy_qry = """copy smartnfinal.{0} FROM
#            's3://hypersonixdata/Smart_and_Final/rds_dump/{1}' 
#             access_key_id '{2}' secret_access_key '{3}' dateformat 'auto'
#             maxerror as 0 DELIMITER '|' NULL as 'NULL';"""

# ####### extra function #########################

# def query_df_callable(cursor, query_list):

#     final_df = pd.DataFrame()
#     for qry in query_list:
#         cursor.execute(qry)
#         col_names = ','.join([col[0] for col in cursor.description]).lower()
#         for c in cursor:
#             df = pd.DataFrame(c, columns=[col_names])
#             final_df = pd.concat([final_df,df], axis=1)
#     return(final_df)


# def run_rds_multple_queries(connection,path_name,file,**kwargs):
#     curr = connection.cursor()
#     scriptContents = pathlib.Path(os.path.join(path_name,'rds_script_files',file)).read_text(encoding='utf-8')
#     sqlStatements = scriptContents.split(sep=';')
#     for statement in sqlStatements[:-1]:
#         curr.execute(f'{statement.format(**kwargs)}' + ';')
#         connection.commit()


# def copy_cmd_execute (query, db_con):
#     try:
#         curr = db_con.cursor()
#         curr.execute(query)
#         db_con.commit()
#     except psycopg2.DatabaseError as error:
#         print(error)

# def loading_inv_files(path,pattern):
#     rds_dev_con = get_redshift_connect_string('dev')
#     run_rds_multple_queries(rds_dev_con,path,'rds_inv_file_loading.sql',
#                            aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
    
    
# def loading_inv_into_db(path,pattern):

#     files = os.listdir(os.path.join(path,'rds_process_files'))
#     match_files = [fn for fn in files if re.match('|'.join(pattern),fn)]
  
#     rds_dev_con = get_redshift_connect_string('dev')
#     rds_ent_con = get_redshift_connect_string('blueprod')
    
#     dev_curr = rds_dev_con.cursor()
#     ent_curr = rds_ent_con.cursor()
#     dev_curr.execute(insert_cmd)
#     rds_dev_con.commit()
#     dev_curr.execute('truncate table smartnfinal.olap_inventory_details_updated;')
#     rds_dev_con.commit()
    
#     for fn in match_files:
#         print(f"Filename ({fn})")

#         if re.match(r'^store_receipts', fn):
#             dt_num = re.sub("[^\d]", "", fn)[0:8]
#             min_date = datetime.strptime(dt_num, '%d%m%Y').date().strftime('%Y-%m-%d')
#             run_rds_multple_queries(rds_dev_con,path,'rds_inv_record_1.sql',filename=fn,
#                                    aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
#             run_rds_multple_queries(rds_dev_con,path,'rds_inv_record_3.sql',mindate=min_date)
#         elif re.match(r'^transfer_in_out', fn):
#             run_rds_multple_queries(rds_dev_con,path,'rds_inv_record_2.sql',filename=fn,
#                                    aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
#             run_rds_multple_queries(rds_dev_con,path,'rds_inv_record_4.sql',filename=fn)
#         elif re.match(r'^inv_adjustments', fn):
#             run_rds_multple_queries(rds_dev_con,path,'rds_inv_record_5_8.sql',filename=fn,
#                                    aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
#         elif re.match(r'^dc_returns', fn):
#             run_rds_multple_queries(rds_dev_con,path,'rds_inv_record_9.sql',filename=fn,
#                                    aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
#         elif re.match(r'^physical_inventory', fn):
#             run_rds_multple_queries(rds_dev_con,path,'rds_inv_record_10.sql',filename=fn,
#                                    aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
#         elif re.match(r'^dc_to_store_orderables', fn):
#             dev_curr.execute(dc_to_store_cmd.format(filename=fn,
#                                                     aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key))
#             rds_dev_con.commit()  
#         elif re.match(r'^delivery_schedule', fn):
#             dev_curr.execute(delivery_cmd.format(filename=fn,
#                                                  aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key))
#             rds_dev_con.commit()
            
#             ent_curr.execute(delivery_cmd.format(filename=fn,
#                                                  aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key))
#             rds_ent_con.commit()
            
            
#     run_rds_multple_queries(rds_dev_con,path,'rds_inv_update_cmd.sql',
#                                    aws_secret_id = s3_access_id, aws_secret_key = s3_secret_key)
    
#     ent_curr.execute(copy_qry.format('olap_inventory_details_3','olap_inventory_details_3_',
#                                     s3_access_id,s3_secret_key))
#     rds_ent_con.commit()
#     rds_ent_con.close()
#     rds_dev_con.close()
    
# def master_data_api():
    
#     data_load_api = 'https://jobs-ent.hypersonix.io/hsapi/gen_automate?company_db=smartnfinal&flag=2'
#     mail_api = 'https://jobs-ent.hypersonix.io/hsapi/gen_automate?company_db=smartnfinal&flag=3'

#     try:
#         data_load_res = requests.get(data_load_api)
#         print(data_load_res)
#         mail_res = requests.get(mail_api)
#         print(mail_res)
#     except requests.exceptions.RequestException as err:
#         raise AirflowException(f"OOps: Something Else {err}")
#     except requests.exceptions.HTTPError as errh:
#         raise AirflowException(f"Http Error: {errh}")
#     except requests.exceptions.ConnectionError as errc:
#         raise AirflowException(f"Error Connecting: {errc}")
#     except requests.exceptions.Timeout as errt:
#         raise AirflowException(f"Timeout Error: {errt}")
#     except JSONDecodeError as e:
#         raise AirflowException(f"JSONDecodeError: {e}")
#         print(a)
#     except SQLAlchemyError as e:
#         error = str(e.__dict__['orig'])
#         print(error)
    
# ############# Success Mail Function ########################

# def collect_info(dict_file,query,conn):
#     df = pd.DataFrame()
#     idx = 0
#     for tbl,col in dict_file.items():
#         df1 = pd.read_sql(query.format(col_name =col ,table_name = tbl), conn)
#         df1.insert(loc=idx, column='table_name', value=tbl)
#         df = pd.concat([df,df1], axis=0).reset_index(drop=True)
#     return(df)

# snf_inv_dict = {'store_receipt':'filename','inventory_adjustment':'filename',
#                 'transfer_in_out':'filename','physical_inventory':'filename', 
#                 'dc_return': 'filename', 'dc_to_store':'filename','delivery_schedule':'filename' }

# snf_inv_olap_dict = {'olap_inventory_details_3':'record_type','olap_inventory_details_updated':'record_type'}

# def success_dag_mail(path_name):
#     rds_dev_con = get_redshift_connect_string('dev')
    
#     try:
#         file_query = """select {col_name}, count(*) as count, created_at 
#                         from smartnfinal.{table_name}  
#                         group by {col_name},created_at order by created_at desc limit 1;"""

#         df1 = collect_info(snf_inv_dict,file_query,rds_dev_con)
#         df1.to_html(os.path.join(path_name,'html_files','rds_inv_file.html'),
#                                     justify='center',col_space=100,index=False)
#         sender_msg(sucess_msg,
#                    'sudhakar.g@hypersonix.io,meenakshi@hypersonix.io,\
#                    navaneeth@hypersonix.ai,amarjeet.s@hypersonix.ai,padmanaban@hypersonix.ai,vishnubala@hypersonix.ai',
#                    is_html=True,html_file= 'rds_inv_file.html')
        
#         olap_query = """select {col_name}, count(*) as count, created_at 
#                         from smartnfinal.{table_name}
#                         where created_at = current_date
#                         group by {col_name},created_at order by {col_name};"""

#         df2 = collect_info(snf_inv_olap_dict,olap_query,rds_dev_con)
#         df2.to_html(os.path.join(path_name,'html_files','rds_inv_olap_file.html'),
#                                     justify='center',col_space=100,index=False)
#         sender_msg(sucess_msg,
#                    'sudhakar.g@hypersonix.io,meenakshi@hypersonix.io,\
#                    navaneeth@hypersonix.ai,amarjeet.s@hypersonix.ai,padmanaban@hypersonix.ai,vishnubala@hypersonix.ai',
#                    is_html=True,html_file= 'rds_inv_olap_file.html')
#     except :
#         raise AirflowException(f"HTML Mail ERROR")


# ######### defining dags ####################################


# default_args = {'owner': 'Sudhakar','depends_on_past': True,
#                'email': ['sudhakar.g@hypersonix.io'],'email_on_failure': True}

# dag = DAG(
#     dag_id = 'redshift_inv_files',
#     default_args=default_args,
#     start_date = datetime(2021,4,28),
#     schedule_interval = '00 12 * * *')

# wait_for_sales_file_loaded_task = ExternalTaskSensor(
#     task_id='wait_for_sales_file_loaded',
#     external_dag_id='redshift_snf_sale',
#     external_task_id='success_dag_mail'
# )


# downloading_files_task = PythonOperator(
#     task_id = 'downloading_files_sftp',
#     provide_context=True,
#     python_callable =sftp_download,
#     retry_delay = timedelta(minutes=15),
#     retries = 4,
#     op_kwargs={
#         'path':path,
#         'u_name' : username,
#         'p_word' : password,
#         'msg1': miss_match_msg ,
#         'msg2': sucess_msg,
#         'files_name': files ,
#         'comp_size_files':comp_size_dict,
#         'sftp_dir': '/upload/Inventory/',
#         'val_path': 'rds_validation_files',
#         'raw_path': 'rds_raw_files',
#         'val_file': 'rds_inv_files_val.json',
#     },
#     dag = dag)

# process_files_task = PythonOperator(
#     task_id = 'transforming_raw_files',
#     python_callable =transforming_data,
#     op_kwargs={'path':path,'sep': ',','pattern':files,
#                'raw_path':'rds_raw_files','process_path':'rds_process_files'},
#     dag = dag)

# s3_transfer_task = PythonOperator(
#     task_id = 'loading_files_into_s3',
#     python_callable =upload_file_to_S3,
#     op_kwargs={'path': path, 'pattern':files+ ['inv_adjustment'],
#                'process_path':'rds_process_files',
#                'bucket_name':bucket_name,'bucket_key':bucket_key},
#     dag = dag)


# redshift_load_inv_file_db_task = PythonOperator(
#     task_id = 'copy_and_load_inv_file_into_db',
#     python_callable =loading_inv_into_db,
#     op_kwargs={'path':path, 'pattern':files},
#     dag = dag)

# remove_source_files_task = PythonOperator(
#     task_id = 'deleting_raw_process_s3_files',
#     python_callable =removing_raw_process_s3_files,
#     op_kwargs={'path': path,'pattern':files + ['olap_inventory_details_3'],
#               'bucket_name':bucket_name,'bucket_key':bucket_key,
#               'raw_path':'rds_raw_files','process_path':'rds_process_files'},
#     dag = dag)


# success_api_task = PythonOperator(
#     task_id = 'success_master_data_api',
#     python_callable =master_data_api,
#     dag = dag)

# success_dag_mail_task = PythonOperator(
#     task_id = 'success_dag_mail',
#     python_callable =success_dag_mail,
#     op_kwargs={'path_name': html_path},
#     dag = dag)

# wait_for_sales_file_loaded_task >> downloading_files_task >> process_files_task >> s3_transfer_task
# s3_transfer_task >> redshift_load_inv_file_db_task
# redshift_load_inv_file_db_task >> remove_source_files_task >> success_api_task >> success_dag_mail_task












