
update smartnfinal.olap_inventory_details_updated set is_avail_in_phy=1
from smartnfinal.olap_inventory_details_updated o 
join(
select distinct store_id,item_id 
from smartnfinal.physical_inventory
)t on o.store_number=t.store_id and o.item_number=t.item_id
where o.created_at= current_date;

UPDATE smartnfinal.olap_inventory_details_updated set is_checkstand = 'Y' 
where item_number in (select DISTINCT dst_item from smartnfinal.checkstand_shipper_multiplier where data_src = 'checkstand')
and created_at=current_date;


UPDATE smartnfinal.olap_inventory_details_updated set is_shipper = 'Y' 
where item_number in (select DISTINCT dst_item from smartnfinal.checkstand_shipper_multiplier where data_src = 'shipper')
and created_at=current_date;


update smartnfinal.olap_inventory_details_updated set
perpetual_quantity = v.perpetual_quantity ,
perpetual_quantity_adjustment = v.perpetual_quantity_adjustment,
physical_inventory_start_date = v.physical_inventory_start_date,
item_value = v.item_value,
physical_inventory_price=v.physical_inventory_price,
perpetual_item_value=v.perpetual_item_value,
perpetual_adjustment_item_value=v.perpetual_adjustment_item_value,
end_of_day_perpetual_inventory_flag=v.end_of_day_perpetual_inventory_flag
from smartnfinal.vw_perpetual_inventory_details_updated v
where v.unique_id = smartnfinal.olap_inventory_details_updated.unique_id
and smartnfinal.olap_inventory_details_updated.created_at=current_date ;



insert into smartnfinal.olap_inventory_details_3 (unique_id, transaction_date, date_check_id, source, source_type, site_id, year, quarter, month_no, week_no, day_of_week, day_name, month_name, month_short, month_year, week_str, fis_p_str, fis_w_str, fis_week, fis_period, fis_quarter, fis_year, named_day, old_store_number, store_number, brand_name, region, region_code, region_name, district_code, district, district_name, store_name, store_type, priorstore_number, pricing_zone, storeopen_date, latitude, longitude, vintage, storeclose_date, address, city, state, state_name, zip, category, sub_category, upc_number, item_number, itemcreate_date, item_description, level1, level2, level3, include_in_sales, level4, level5, level6, level7, level8, level9, level10, level11, level12, sinage_description, item_size, familycode, item_brand, plbornon_date, clubsize, business_item, private_label, seasonflag, scaleableflag, itemstatus, gl_account, batch, ap_vendor, document, quantity, cost, retail, description, dsd_seq, order_type, account_department, pos_department, created_at, transaction_time, record_type, record_type_desc, filename, price, total_cost, total_price, is_avail_in_phy, scaleindicator, item_discount_sales, item_net_sales, item_cost, net_sales, business_date, lane_number, check_number, transaction_type_code, item_discount_amount, physical_inventory_start_date, perpetual_quantity, perpetual_quantity_adjustment, item_value, replinishment_days, physical_inventory_price, perpetual_item_value, perpetual_adjustment_item_value, end_of_day_perpetual_inventory_flag, prod_attr_1, prod_attr_2, prod_attr_3, prod_attr_4, prod_attr_5, prod_attr_6, prod_attr_7, prod_attr_8, prod_attr_9, prod_attr_10, prod_attr_11, prod_attr_12, prod_attr_13, prod_attr_14, prod_attr_15, pricing_zone_name, prod_attr_16, prod_attr_17,is_checkstand,is_shipper,reg_price,reg_price2,is_warehouse)
select unique_id,o.transaction_date,o.date_check_id,o.source,o.source_type,o.site_id,o.year,o.quarter,o.month_no,o.week_no,o.day_of_week,o.day_name,o.month_name,o.month_short,o.month_year,o.week_str,o.fis_p_str,o.fis_w_str,o.fis_week,o.fis_period,o.fis_quarter,o.fis_year,o.named_day,o.old_store_number,o.store_number,o.brand_name,o.region,o.region_code,o.region_name,o.district_code,o.district,o.district_name,o.store_name,o.store_type,o.priorstore_number,o.pricing_zone,o.storeopen_date,o.latitude,o.longitude,o.vintage,o.storeclose_date,o.address,o.city,o.state,o.state_name,o.zip,o.category,o.sub_category,o.upc_number,o.item_number,o.itemcreate_date,o.item_description,o.level1,o.level2,o.level3,o.include_in_sales,o.level4,o.level5,o.level6,o.level7,o.level8,o.level9,o.level10,o.level11,o.level12,o.sinage_description,o.item_size,o.familycode,o.item_brand,o.plbornon_date,o.clubsize,o.business_item,o.private_label,o.seasonflag,o.scaleableflag,o.itemstatus,o.gl_account,o.batch,o.ap_vendor,o.document,o.quantity,o.cost,o.retail,o.description,o.dsd_seq,o.order_type,o.account_department,o.pos_department,o.created_at,o.transaction_time,o.record_type,o.record_type_desc,o.filename,o.price,o.total_cost,o.total_price,o.is_avail_in_phy,o.scaleindicator,o.item_discount_sales,o.item_net_sales,o.item_cost,o.net_sales,o.business_date,o.lane_number,o.check_number,o.transaction_type_code,o.item_discount_amount,o.physical_inventory_start_date,o.perpetual_quantity,o.perpetual_quantity_adjustment,o.item_value,COALESCE(v1.replenishment_days,2),o.physical_inventory_price,o.perpetual_item_value,o.perpetual_adjustment_item_value,o.end_of_day_perpetual_inventory_flag,o.prod_attr_1,o.prod_attr_2,o.prod_attr_3,o.prod_attr_4,o.prod_attr_5,o.prod_attr_6,o.prod_attr_7,o.prod_attr_8,o.prod_attr_9,o.prod_attr_10,o.prod_attr_11,o.prod_attr_12,o.prod_attr_13,o.prod_attr_14,o.prod_attr_15,o.pricing_zone_name,COALESCE(v.val,'N')asprod_attr_16,COALESCE(t.val,'N')as prod_attr_17,o.is_checkstand,o.is_shipper,p.regular_price AS reg_price,COALESCE(p.regular_price,o.price)as reg_price2,COALESCE(d.dc, 'DSD')
from smartnfinal.olap_inventory_details_updated o
left join (select distinct dst_item, 'DC' dc from smartnfinal.dc_to_store d 
        join smartnfinal.checkstand_shipper_multiplier c on d.item = c.src_item) d on d.dst_item = o.item_number
left join smartnfinal.vw_inventory_replenishment_days v1 on o.store_number=v1.store_id and o.item_number=v1.item
left join smartnfinal.kvi_traffic t on t.traffic = o.item_description
left join smartnfinal.kvi_values v on v.value = o.item_description
LEFT join smartnfinal.price_new p on o.transaction_date=p.day_date and o.store_number=p.store_number and o.item_number=p.item_number
where  o.created_at = CURRENT_DATE ;

update smartnfinal.olap_inventory_details_3 set
whs_id1=t.whs_id1,
whs_id2=t.whs_id2,
whcount=t.whcount,
delivery_schedule=t.delivery_schedule,
inc_qty=t.inc_qty
from smartnfinal.vw_dc_to_store_with_delivery_sch t
join smartnfinal.olap_inventory_details_3 o on t.store_number=o.store_number and t.item_number=o.item_number
where o.created_at = current_date;

update smartnfinal.entity_log
set row_count =(select count(*) from smartnfinal.olap_inventory_details_3),
most_recent_date=(select max(transaction_date) from smartnfinal.olap_inventory_details_3),
min_date = (select min(transaction_date) from smartnfinal.olap_inventory_details_3),
lud=(select current_date),
most_recent_time = (select max(transaction_date)::timestamp from smartnfinal.olap_inventory_details_3),
lud_time=(select current_timestamp)
where entity_name='inventory';

unload ('select * from smartnfinal.olap_inventory_details_3 where created_at = current_date')
to 's3://hypersonixdata/Smart_and_Final/rds_dump/olap_inventory_details_3_'
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
parallel on delimiter '|' NULL as 'NULL';

