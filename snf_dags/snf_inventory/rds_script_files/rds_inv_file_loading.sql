
copy smartnfinal.dc_to_store FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/dc_to_store_orderables' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
dateformat 'auto'  maxerror as 0 DELIMITER ',' ignoreheader 1;


copy smartnfinal.transfer_in_out FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/transfer_in_out' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
dateformat 'auto' TIMEFORMAT AS 'DD-MON-YY HH.MI.SS' maxerror as 0 DELIMITER ',' ignoreheader 1;


copy smartnfinal.dc_return FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/dc_returns' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
dateformat 'auto'  maxerror as 0 DELIMITER ',' ignoreheader 1;


copy smartnfinal.inventory_adjustment FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/inv_adjustments' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
dateformat 'auto' TIMEFORMAT AS 'DD-MON-YY HH.MI.SS' maxerror as 0 DELIMITER ',' ignoreheader 1;


copy smartnfinal.store_receipt FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/store_receipts' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
dateformat 'auto' maxerror as 0 DELIMITER ',' ignoreheader 1;


copy smartnfinal.delivery_schedule FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/delivery_schedule' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
dateformat 'auto' TIMEFORMAT AS 'DD-MON-YY HH:MI:SS'  maxerror as 0 DELIMITER ',' ignoreheader 1;


copy smartnfinal.physical_inventory FROM
's3://hypersonixdata/Smart_and_Final/rds_dump/physical_inventory' 
access_key_id '{aws_secret_id}' secret_access_key '{aws_secret_key}'
dateformat 'auto' maxerror as 0 DELIMITER ',' ignoreheader 1;