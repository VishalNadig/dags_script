import boto3
import os
import re
import snowflake.connector
from configparser import ConfigParser,RawConfigParser


# base_path = os.path.dirname(os.path.abspath(__file__))
# initfile = os.path.join(base_path, 'credential.cfg')

config = RawConfigParser()
res = config.read('/home/ubuntu/airflow/dags/snf_dags/credential.ini')
sf_user_name = config.get('SNOWFLAKE', 'SF_USER_NAME')
sf_pass = config.get('SNOWFLAKE', 'SF_PASS')
sf_acct = config.get('SNOWFLAKE', 'SF_ACCOUNT')
sf_wh = config.get('SNOWFLAKE', 'SF_WAREHOUSE')

sf_database = 'SMARTNFINAL_DEV'

session = boto3.session.Session('s3')
credentials = session.get_credentials()
s3_access_key = credentials.access_key
s3_secret_key = credentials.secret_key


copy_qry = """COPY INTO PUBLIC.{0}
              FROM s3://hypersonixdata/Smart_and_Final/CSV/{1} 
              credentials=(AWS_KEY_ID='{2}' AWS_SECRET_KEY='{3}')
              FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = ',' SKIP_HEADER = 1)
              on_error = 'continue';"""


def upload_to_snowflake(path):
    sf_con = snowflake.connector.connect(user = sf_user_name, password = sf_pass, account = sf_acct)
    file_dir = os.listdir(os.path.join(path,'process_files'))
  
    sf_curr = sf_con.cursor()
    sf_curr.execute('USE DATABASE %s;' % sf_database)
    sf_curr.execute('USE WAREHOUSE %s;' % sf_wh)
    try:
        for fn in file_dir:
            print(f"Loading ({fn}) into SnowFlake")

            if re.match(r'^DC_BOH', fn):
                sf_curr.execute(copy_qry.format('dc_boh',fn,s3_access_key,s3_secret_key))
            elif re.match(r'^Item_WT_Lookup', fn):
                sf_curr.execute(copy_qry.format('item_wt_lookup',fn,s3_access_key,s3_secret_key))
            elif re.match(r'^Store_Alloc', fn):
                sf_curr.execute(copy_qry.format('store_alloc',fn,s3_access_key,s3_secret_key))
            elif re.match(r'^In_Transit_PO', fn):
                sf_curr.execute(copy_qry.format('in_transit_po',fn,s3_access_key,s3_secret_key))
    except Exception as error:
        print(error)

    sf_curr.close()
    
if __name__ == "__main__":
    upload_to_snowflake()
                                
                                
    
    
    
    
    
    
    
    
    
