import os.path
import os
import paramiko
import pandas as pd
import boto3
from datetime import date
import snowflake
import snowflake.connector
from snowflake.connector.pandas_tools import write_pandas

from datetime import datetime,date,timedelta
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python import PythonOperator,BranchPythonOperator
from airflow.operators.bash_operator import BashOperator
from configparser import ConfigParser, RawConfigParser
from snf_dags.etl_functions import sender_msg

#=========== Defining paths and variables ======================================================

path = "/home/ubuntu/airflow/dags/snf_dags/snf_prices/"
raw_path = "raw_files"
processed_path="processed_files"
html_path = '/home/ubuntu/airflow/dags/snf_dags/'


####### importing credential info #############################

config = RawConfigParser()
res = config.read('/home/ubuntu/airflow/dags/snf_dags/credential.ini')

username = config.get('SNF_SFTP', 'USERNAME')
password = config.get('SNF_SFTP', 'PASSWORD')
host = 'ftp.hypersonix.io'
port = 22

session = boto3.session.Session('s3')
credentials = session.get_credentials()
aws_access_key_id = credentials.access_key
aws_secret_access_key = credentials.secret_key
region_name = 'us-west-2'
Bucket='hypersonixdata'
s3_key='Smart_and_Final/price/'


snf_user = 'v_arush.s@hypersonix.ai'
snf_pass = 'Remote@123'
snf_account='oma07528'
snf_warehouse='DEV'
snf_db='DEV_SMARTNFINAL'
snf_prod_db='prod_smartnfinal'
snf_schema='PUBLIC'
stag_table='PRICE_NEW_STG'
main_table='PRICE_NEW'



#========== Custome Msg =========================================================

sucess_msg= dict(Body = ' SNF Prices loaded sucessfully',
                 Subject = 'Snowflake: SNF prices data loaded Successfully')

#========= Required function to run Dag ==========================================

def sftp_file_download(host,port,username, password,path,raw_path,processed_path):

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(host, port, username, password, banner_timeout=500)
    print("connected")
    sftp_client = client.open_sftp()
    sftp_client.chdir('/upload/')
    latest = 0
    global latestfile
    for fileattr in sftp_client.listdir_attr():
        if fileattr.filename.startswith('Price') and fileattr.filename.endswith(".csv.gz") and fileattr.st_mtime > latest:
            latest = fileattr.st_mtime
            latestfile = fileattr.filename

    print("Latest file: ", latestfile)
    sftp_client.get(latestfile,os.path.join(path,raw_path,latestfile))
    print("file downloaded")

    sftp_client.close()
    client.close()
    print("connection closed")

    df = pd.read_csv(os.path.join(path,raw_path,latestfile), sep='|', compression='gzip',encoding='latin-1', na_values=' ', dtype=str)
    print("data loaded to dataframe")
    df['filename'] = latestfile
    df['created_at'] = date.today().strftime('%Y-%m-%d')
    print("********************************************************")
    print("Transformation successful: ")
    print(df.head)
    print("********************************************************")
    df.to_csv(os.path.join(path,processed_path,'price_data.csv'), index=False, sep='|')
    print("processed file created")

    os.remove(os.path.join(path,raw_path,latestfile))
    print("Raw file deleted")


def upload_to_s3(aws_access_key_id,aws_secret_access_key,region_name,path,processed_path,Bucket,s3_key):

    session = boto3.session.Session(aws_access_key_id=aws_access_key_id,
                                    aws_secret_access_key=aws_secret_access_key,
                                    region_name=region_name)
    s3client = session.client('s3')
    s3client.upload_file(Filename=os.path.join(path,processed_path,'price_data.csv'),
                         Bucket=Bucket, Key=os.path.join(s3_key,'price_data.csv'))
    print("file uploaded to s3")


def snowflake_ingestion(snf_user,snf_pass,snf_account,snf_warehouse,stag_table,main_table):

    conn = snowflake.connector.connect(user=snf_user,password=snf_pass,account=snf_account,
    warehouse=snf_warehouse)      
    print("connected to snowflake")

    truncate_command=''' truncate table DEV_SMARTNFINAL.public.{};'''.format(stag_table)
    conn.cursor().execute(truncate_command)
    print("staging table truncated")

    copy_command=''' COPY INTO DEV_SMARTNFINAL.public.{}
    FROM s3://hypersonixdata/Smart_and_Final/price/price_data.csv
    credentials=(AWS_KEY_ID='AKIAVWISPWTCNAHF73VL', AWS_SECRET_KEY='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')
    FILE_FORMAT = (TYPE = CSV FIELD_DELIMITER = '|' SKIP_HEADER = 1 DATE_FORMAT = 'YYYY/MM/DD')
    FORCE = TRUE; '''.format(stag_table)
    conn.cursor().execute(copy_command)
    print("data copied to staging")

    insert_command=''' insert into DEV_SMARTNFINAL."PUBLIC".{}(DAY_DATE_src, STORE_NUMBER ,ITEM_NUMBER ,REGULAR_PRICE ,PROMO_PRICE ,DISCOUNT_AMOUNT ,PROMO_STARTDATE,PROMO_ENDDATE,BMSM_MULLTIPLE ,BMSM_PRICE ,BMSM_STARTDATE,BMSM_ENDDATE,FILENAME ,CREATED_AT,DAY_DATE)
    select DAY_DATE AS day_date_src,STORE_NUMBER ,ITEM_NUMBER ,REGULAR_PRICE ,PROMO_PRICE ,DISCOUNT_AMOUNT ,PROMO_STARTDATE,PROMO_ENDDATE,BMSM_MULLTIPLE ,BMSM_PRICE ,BMSM_STARTDATE,BMSM_ENDDATE,FILENAME ,to_date(CREATED_AT, 'yyyy-mm-dd'),DAY_DATE    
    from DEV_SMARTNFINAL."PUBLIC".{} ;  '''.format(main_table,stag_table)
    conn.cursor().execute(insert_command)
    print("data inserted to main table")

    # prod_command=''' CREATE OR REPLACE TABLE prod_smartnfinal.PUBLIC.{} CLONE dev_smartnfinal.public.{};'''.format(main_table,main_table)
    # conn.cursor().execute(prod_command)
    # print("data inserted to production table")


def removing_raw_process_s3_files(aws_access_key_id,aws_secret_access_key,region_name,Bucket, s3_key):

    os.remove(os.path.join(path,processed_path,'price_data.csv'))
    print("Process file deleted")
    session = boto3.session.Session(aws_access_key_id=aws_access_key_id,
                                    aws_secret_access_key=aws_secret_access_key,
                                    region_name=region_name)
    s3client = session.client('s3')
    s3client.delete_object(Bucket=Bucket, Key=os.path.join(s3_key,'price_data.csv'))
    print("S3 file deleted")

def success_dag_mail(path_name):

    conn = snowflake.connector.connect(user=snf_user,password=snf_pass,account=snf_account,warehouse=snf_warehouse,database=snf_db,schema=snf_schema)      
    
    qry = """ SELECT DISTINCT day_date,day_date_src,filename,created_at FROM  prod_smartnfinal.public.PRICE_NEW order by 1 desc limit 5; """
    df = pd.read_sql(qry,conn)

    df.to_html(os.path.join(path_name,'html_files','sf_snf_prices.html'),justify='center',col_space=100,index=False)
    sender_msg(sucess_msg, 'v_arush.s@hypersonix.ai,meenakshi@hypersonix.ai,archit.s@hypersonix.ai,vaibhav.g@hypersonix.ai',is_html=True,html_file= 'sf_snf_prices.html')
    print("email sent")
    

#========= creating Dags ==========================================

default_args = {'owner': 'op','depends_on_past': False}

dag = DAG(
    dag_id = 'snf_prices',
    default_args=default_args,
    start_date = datetime(2021,11,28),
    schedule_interval = '30 09 * * *')

snf_task = DummyOperator(task_id='run_snf_prices_pipeline', dag=dag)

sftp_file_download_task = PythonOperator(
    task_id = 'sftp_file_download',
    python_callable =sftp_file_download,
    op_kwargs={'host':host,'port':port,'username':username, 'password':password,'path':path,'raw_path':raw_path
    ,'processed_path':processed_path},
    dag = dag)

upload_to_s3_task = PythonOperator(
    task_id = 'upload_to_s3',
    python_callable =upload_to_s3,
    op_kwargs={'aws_access_key_id':aws_access_key_id,'aws_secret_access_key':aws_secret_access_key,'region_name':region_name,'path':path,'processed_path':processed_path,'Bucket':Bucket,'s3_key':s3_key},
    dag = dag)

snowflake_ingestion_task = PythonOperator(
    task_id = 'snowflake_ingestion',
    python_callable =snowflake_ingestion,
    op_kwargs={'snf_user':snf_user,'snf_pass':snf_pass,'snf_account':snf_account,'snf_warehouse':snf_warehouse,'stag_table':stag_table,'main_table':main_table},
    dag = dag)

removing_raw_process_s3_files_task = PythonOperator(
    task_id = 'removing_raw_process_s3_files',
    python_callable =removing_raw_process_s3_files,
    op_kwargs={'aws_access_key_id':aws_access_key_id,'aws_secret_access_key':aws_secret_access_key,'region_name':region_name,'Bucket':Bucket,'s3_key':s3_key},
    dag = dag)

success_dag_mail_task = PythonOperator(
    task_id = 'success_dag_mail',
    python_callable =success_dag_mail,
    op_kwargs={'path_name': html_path},
    dag = dag)

snf_task >> sftp_file_download_task >> upload_to_s3_task >> snowflake_ingestion_task >> removing_raw_process_s3_files_task >> success_dag_mail_task

