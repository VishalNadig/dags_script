
# loading modules 
import os
import re
import time
import json
import boto3
import base64
import pathlib
import paramiko
import functools
import psycopg2
import pandas
import numpy as np
import pandas as pd
import snowflake.connector
from datetime import date
from datetime import datetime
import pandas.io.common

import pymysql
import pymongo

from sqlalchemy import create_engine


import mimetypes
import smtplib, ssl
from email import encoders
from email.header import Header
from email.message import Message
from smtplib import SMTPException
from email.utils import formataddr
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.audio import MIMEAudio
from email.mime.image import MIMEImage
from botocore.exceptions import ClientError
from email.mime.multipart import MIMEMultipart
from airflow.exceptions import AirflowSkipException
from airflow.exceptions import AirflowException
from configparser import ConfigParser, RawConfigParser


# loading credential data
base_path = os.path.dirname(os.path.abspath(__file__))
initfile = os.path.join(base_path, 'credential.cfg')

config = RawConfigParser()
res = config.read('/home/ubuntu/airflow/dags/snf_dags/credential.cfg')

mysql_user = config.get('DEV_MYSQL', 'USERNAME')
mysql_pass = config.get('DEV_MYSQL', 'PASSWORD')

mysql_dev_host = config.get('DEV_MYSQL', 'HOST')
mysql_stg_host = config.get('STAGE_MYSQL', 'HOST')
mysql_green_host = config.get('GREEN_MYSQL', 'HOST')
mysql_blue_host = config.get('BLUE_MYSQL', 'HOST')

sender_host = config.get('EMAIL', 'EMAIL_HOST')
sender_email = config.get('EMAIL', 'EMAIL_SENDER')
sender_username = config.get('EMAIL', 'EMAIL_USERNAME')
sender_password = config.get('EMAIL', 'EMAIL_PASSWORD')

sf_user_name = config.get('SNOWFLAKE', 'SF_USER_NAME')
sf_pass = config.get('SNOWFLAKE', 'SF_PASS')
sf_acct = config.get('SNOWFLAKE', 'SF_ACCOUNT')
sf_wh = config.get('SNOWFLAKE', 'SF_WAREHOUSE')
sf_db = config.get('SNOWFLAKE', 'SF_DATABASE')


prod_sf_user_name = config.get('PROD_SNOWFLAKE', 'SF_USER_NAME')
prod_sf_pass = config.get('PROD_SNOWFLAKE', 'SF_PASS')
prod_sf_acct = config.get('PROD_SNOWFLAKE', 'SF_ACCOUNT')
prod_sf_wh = config.get('PROD_SNOWFLAKE', 'SF_WAREHOUSE')

mango_host = config.get('MOZART_MONGO', 'MOZART_MONGO_HOST')
mango_username= config.get('MOZART_MONGO', 'MOZART_MONGO_USERNAME')
mango_password = config.get('MOZART_MONGO', 'MOZART_MONGO_PASSWORD')



#session = boto3.Session(aws_access_key_id=s3_access_key_id,aws_secret_access_key=s3_secret_access_key)
ss3 = boto3.resource('s3')

session = boto3.session.Session()
s3client = session.client('s3')

def sender_msg(msg_dict, recipients, is_html=None, html_file=None): 
 
    if is_html is None:
        try:
            body = """ {0} """.format(msg_dict.get('Body'))
            msg = MIMEMultipart()

            msg['Subject'] = msg_dict.get('Subject')
            msg['From'] =  sender_email
            msg['To'] = (', ').join(recipients.split(','))

            msg.attach(MIMEText(body,'plain'))

            server = smtplib.SMTP(sender_host, 587,timeout = 10)
            server.starttls()
            server.ehlo()
            server.login(sender_username,sender_password)
            server.send_message(msg)
            server.quit()
        except smtplib.SMTPException as err:
            print (f'Error: unable to send email {err}')      
    elif is_html is not None:
        try:
            path = '/home/ubuntu/airflow/dags/'
            html = open(os.path.join(path,'html_files',html_file))
            msg = MIMEText(html.read(), 'html')

            msg['Subject'] = msg_dict.get('Subject')
            msg['From'] = sender_email
            msg['To'] = (', ').join(recipients.split(','))

            server = smtplib.SMTP(sender_host, 587,timeout = 10)
            server.starttls()
            server.ehlo()
            server.login(sender_username,sender_password)
            server.send_message(msg)
            server.quit()
        except smtplib.SMTPException as err:
            print (f'Error: unable to send email {err}')

def sender_attachment(fileToSend,msg_dict, recipients): 
 
    try:
        body = """ {0} """.format(msg_dict.get('Body'))
        msg = MIMEMultipart()

        msg['Subject'] = msg_dict.get('Subject')
        msg['From'] = formataddr((str(Header(msg_dict.get('From'), 'utf-8')), sender_email)) 
        msg['To'] = (', ').join(recipients.split(','))

        msg.attach(MIMEText(body,'plain'))
        
        ctype, encoding = mimetypes.guess_type(fileToSend)
        if ctype is None or encoding is not None:
            ctype = "application/octet-stream"

        maintype, subtype = ctype.split("/", 1)

        if maintype == "text":
            fp = open(fileToSend)
            # Note: we should handle calculating the charset
            attachment = MIMEText(fp.read(), _subtype=subtype)
            fp.close()
        elif maintype == "image":
            fp = open(fileToSend, "rb")
            attachment = MIMEImage(fp.read(), _subtype=subtype)
            fp.close()
        elif maintype == "audio":
            fp = open(fileToSend, "rb")
            attachment = MIMEAudio(fp.read(), _subtype=subtype)
            fp.close()
        else:
            fp = open(fileToSend, "rb")
            attachment = MIMEBase(maintype, subtype)
            attachment.set_payload(fp.read())
            fp.close()
            encoders.encode_base64(attachment)
        attachment.add_header("Content-Disposition", "attachment", filename=fileToSend)
        msg.attach(attachment)

        server = smtplib.SMTP(sender_host, 587,timeout = 10)
        server.starttls()
        server.ehlo()
        server.login(sender_username,sender_password)
        server.send_message(msg)
        server.quit()
    except SMTPException:
        print ("Error: unable to send email")


def upload_file_to_S3(path,pattern,bucket_name,bucket_key,process_path):
    files = os.listdir(os.path.join(path ,process_path))
    match_files = [fn for fn in files if re.match('|'.join(pattern),fn)]
    for fn in match_files:
        print("putting %s in s3 " % (fn))
        s3client.upload_file(Filename=os.path.join(path,process_path,fn),
                                   Bucket=bucket_name, Key=os.path.join(bucket_key,fn)) 
        
def upload_file_to_S3_new(path,regex_file, bucket_name,bucket_key,process_path, pattern=True):
    files = os.listdir(os.path.join(path ,process_path))
    if pattern:
        match_files = [fn for fn in files if re.match('|'.join(regex_file),fn)]
    else: match_files = files
        
    for fn in match_files:
        if os.path.isfile(os.path.join(path,process_path, fn)):
            print("putting %s in s3 " % (fn))
            s3client.upload_file(Filename=os.path.join(path,process_path,fn),
                                       Bucket=bucket_name, Key=os.path.join(bucket_key,fn))
        else: pass
        
def remove_files_s3(file_name):
    try:
        print("deleting %s from s3 " % (file_name))
        s3client.Object(bucket_name, os.path.join(bucket_key,file_name)).delete()   
    except Exception as e:
        print (str(e))
        print ("error")          
        
def copy_cmd_execute (query, db_con):
    try:
        curr = db_con.cursor()
        curr.execute(query)
        db_con.commit()
        #curr.close()
        print("Successfully loaded data into Dev")
    except psycopg2.DatabaseError as error:
        print(error)
        
def delete_csv_files(csv_path,process_path,filename):
    print("Deleting files from s3 and local files")
    for fn in filename:
        delete_s3(fn)
        os.remove(os.path.join(csv_path,fn))
        os.remove(os.path.join(process_path,fn))
        

def load_json_file(p_name,file_path,fname):
    with open(os.path.join(p_name,file_path,fname), 'r') as f:
        d = json.load(f)
    return d

    
class AllowAnythingPolicy(paramiko.MissingHostKeyPolicy):
    def missing_host_key(self, client, hostname, key):
        return
    
def hasNumbers(inputString):
    return bool(re.search(r'\d', inputString))

def sftp_download(path,u_name,p_word,msg1,msg2,comp_size_files,
                  files_name,sftp_dir,raw_path,val_path,val_file,**context):
    execution_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    
    df = load_json_file(path,val_path,val_file)
    
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(AllowAnythingPolicy())
    client.connect('ftp.hypersonix.io', username= u_name, password=p_word,banner_timeout=200)
    sftp = client.open_sftp()
    
    sftp.chdir(sftp_dir)
    
    new_files_list = []
    f_dict = {}
    #print(files_name)
    for fname in files_name:
        for fileattr in sftp.listdir_attr():
            if fileattr.filename.startswith(fname) and not fileattr.filename.endswith(".trg"):
                latest = fileattr.st_mtime
                latestfile = fileattr.filename
                latestsize = fileattr.st_size
                f_dict[latestfile] =latestsize
        files_list = [k for k in f_dict.keys() if k.startswith(fname)]
                
        new_list = list(set(files_list) - set(df[fname]))
        #print(new_list)
        new_files_list.extend(new_list)
        df[fname].extend(new_list)

    file_size_dict = dict([[key,val] for key, val in f_dict.items() if key in new_files_list])
    print(file_size_dict.keys())
    if len(file_size_dict.keys()) < len(files_name):
        response = "No_Files_Available"
        context['ti'].xcom_push(key='response', value = response)
        raise AirflowException(f"No New File Available in this Execution Time ({execution_time}).")
    else:
        nof= len(file_size_dict.keys())
        print(f"We have received {nof} files now checking file size ....")
        chk_size = []
        for k in file_size_dict:
            if hasNumbers(k):
                if k[:re.search(r"\d",k).start() -1] in comp_size_files:
                    if file_size_dict.get(k) >= comp_size_files.get(k[:re.search(r"\d",k).start() -1]):
                        chk_size.append(k)
            else:
                if any(k.startswith(key) for key in comp_size_files):
                    match_k  = ''.join([key for key in comp_size_files if k.startswith(key)])
                    if file_size_dict.get(k) >= comp_size_files.get(match_k):
                        chk_size.append(k)
       
        if len(chk_size) < len(files_name):
            print(chk_size)
            sender_msg(msg1,"sudhakar.g@hypersonix.ai,v_arush.s@hypersonix.ai, navaneeth@hypersonix.ai")
            raise AirflowSkipException(f"MissMatch files size in this Execution Time ({execution_time}).")
        else:
            response = "Files_Available"
            context['ti'].xcom_push(key='response', value = response)
            for fn in chk_size:
                print(f"downloading {fn} file ")
                sftp.get(fn,os.path.join(path,raw_path,fn))
            #sender_msg(msg2,"sudhakar.g@hypersonix.io")
            with open(os.path.join(path,val_path,val_file), 'w') as f:
                json.dump(df, f)
                
def sftp_download_new(path,u_name,p_word,msg1,msg2,comp_size_files,
                  files_name,sftp_dir,raw_path,val_path,val_file,**context):
    execution_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    
    df = load_json_file(path,val_path,val_file)
    
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(AllowAnythingPolicy())
    client.connect('ftp.hypersonix.io', username= u_name, password=p_word,banner_timeout=200)
    sftp = client.open_sftp()
    
    sftp.chdir(sftp_dir)
    
    new_files_list = []
    f_dict = {}
    #print(files_name)
    for fname in files_name:
        for fileattr in sftp.listdir_attr():
            if fileattr.filename.startswith(fname) and not fileattr.filename.endswith(".trg"):
                latest = fileattr.st_mtime
                latestfile = fileattr.filename
                latestsize = fileattr.st_size
                f_dict[latestfile] =latestsize
        files_list = [k for k in f_dict.keys() if k.startswith(fname)]
                
        new_list = list(set(files_list) - set(df[fname]))

        new_files_list.extend(new_list)
        df[fname].extend(new_list)

    file_size_dict = dict([[key,val] for key, val in f_dict.items() if key in new_files_list])
    print(file_size_dict.keys())
    
    if len(file_size_dict.keys()) ==0:
        response = "No_Files_Available"
        context['ti'].xcom_push(key='response', value = response)
        raise AirflowException(f"No New File Available File in this Execution Time ({execution_time}).")
    elif len(file_size_dict.keys()) < len(files_name):
        response = "No_Files_Available"
        context['ti'].xcom_push(key='response', value = response)
        raise AirflowException(f"We received only 1 file out of 2 Ent File in this Execution Time ({execution_time}).")
        
    elif len(file_size_dict.keys()) > len(files_name):
        raise AirflowException(f"We received more than 2 Ent file in this Execution Time  ({execution_time}).")
    else:
        nof= len(file_size_dict.keys())
        print(f"We have received {nof} files now checking file size ....")
        chk_file = []
        for file in file_size_dict:
            fn = [file[0:value[0]] for key, value in comp_size_files.items() if file.startswith(key)]
            fn = ''.join(fn)
            if file_size_dict.get(file) >= comp_size_files[fn][1]:
                chk_file.append(file)
#         if len(chk_file) < len(files_name):
        if len(chk_file) ==0:
            print(chk_file)
            sender_msg(msg1,"sudhakar.g@hypersonix.ai,v_arush.s@hypersonix.ai, navaneeth@hypersonix.ai")
            raise AirflowSkipException(f"MissMatch files size in this Execution Time ({execution_time}).")
        else:
            response = "Files_Available"
            context['ti'].xcom_push(key='response', value = response)
            for fns in chk_file:
                print(f"downloading {fns} file ")
                sftp.get(fns,os.path.join(path,raw_path,fns))
            with open(os.path.join(path,val_path,val_file), 'w') as f:
                json.dump(df, f)
                
def transforming_data(path,raw_path,process_path,sep,pattern,headers=None):
    if headers is None:
        files = os.listdir(os.path.join(path + raw_path))
        match_files = [fn for fn in files if re.match('|'.join(pattern),fn)]
        for fn in match_files:
            try:
                if fn.endswith('.gz'):
                    print(f"Processing files {fn}")
                    df = pd.read_csv(os.path.join(path,raw_path,fn),sep=sep, 
                                     compression='gzip',header=None,encoding='latin-1', na_values=' ',dtype=str)
                    df['filename'] = fn
                    df['created_at'] = date.today().strftime('%Y-%m-%d')
                    df.to_csv(os.path.join(path,process_path,fn.replace(".gz","")),sep=sep,index=False)
                else:
                    print(f"Processing files {fn}")
                    df = pd.read_csv(os.path.join(path,raw_path,fn),sep=sep,
                                     encoding='latin-1',header=None, na_values=' ',dtype=str)
                    df['filename'] = fn
                    df['created_at'] = date.today().strftime('%Y-%m-%d')   
                    df.to_csv(os.path.join(path,process_path,fn),sep=sep,index=False)
            except pandas.errors.EmptyDataError:
                print(f"Empty File....{fn}")
                continue

    elif headers is not None:
        files = os.listdir(os.path.join(path + raw_path))
        match_files = [fn for fn in files if re.match('|'.join(pattern),fn)]
        for fn in match_files:
            try:
                if fn.endswith('.gz'):
                    print(f"Processing files {fn}")
                    df = pd.read_csv(os.path.join(path,raw_path,fn),sep=sep, 
                                     compression='gzip',encoding='latin-1', na_values=' ',dtype=str)
                    df['filename'] = fn
                    df['created_at'] = date.today().strftime('%Y-%m-%d')  
                    df.to_csv(os.path.join(path,process_path,fn.replace(".gz","")),sep=sep,index=False)
                else:
                    print(f"Processing files {fn}")
                    df = pd.read_csv(os.path.join(path,raw_path,fn),sep=sep, 
                                     encoding='latin-1', na_values=' ',dtype=str)
                    df['filename'] = fn
                    df['created_at'] = date.today().strftime('%Y-%m-%d')   
                    df.to_csv(os.path.join(path + process_path,fn),sep=sep,index=False)
            except pandas.errors.EmptyDataError:
                print(f"Empty File....{fn}")
                continue
                  
                    


def removing_raw_process_s3_files(path,pattern,bucket_name,bucket_key,raw_path,process_path):
    r_files = os.listdir(os.path.join(path,raw_path))
    r_match_files = [fn for fn in r_files if re.match('|'.join(pattern),fn)]
    for fn in r_match_files:
        print("deleting %s from raw_files " % (fn))
        os.remove(os.path.join(path,raw_path,fn))
        
    p_files = os.listdir(os.path.join(path,process_path))
    p_match_files = [fn for fn in p_files if re.match('|'.join(pattern),fn)]
    for fn in p_match_files:
        print("deleting %s from process_files " % (fn))
        os.remove(os.path.join(path,process_path,fn))  
    for fn in pattern:
        try:
            print("deleting %s from s3 " % (fn))
            response = s3client.list_objects_v2(Bucket=bucket_name, 
                                                 Prefix=os.path.join(bucket_key,fn))
            for obj in response['Contents']:
                s3client.delete_object(Bucket=bucket_name, Key=obj['Key'])
        except KeyError as e:
            print(e)
            continue
            
def removing_raw_process_s3_files_new(path,regex_file,bucket_name,bucket_key,raw_path,process_path, pattern= True):
    r_files = os.listdir(os.path.join(path,raw_path))
    p_files = os.listdir(os.path.join(path,process_path))
    
    if pattern:
        r_match_files = [fn for fn in r_files if re.match('|'.join(regex_file),fn)]
        p_match_files = [fn for fn in p_files if re.match('|'.join(regex_file),fn)]
    else:
        r_match_files = r_files
        p_match_files = p_files
    for fn in r_match_files:
        if os.path.isfile(os.path.join(path,raw_path, fn)):
            print("deleting %s from raw_files " % (fn))
            os.remove(os.path.join(path,raw_path,fn))
    for fn in p_match_files:
        if os.path.isfile(os.path.join(path,process_path, fn)):
            print("deleting %s from process_files " % (fn))
            os.remove(os.path.join(path,process_path,fn))
            
    for fn in p_match_files:
        try:
            print("deleting %s from s3 " % (fn))
            response = s3client.list_objects_v2(Bucket=bucket_name, 
                                                 Prefix=os.path.join(bucket_key,fn))
            for obj in response['Contents']:
                s3client.delete_object(Bucket=bucket_name, Key=obj['Key'])
        except KeyError as e:
            print(e)
            continue
        
        
        
def run_sf_single_cmd(u_db,u_wh,cmd):
    print("USING DATABASE: " + u_db)
    print("USING WAREHOUSE: " + u_wh)
   
    con = snowflake.connector.connect(user=sf_user_name,password=sf_pass,
                                      account=sf_acct, databse =u_db,warehouse=u_wh)
    try:
        con.cursor().execute(cmd)
    except snowflake.connector.errors.ProgrammingError as e:
        print('ERROR: ' + e.msg)
        
def sf_connect_sting():
    return snowflake.connector.connect(user=sf_user_name,password=sf_pass,account=sf_acct)

def prod_sf_connect_sting():
    return snowflake.connector.connect(user=prod_sf_user_name,
                                       password=prod_sf_pass,account=prod_sf_acct)
        
#  ocsp_fail_open=False       


def get_redshift_connect_string(env):
        region_name = "us-west-2"
        session = boto3.Session(profile_name='my-secret')
        client = session.client(
                service_name='secretsmanager',
                region_name=region_name
        )
        get_rds_secrets = client.get_secret_value(
            SecretId=f"{env}/redshift"
        )
        secret_string= json.loads(get_rds_secrets.get('SecretString'))
        rds_host = secret_string.get("host", "error")
        rds_user = secret_string.get("username", "error")
        rds_pass = secret_string.get("password", "error")
        rds_port = secret_string.get("port", "error")
        return psycopg2.connect(dbname = 'hxmaster',host=rds_host,port =rds_port,
                                user=rds_user, password=rds_pass)
    


def get_mongo_connect_string():
    return pymongo.MongoClient(mango_host, username=mango_username, password=mango_password, port=27017)


def updating_mysql_entity(df,db):

    msg = dict(Body = f'Sucessfully updated {db} MySql entities in Dev, Stage and Prod Environment',
               Subject = f'Mysql: Successfully updated {db} MySql entities')
    

    list_of_host = [mysql_dev_host ,mysql_stg_host,mysql_blue_host, mysql_green_host]

    for h_lst in list_of_host:
        print(h_lst)
        try:
            engine = create_engine(f"mysql+mysqlconnector://{mysql_user}:{mysql_pass}@{h_lst}/{db}")
            df.to_sql(name='entity_log', con=engine, if_exists = 'replace', index=False)
            engine.dispose()
        except Exception as e:
            print("Alert: MySql engine is OFF",e)

    sender_msg(msg,'meenakshi@hypersonix.ai,amarjeet.s@hypersonix.ai, vaibhav.g@hypersonix.ai, archit.s@hypersonix.ai')




    


    
    

        
    
            


    
    
    
    
    
    
    
